package eu.niva.queue;

/*-
 * #%L
 * seamless-claim-queues
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

public class LockedElement<T>
{
	private final Long lockId;
	private final T element;
	
	public LockedElement(Long lockId, T element)
	{
		this.lockId = lockId;
		this.element = element;
	}

	public Long getLockId()
	{
		return lockId;
	}

	public T getElement()
	{
		return element;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((element == null) ? 0 : element.hashCode());
		result = prime * result + ((lockId == null) ? 0 : lockId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LockedElement other = (LockedElement) obj;
		if (element == null)
		{
			if (other.element != null)
				return false;
		}
		else if (!element.equals(other.element))
			return false;
		if (lockId == null)
		{
			if (other.lockId != null)
				return false;
		}
		else if (!lockId.equals(other.lockId))
			return false;
		return true;
	}
	
}
