package eu.niva.queue;

/*-
 * #%L
 * seamless-claim-queues
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This tries to avoid the use of a message broker to keep the infrastructure simple and self-contained.
 * A proper message broker/event streaming solution can be swapped in if needed. 
 * 
 * @param <T> the queue elements type
 */
public class Queue<T>
{
	private static final Logger log = LoggerFactory.getLogger(Queue.class);
	
	private String id;
	private Repository<T> repo;
	private ExecutorService executor;
	private Consumer<T> consumer;
	private long timeoutMs;
	private long waitForNewElementsMs;

	private Thread poller;
	private LocalDateTime lastReclaimTime;
	private volatile int running;

	public Queue(String id, Repository<T> repo, ExecutorService executor, long timeoutMs)
	{
		this(id, repo, executor, null, timeoutMs, 5_000L);
	}

	public Queue(String id, Repository<T> repo, ExecutorService executor, Consumer<T> consumer, long timeoutMs)
	{
		this(id, repo, executor, consumer, timeoutMs, 5_000L);
	}

	public Queue(String id, Repository<T> repo, ExecutorService executor, Consumer<T> consumer, long timeoutMs, long waitForNewElementsMs)
	{
		this.id = id;
		this.repo = repo;
		this.executor = executor;
		this.consumer = consumer;
		this.timeoutMs = timeoutMs;
		this.waitForNewElementsMs = waitForNewElementsMs;
		this.lastReclaimTime = LocalDateTime.MIN;
		startPoller();
	}
	
	public void setConsumer(Consumer<T> consumer)
	{
		this.consumer = consumer;
		startPoller();
	}
	
	public void add(T element)
	{
		repo.add(element);
	}
	
	protected Repository<T> getRepository()
	{
		return repo;
	}

	private void startPoller()
	{
		if (consumer == null || running != 0)
			return;
		
		poller = new Thread(() -> {
			Optional<LockedElement<T>> lockedElement;
			try
			{
				running = 1;
				while(running == 1)
				{
					TimeUnit.MILLISECONDS.sleep(waitForNewElementsMs);
					while(true)
					{
						try 
						{
							lockedElement = repo.lockOne();
						} 
						catch (Throwable t)
						{
							// In case of error during lock, we log it and act as if the queue was empty
							// this allows administrators to notice the error and, if the error was
							// related to a single element in the queue, it will eventually time out.
							log.error(t.getMessage(), t);
							lockedElement = Optional.empty();
						}
						
						if (!lockedElement.isPresent())
							break;
						executeConsumer(lockedElement.get());
					}
					reclaimTimedOutElements();
				}
			}
			catch (InterruptedException e)
			{
			}
		});
		poller.setDaemon(true);
		poller.start();
	}

	private void reclaimTimedOutElements()
	{
		LocalDateTime now = LocalDateTime.now();
		LocalDateTime ref = now.minus(timeoutMs, ChronoUnit.MILLIS);
		if (lastReclaimTime.isBefore(ref))
			repo.unlockOlderThan(ref);
		lastReclaimTime = now;
	}

	private void executeConsumer(LockedElement<T> lockedElement) throws InterruptedException
	{
		while (true)
		{
			try
			{
				if (!executor.isShutdown())
				{
					executor.execute(() -> consume(lockedElement));
				}
				break;
			}
			catch (RejectedExecutionException e)
			{
				TimeUnit.SECONDS.sleep(1);
			}
		}
	}
	
	private void consume(LockedElement<T> lockedElement)
	{
		log.debug("Consuming element {}", lockedElement.getLockId());
		
		try
		{
			consumer.accept(lockedElement.getElement());
			repo.delete(lockedElement);
		}
		catch (Throwable t)
		{
			log.error("Queue " + id + ": " + t.getMessage(), t);
			repo.error(lockedElement);
		}
	}

}
