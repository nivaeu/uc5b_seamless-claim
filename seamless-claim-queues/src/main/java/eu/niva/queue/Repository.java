package eu.niva.queue;

/*-
 * #%L
 * seamless-claim-queues
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.concurrent.locks.Lock;

public interface Repository<T>
{
	/**
	 * Adds an element to the queue
	 * @param element the element to add
	 */
	void add(T element);

	/**
	 * Locks an element in the queue 
	 * @return a {@link LockedElement}
	 */
	Optional<LockedElement<T>> lockOne();
	
	/**
	 * Unlocks all elements that was locked before the supplied date (and not yet deleted or unlocked).
	 * Unlocked elements are not deleted from the queue and will be locked again (sooner or later) by a call to {@link Lock}
	 * @param date the LocalDateTime to use as a reference
	 */
	void unlockOlderThan(LocalDateTime date);

	/**
	 * Deletes an element from the queue, this implicitly removes any locks on the element
	 * @param lockedElement a {@link LockedElement}
	 */
	void delete(LockedElement<T> lockedElement);
	
	/**
	 * Marks an element as error, the element must be unlocked
	 * @param lockedElement a {@link LockedElement}
	 */
	void error(LockedElement<T> lockedElement);

}
