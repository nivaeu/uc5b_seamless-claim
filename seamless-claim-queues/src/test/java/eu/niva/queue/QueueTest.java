package eu.niva.queue;

/*-
 * #%L
 * seamless-claim-queues
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class QueueTest
{
	private ThreadPoolExecutor executor;
	private TestRepository<String> repo;
	
	@BeforeEach
	public void beforeEach()
	{
		executor = new ThreadPoolExecutor(2, 10, 5, TimeUnit.SECONDS, new ArrayBlockingQueue<>(100));		
		repo = new TestRepository<>(Arrays.asList("A", "B", "C"));
	}
	
	@Test
	public void testElementsAreaConsumed() throws InterruptedException
	{
		new Queue<String>("test", repo, executor, x -> {}, 100L, 100L);
		TimeUnit.MILLISECONDS.sleep(500);
		assertEquals(0, repo.elements.size());
	}

	@Test
	public void testElementsTimesOut() throws InterruptedException
	{
		Consumer<String> consumer = (str) -> { 
			try
			{
				TimeUnit.MILLISECONDS.sleep(10_00);
			}
			catch (InterruptedException e)
			{
				throw new RuntimeException(e);
			} 
		};
		
		repo = spy(repo);
		new Queue<String>("test", repo, executor, consumer, 100L, 100L);
		TimeUnit.MILLISECONDS.sleep(600);
		verify(repo, atLeast(3)).unlock(anyLong());
	}
	
	@Test
	public void testConsumerError() throws InterruptedException
	{
		Consumer<String> consumer = (str) -> {
			if ("A".equals(str))
				throw new RuntimeException("NO WAY");
		};
		
		repo = spy(repo);
		new Queue<String>("test", repo, executor, consumer, 100L, 100L);
		TimeUnit.MILLISECONDS.sleep(600);
		verify(repo, times(1)).error(any());
		verify(repo, times(1)).unlock(0L);
	}
}
