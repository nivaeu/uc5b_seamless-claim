package eu.niva.queue;

/*-
 * #%L
 * seamless-claim-queues
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TestRepository<T> implements Repository<T>
{
	LinkedList<LockedElement<T>> elements;
	Map<Long, LocalDateTime> locks;
	
	private long key;

	public TestRepository(List<T> elements)
	{
		key = 0L;
		locks = new HashMap<>();
		this.elements = elements.stream()
			.map(e -> new LockedElement<T>(key++, e))
			.collect(toCollection(LinkedList::new));
	}

	@Override
	public synchronized void add(T element)
	{
		elements.add(new LockedElement<T>(key++, element));
	}

	@Override
	public synchronized Optional<LockedElement<T>> lockOne()
	{
		if (elements.size() == 0)
			return Optional.empty();	
		LockedElement<T> ret = elements.pop();
		locks.put(ret.getLockId(), LocalDateTime.now());
		return Optional.of(ret);
	}

	@Override
	public synchronized void delete(LockedElement<T> lockedElement)
	{
		if (locks.remove(lockedElement.getLockId()) == null)
			throw new IllegalStateException("Cannot delete element");
	}

	@Override
	public synchronized void unlockOlderThan(LocalDateTime date)
	{
		log.debug("Unlocking date={}, locks={}", date, locks);
		locks.entrySet().stream()
			.filter(e -> e.getValue().compareTo(date) < 0)
			.map(e -> e.getKey())
			.collect(toList())
			.forEach(this::unlock);
	}

	@Override
	public synchronized void error(LockedElement<T> lockedElement)
	{
		unlock(lockedElement.getLockId());
	}
	
	void unlock(Long id)
	{
		log.debug("Unlocking {}", id);
		locks.remove(id);
	}

}
