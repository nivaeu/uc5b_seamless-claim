# seamless-claim-sdk

This module is the SDK containing all interfaces to be implemented and all the base classes to be used
to crate new interventions/contract types and to connect them to the external services

# Plugins

There are different types of plugins:
* Authentication providers
* Party data loaders
* Intervention services

All plugins are loaded using java `ServiceLoader`s  

## Authentication providers

The authentication provider is responsible to authenticate users it must implements the interface `eu.niva.seamlessclaim.sdk.auth.AuthProvider`

## Party data loaders

The party data loaders are responsible to load common known data from the other PAs systems, these must implement the interface: `eu.niva.seamlessclaim.sdk.datapull.loader.PartyLoader`

## Intervention services

The intervention services are the most complex plugins and allows to manage one or more interventions, they must implement the interface `eu.niva.seamlessclaim.sdk.intervention.InterventionService`