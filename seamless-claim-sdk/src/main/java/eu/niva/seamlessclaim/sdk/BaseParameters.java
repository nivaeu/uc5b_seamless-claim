package eu.niva.seamlessclaim.sdk;

/*-
 * #%L
 * seamless-claim-sdk
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.Locale;

import lombok.Data;
import lombok.Getter;

@Data
public class BaseParameters
{
	private String userId;
	private Locale locale;
	
	@Getter(lazy = true) 
	private final String simpleLanguage = computeSimpleLanguage();

	public BaseParameters()
	{
	}

	private String computeSimpleLanguage()
	{
		if (locale == null)
			return null;
		
		String ret = locale.getLanguage().toUpperCase();
		int pos = ret.indexOf('_');
		if (pos == -1)
			return ret;
		
		return ret.substring(0, pos);
	}

	public BaseParameters(String userId, Locale locale)
	{
		this.userId = userId;
		this.locale = locale;
	}
	
	public BaseParameters(BaseParameters other)
	{
		this(other.userId, other.locale);
	}

}
