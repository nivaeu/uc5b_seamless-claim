package eu.niva.seamlessclaim.sdk.intervention;

/*-
 * #%L
 * seamless-claim-sdk
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.time.OffsetDateTime;

import eu.niva.seamlessclaim.sdk.ContractStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContractData
{
	private String id;
	private String contractTypeId;
	private String partyId;
	private String partyDescription;
	private OffsetDateTime subscribtionDt;
	private OffsetDateTime expiryDate;
	private String subscribedBy;
	private ContractStatus statusCode;

}
