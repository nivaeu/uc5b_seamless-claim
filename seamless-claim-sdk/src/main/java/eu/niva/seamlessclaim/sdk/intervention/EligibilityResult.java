package eu.niva.seamlessclaim.sdk.intervention;

/*-
 * #%L
 * seamless-claim-sdk
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.time.OffsetDateTime;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class EligibilityResult
{
	private final String message;
	private final boolean eligible;
	private final String sourceSystem;
	private final OffsetDateTime checkedAt;
	private final String detailsURL;
	private final String notificationMessage;

	@JsonCreator
	public EligibilityResult(
		@JsonProperty("message") String message, 
		@JsonProperty("wligible") boolean eligible, 
		@JsonProperty("sourceSystem") String sourceSystem, 
		@JsonProperty("checkedAt") OffsetDateTime checkedAt,
		@JsonProperty("detailsURL") String detailsURL, 
		@JsonProperty("notificationMessage") String notificationMessage)
	{
		this.message = message;
		this.eligible = eligible;
		this.sourceSystem = sourceSystem;
		this.checkedAt = checkedAt;
		this.detailsURL = detailsURL;
		this.notificationMessage = notificationMessage;
	}

	public EligibilityResult(String message, boolean eligible, String sourceSystem, 
			String detailsURL, String notificationMessage)
		{
			this.message = message;
			this.eligible = eligible;
			this.sourceSystem = sourceSystem;
			this.checkedAt = OffsetDateTime.now();
			this.detailsURL = detailsURL;
			this.notificationMessage = notificationMessage;
		}
	
	
	public String getSourceSystem()
	{
		return sourceSystem;
	}

	public String getMessage()
	{
		return message;
	}

	public boolean isEligible()
	{
		return eligible;
	}

	public OffsetDateTime getCheckedAt()
	{
		return checkedAt;
	}

	public Optional<String> getDetailsURL()
	{
		return Optional.ofNullable(detailsURL);
	}

	public Optional<String> getNotificationMessage()
	{
		return Optional.ofNullable(notificationMessage);
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((checkedAt == null) ? 0 : checkedAt.hashCode());
		result = prime * result + ((detailsURL == null) ? 0 : detailsURL.hashCode());
		result = prime * result + (eligible ? 1231 : 1237);
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		result = prime * result + ((notificationMessage == null) ? 0 : notificationMessage.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EligibilityResult other = (EligibilityResult) obj;
		if (checkedAt == null)
		{
			if (other.checkedAt != null)
				return false;
		}
		else if (!checkedAt.equals(other.checkedAt))
			return false;
		if (detailsURL == null)
		{
			if (other.detailsURL != null)
				return false;
		}
		else if (!detailsURL.equals(other.detailsURL))
			return false;
		if (eligible != other.eligible)
			return false;
		if (message == null)
		{
			if (other.message != null)
				return false;
		}
		else if (!message.equals(other.message))
			return false;
		if (notificationMessage == null)
		{
			if (other.notificationMessage != null)
				return false;
		}
		else if (!notificationMessage.equals(other.notificationMessage))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "EligibilityResult [message=" + message + ", eligible=" + eligible + ", sourceSystem=" + sourceSystem + ", checkedAt=" + checkedAt + ", detailsURL=" + detailsURL
				+ ", notificationMessage=" + notificationMessage + "]";
	}
	
}
