package eu.niva.seamlessclaim.sdk.intervention;

/*-
 * #%L
 * seamless-claim-sdk
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

public class PaymentCalculationResult<P>
{
	private final P data;

	public PaymentCalculationResult(P data)
	{
		this.data = data;
	}

	public P getData()
	{
		return data;
	}
	
}
