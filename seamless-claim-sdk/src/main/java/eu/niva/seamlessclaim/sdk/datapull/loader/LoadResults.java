package eu.niva.seamlessclaim.sdk.datapull.loader;

/*-
 * #%L
 * seamless-claim-sdk
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.time.OffsetDateTime;

import lombok.Value;

/**
 * Class used to hold a remote system call result at a point in time
 *
 * @param <T> the loaded type
 */
@Value
public class LoadResults<T>
{
	private OffsetDateTime validityDate;
	private T data;
}
