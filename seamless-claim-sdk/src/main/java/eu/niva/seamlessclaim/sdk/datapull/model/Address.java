package eu.niva.seamlessclaim.sdk.datapull.model;

/*-
 * #%L
 * seamless-claim-sdk
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import static java.util.stream.Collectors.joining;

import java.util.Arrays;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Address
{
	private String line1;
	private String line2;
	private String line3;
	private String line4;
	private String line5;
	
	public String getAllLines()
	{
		return Arrays.stream(new String[] { line1, line2, line3, line4, line5 })
			.filter(l -> l != null && !l.isEmpty())
			.collect(joining("\n"));
	}
	
}
