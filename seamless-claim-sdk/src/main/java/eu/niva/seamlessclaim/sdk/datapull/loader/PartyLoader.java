package eu.niva.seamlessclaim.sdk.datapull.loader;

/*-
 * #%L
 * seamless-claim-sdk
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.List;

import eu.niva.seamlessclaim.sdk.Configurable;
import eu.niva.seamlessclaim.sdk.datapull.model.Farm;
import eu.niva.seamlessclaim.sdk.datapull.model.FarmWithDetails;
import eu.niva.seamlessclaim.sdk.datapull.model.Person;

/**
 * Interface to implement to load party/farmer data
 */
public interface PartyLoader extends Configurable
{
	/**
	 * Loads the farm data from a remote system
	 * @param params the parameters
	 * @return the farm data
	 */
	LoadResults<FarmWithDetails> getFarm(FarmLoadParameters params);
	
	/**
	 * Search for farms on a remote system
	 * 
	 * @param params the parameters
	 * @return the matching farms
	 */
	LoadResults<List<Farm>> searchFarms(FarmSearchParameters params);
	
	/**
	 * Loads a person from a remote system
	 * 
	 * @param params the parameters
	 * @return the loaded person
	 */
	LoadResults<Person> getPerson(FarmLoadParameters params);
	
	/**
	 * Loads the farm's IBAN from a remote system
	 * @param params the parameters
	 * @return the farm's IBAN
	 */
	LoadResults<String> getIban(FarmLoadParameters params);
}
