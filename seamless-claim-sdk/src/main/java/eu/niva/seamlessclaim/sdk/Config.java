package eu.niva.seamlessclaim.sdk;

/*-
 * #%L
 * seamless-claim-sdk
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.Optional;
import java.util.concurrent.ExecutorService;

import javax.ws.rs.client.Client;

import org.jdbi.v3.core.Jdbi;

/**
 * Class holding plugins configuration, matching the configuration found in the server's config.yml
 */
public interface Config
{
	/**
	 * @return a JAX-RS client
	 */
	Optional<Client> getClient();
	
	/**
	 * @param name the datasource to connect to
	 * @return
	 */
	Optional<Jdbi> getDataSourceFactory(String name);
	
	/**
	 * Reads a property from the configured ones
	 * 
	 * @param name the property name
	 * @return the property value, if any
	 */
	Optional<String> getProperty(String name);
	
	/**
	 * @return an executor service to perfrom background/async operations
	 */
	Optional<ExecutorService> getExecutorService();
}
