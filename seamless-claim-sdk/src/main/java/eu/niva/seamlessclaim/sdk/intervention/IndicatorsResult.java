package eu.niva.seamlessclaim.sdk.intervention;

/*-
 * #%L
 * seamless-claim-sdk
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.Collections;
import java.util.List;

public class IndicatorsResult<E>
{
	private final List<EligibilityResult> interventionEligibility;
	private final List<EligibilityResult> paymentEligibility;
	private final E data;
	
	public IndicatorsResult(List<EligibilityResult> interventionEligibility, List<EligibilityResult> paymentEligibility, E data)
	{
		this.interventionEligibility = Collections.unmodifiableList(interventionEligibility);
		this.paymentEligibility = Collections.unmodifiableList(paymentEligibility);
		this.data = data;
	}

	public List<EligibilityResult> getInterventionEligibility()
	{
		return interventionEligibility;
	}

	public List<EligibilityResult> getPaymentEligibility()
	{
		return paymentEligibility;
	}

	public E getData()
	{
		return data;
	}
	
	public EligibilityStatus getEligibilityStatus()
	{
		boolean ok = interventionEligibility.stream().allMatch(m -> m.isEligible());
		EligibilityStatus status = ok ? EligibilityStatus.GREEN : EligibilityStatus.RED;
		
		if (status == EligibilityStatus.GREEN && paymentEligibility.stream().anyMatch(m -> !m.isEligible()))
			status = EligibilityStatus.YELLOW;
		
		return status;
	}
	
}
