package eu.niva.seamlessclaim.sdk.datapull.loader;

/*-
 * #%L
 * seamless-claim-sdk
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.time.OffsetDateTime;

import eu.niva.seamlessclaim.sdk.BaseParameters;

/**
 * Class holdding the parameters neeedd to laod farmer data
 *
 */
public class FarmLoadParameters extends BaseParameters
{
	private final String farmId;
	private final OffsetDateTime date;

	public FarmLoadParameters(BaseParameters env, String farmId, OffsetDateTime date)
	{
		super(env);
		this.farmId = farmId;
		this.date = date;
	}

	public String getFarmId()
	{
		return farmId;
	}

	public OffsetDateTime getDate()
	{
		return date;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((farmId == null) ? 0 : farmId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		FarmLoadParameters other = (FarmLoadParameters) obj;
		if (date == null)
		{
			if (other.date != null)
				return false;
		}
		else if (!date.equals(other.date))
			return false;
		if (farmId == null)
		{
			if (other.farmId != null)
				return false;
		}
		else if (!farmId.equals(other.farmId))
			return false;
		return true;
	}

}
