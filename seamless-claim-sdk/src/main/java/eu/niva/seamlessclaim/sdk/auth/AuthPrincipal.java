package eu.niva.seamlessclaim.sdk.auth;

/*-
 * #%L
 * seamless-claim-sdk
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.security.Principal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthPrincipal implements Principal
{
	private String name;
}

