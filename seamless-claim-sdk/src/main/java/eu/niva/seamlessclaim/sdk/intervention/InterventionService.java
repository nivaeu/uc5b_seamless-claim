package eu.niva.seamlessclaim.sdk.intervention;

/*-
 * #%L
 * seamless-claim-sdk
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.io.OutputStream;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Set;

import org.checkerframework.checker.nullness.qual.NonNull;

import eu.niva.seamlessclaim.sdk.BaseParameters;
import eu.niva.seamlessclaim.sdk.Configurable;

public interface InterventionService<E extends EligibilityData, M extends MonitoringData, P> extends Configurable
{
	Class<E> getEligibilityDataClass();
	Class<M> getMonitoringDataClass();
	Class<P> getPaymentDataClass();
	
	Set<String> getSupportedInterventionTypes();
	
	/**
	 * Gets the {@link CampaignReferenceDate} given a date.
	 * @param date a date
	 * @return the CampaignReferenceDate
	 */
	@NonNull CampaignReferenceDate getCampaignReferenceDate(OffsetDateTime date);
	
	/**
	 * Computes the eligibility indicators using the supplied parameters.
	 * This includes downloading any needed data from remote systems.
	 * @param params the computation parameters
	 * @return the indicator computation result
	 */
	@NonNull IndicatorsResult<E> computeEligibilityIndicators(ComputeIndicatorsParameters params);
	
	
	/**
	 * Computes the eligibility indicators.
	 * A previous calculation data is passed in as an argument; it is expected that 
	 * implementation only computes the {@link EligibilityResult} and reuses the
	 * supplied data that was computed via {@link #computeEligibilityIndicators(ComputeIndicatorsParameters)}.
	 * 
	 * @param params the computation parameters
	 * @param data the pre-computed data
	 * @return the indicator computation result
	 */
	@NonNull IndicatorsResult<E> computeEligibilityIndicators(ComputeIndicatorsParameters params, E data);
	
	/**
	 * Computes the monitoring results.
	 * @param params the computation parameters
	 * @param eligibilityData the eligibility data
	 * @param monitoringSystemId the monitoring system id
	 * @return the monitoring results
	 */
	@NonNull MonitoringResult<M> computeMonitoringResult(ComputeIndicatorsParameters params, E eligibilityData, String monitoringSystemId);
	
	@NonNull PaymentCalculationResult<P> calculatePayment(ComputeIndicatorsParameters params, E eligibilityData, M monitoringData);
	
	/**
	 * Register the contract to the monitoring system returning the monitoring system id.
	 * @param params the parameters
	 * @return the monitoring system reference id
	 */
	@NonNull String registerToMonitoringSystem(ComputeIndicatorsParameters params);
	
	void sendNotification(String farmId, List<String> message);
	
	/**
	 * Generates the contract report in PDF format.
	 * 
	 * @param params some useful params such as teh current user and locale
	 * @param contract the contract to work on
	 * @param paymentData the payment data
	 * @param out the output stream to write the PDF to
	 * @throws Exception if any exception is to be thrown
	 */
	void generateContractPDF(BaseParameters params, ContractData contract, E eligibilityData, OutputStream out) throws Exception;
	
	/**
	 * Generates the payment report in PDF format.
	 * 
	 * @param params some useful params such as teh current user and locale
	 * @param contract the contract to work on
	 * @param eligibilityData the eligibility data
	 * @param monitoringData the monitoring data
	 * @param paymentData the payment data
	 * @param out the output stream to write the PDF to
	 * @throws Exception if any exception is to be thrown
	 */
	void generatePaymentPDF(BaseParameters params, ContractData contract, E eligibilityData, M monitoringData, P paymentData, OutputStream out) throws Exception;
	
	/**
	 * Checks if a user is allowed to read data for a farm
	 * @param userId the user id
	 * @param farmId the farm id
	 * @return true if the read operation is allowed, false otherwise
	 */
	boolean canReadFarmData(String userId, String farmId);
	
	/**
	 * Checks if a user is allowed to write data for a farm
	 * @param userId the user id
	 * @param farmId the farm id
	 * @return true if the read operation is allowed, false otherwise
	 */
	boolean canWriteFarmData(String userId, String farmId);
}
