package eu.niva.seamlessclaim.sdk.intervention;

/*-
 * #%L
 * seamless-claim-sdk
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import eu.niva.seamlessclaim.sdk.BaseParameters;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ComputeIndicatorsParameters extends BaseParameters
{
	private String farmId;
	private CampaignReferenceDate campaignReferenceDate;
	private String contractId;
	
	public ComputeIndicatorsParameters()
	{
	}

	public ComputeIndicatorsParameters(ComputeIndicatorsParameters other)
	{
		super(other);
		this.farmId = other.farmId;
		this.campaignReferenceDate = other.campaignReferenceDate;
		this.contractId = other.contractId;
	}
	
	public ComputeIndicatorsParameters(BaseParameters baseParams, String farmId, CampaignReferenceDate campaignReferenceDate, String contractId)
	{
		super(baseParams);
		this.farmId = farmId;
		this.campaignReferenceDate = campaignReferenceDate;
		this.contractId = contractId;
	}
		
	public ComputeIndicatorsParameters(BaseParameters baseParams, String farmId, CampaignReferenceDate campaignReferenceDate)
	{
		this(baseParams, farmId, campaignReferenceDate, null);
	}
	
	
	
}
