package eu.niva.seamlessclaim.sdk.datapull.loader;

/*-
 * #%L
 * seamless-claim-sdk
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import eu.niva.seamlessclaim.sdk.BaseParameters;

public class FarmSearchParameters extends BaseParameters
{
	private final String searchString;
	
	public FarmSearchParameters(BaseParameters env, String searchString)
	{
		super(env);
		this.searchString = searchString;
	}

	public String getSearchString()
	{
		return searchString;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((searchString == null) ? 0 : searchString.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		FarmSearchParameters other = (FarmSearchParameters) obj;
		if (searchString == null)
		{
			if (other.searchString != null)
				return false;
		}
		else if (!searchString.equals(other.searchString))
			return false;
		return true;
	}
	
}
