package eu.niva.seamlessclaim.sdk.datapull.model;

/*-
 * #%L
 * seamless-claim-sdk
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class Person implements Party
{
	private String id;
	private String code;
	private String description;
	private Gender gender;
	private LocalDateTime bornDate;
	private Address bornAddress;
	private Address address;
}
