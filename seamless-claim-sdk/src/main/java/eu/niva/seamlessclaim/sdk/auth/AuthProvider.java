package eu.niva.seamlessclaim.sdk.auth;

/*-
 * #%L
 * seamless-claim-sdk
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import javax.ws.rs.container.ContainerRequestFilter;

import org.checkerframework.checker.nullness.qual.NonNull;

import eu.niva.seamlessclaim.sdk.Configurable;

public interface AuthProvider extends Configurable
{
	/**
	 * @return the auth filter
	 */
	@NonNull ContainerRequestFilter createAuthFilter();
	
	/**
	 * Checks if a principal is a backoffice user
	 * @return true if the principal is a backoffice user
	 */
	boolean isBackofficeUser(AuthPrincipal user);
	
}
