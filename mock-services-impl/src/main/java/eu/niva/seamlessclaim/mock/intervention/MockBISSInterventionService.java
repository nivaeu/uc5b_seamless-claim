package eu.niva.seamlessclaim.mock.intervention;

/*-
 * #%L
 * mock-services-impl
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.niva.seamlessclaim.intervention.biss.BISSEligibilityData;
import eu.niva.seamlessclaim.intervention.biss.BISSInterventionService;
import eu.niva.seamlessclaim.intervention.biss.BISSMonitoringData;
import eu.niva.seamlessclaim.mock.datapull.loder.PartyLoaderImpl;
import eu.niva.seamlessclaim.sdk.Config;
import eu.niva.seamlessclaim.sdk.datapull.loader.PartyLoader;
import eu.niva.seamlessclaim.sdk.intervention.CampaignReferenceDate;
import eu.niva.seamlessclaim.sdk.intervention.ComputeIndicatorsParameters;
import eu.niva.seamlessclaim.sdk.intervention.EligibilityResult;
import eu.niva.seamlessclaim.sdk.intervention.IndicatorsResult;
import eu.niva.seamlessclaim.sdk.intervention.MonitoringResult;
import eu.niva.seamlessclaim.sdk.intervention.MonitoringState;

public class MockBISSInterventionService extends BISSInterventionService
{
	private static final Logger log = LoggerFactory.getLogger(MockBISSInterventionService.class);
	private PartyLoaderImpl partyLoader;

	@Override
	public IndicatorsResult<BISSEligibilityData> computeEligibilityIndicators(ComputeIndicatorsParameters params)
	{
		OffsetDateTime now = OffsetDateTime.now();
		BISSEligibilityData ret = new BISSEligibilityData();
		ret.setActiveFarmer(true);
		ret.setDeclaredEligibleAreaSqm(12_348d);
		ret.setEnoughLandUse(true);
		ret.setGenuineFarmer(true);
		ret.setHavePaymentRights(true);
		ret.setLandUseDeclared(true);
		ret.setPaymentRightsAreaSqm(20_200d);
		ret.setPaymentRightsValue(20_200 * 134.0);
		
		List<EligibilityResult> interElig = Arrays.asList(
			new EligibilityResult("You are a genuine farmer", ret.getGenuineFarmer(), "farmer registry", now, "http://www.google.com", null)
		);

		List<EligibilityResult> payElig = Arrays.asList(
			new EligibilityResult("You declare your land use", ret.getLandUseDeclared(), "GSAA (Geospatial Application)", now, "http://maps.google.com", null),
			new EligibilityResult("You have at least 0.2 ha of land", ret.getEnoughLandUse(), "LPIS", now, "http://www.bing.com", null),
			new EligibilityResult("You have corresponding payment rights", ret.getHavePaymentRights(), "LPIS", now, "https://it.finance.yahoo.com/", null)
		);
		
		return new IndicatorsResult<BISSEligibilityData>(interElig, payElig, ret);
	}
	@Override
	public @NonNull IndicatorsResult<BISSEligibilityData> computeEligibilityIndicators(
		ComputeIndicatorsParameters params, BISSEligibilityData data)
	{
		return computeEligibilityIndicators(params);
	}

	@Override
	public CampaignReferenceDate getCampaignReferenceDate(OffsetDateTime date)
	{
		return new CampaignReferenceDate(date.getYear(), date);
	}

	@Override
	public MonitoringResult<BISSMonitoringData> computeMonitoringResult(ComputeIndicatorsParameters params, BISSEligibilityData eligibilityData, String monitoringSystemId)
	{
		BISSMonitoringData ret = new BISSMonitoringData();
		ret.setAreaFoundSqm(21_050d);
		ret.setState(MonitoringState.ENDED);
		return new MonitoringResult<BISSMonitoringData>(ret);
	}

	@Override
	public void sendNotification(String farmId, List<String> message)
	{
		log.info("Sending notifications to {}: {}", farmId, message);
	}

	@Override
	public String registerToMonitoringSystem(ComputeIndicatorsParameters params)
	{
		return "MONKEY_" + params.getContractId();
	}

	@Override
	protected void doConfigure(Config config) throws Exception
	{
		partyLoader = new PartyLoaderImpl(config);
	}
	
	@Override
	public boolean isConfigured()
	{
		return true;
	}
	@Override
	public boolean canReadFarmData(String userId, String farmId)
	{
		return true;
	}
	@Override
	public boolean canWriteFarmData(String userId, String partyId)
	{
		return true;
	}
	@Override
	protected PartyLoader getPartyLoader()
	{
		return partyLoader;
	}

}
