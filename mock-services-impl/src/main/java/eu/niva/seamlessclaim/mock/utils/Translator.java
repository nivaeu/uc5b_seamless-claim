package eu.niva.seamlessclaim.mock.utils;

/*-
 * #%L
 * mock-services-impl
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Logger;

public class Translator {
	
	private static final Logger LOGGER = Logger.getLogger(Translator.class.getName());
	
	/**
	 * translate @param key in @param locale language, if an entry is found. Otherwise return the @param key itself
	 * 
	 * @param key to be translated
	 * @param locale in which the key will be translated
	 * @return the @param key translated in the @locale language, if an entry is found in one of the properties file,
	 * otherwise the @param key is returned
	 */
	public static String translate(String key, Locale locale) {
		try {
			ResourceBundle bundle = ResourceBundle.getBundle("Messages", locale);
			return bundle.getString(key);
		} catch(MissingResourceException e) {
			LOGGER.warning(key + " key not found in Messages for locale " + locale);
			return key;
		}
	}
}
