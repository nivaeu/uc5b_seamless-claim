package eu.niva.seamlessclaim.mock.auth;

/*-
 * #%L
 * mock-services-impl
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.List;
import java.util.Optional;

import eu.niva.seamlessclaim.sdk.auth.AuthPrincipal;
import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;
import io.dropwizard.auth.basic.BasicCredentials;

public class BasicAuthenticator implements Authenticator<BasicCredentials, AuthPrincipal>
{

	private List<String> allowedUsers;

	public BasicAuthenticator(List<String> allowedUsers)
	{
		this.allowedUsers = allowedUsers;
	}

	@Override
	public Optional<AuthPrincipal> authenticate(BasicCredentials credentials) throws AuthenticationException
	{

		String username = credentials.getUsername();

		if (allowedUsers.contains(username.toUpperCase()))
		{
			if (username.toLowerCase().equals(credentials.getPassword()))
			{
				return Optional.of(new AuthPrincipal(username.toUpperCase()));
			}
		}

		return Optional.empty();
	}
}
