package eu.niva.seamlessclaim.mock.datapull.loder;

/*-
 * #%L
 * mock-services-impl
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.time.OffsetDateTime;
import java.util.List;

import javax.ws.rs.core.GenericType;

import eu.niva.seamlessclaim.sdk.Config;
import eu.niva.seamlessclaim.sdk.datapull.loader.FarmLoadParameters;
import eu.niva.seamlessclaim.sdk.datapull.loader.FarmSearchParameters;
import eu.niva.seamlessclaim.sdk.datapull.loader.LoadResults;
import eu.niva.seamlessclaim.sdk.datapull.loader.PartyLoader;
import eu.niva.seamlessclaim.sdk.datapull.model.Farm;
import eu.niva.seamlessclaim.sdk.datapull.model.FarmWithDetails;
import eu.niva.seamlessclaim.sdk.datapull.model.Person;

public class PartyLoaderImpl extends HttpLoader implements PartyLoader
{

	public PartyLoaderImpl()
	{
	}
	
	public PartyLoaderImpl(Config config)
	{
		configure(config);
	}

	@Override
	public LoadResults<String> getIban(FarmLoadParameters params)
	{
		return new LoadResults<>(OffsetDateTime.now(), "MOCK-IBAN");
	}
	
	@Override
	public LoadResults<FarmWithDetails> getFarm(FarmLoadParameters params)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LoadResults<List<Farm>> searchFarms(FarmSearchParameters params)
	{
		List<Farm> farms = getWebTarget().path("registry")
			.path(params.getUserId())
			.path("/parties")
			.queryParam("search", params.getSearchString())
			.request()
			.get(new GenericType<List<Farm>>()
			{
			});
		
		return new LoadResults<List<Farm>>(OffsetDateTime.now(), farms);
	}

	@Override
	public LoadResults<Person> getPerson(FarmLoadParameters params)
	{
		// TODO Auto-generated method stub
		return null;
	}
	
}
