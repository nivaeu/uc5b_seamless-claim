package eu.niva.seamlessclaim.mock.datapull.loder;

/*-
 * #%L
 * mock-services-impl
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;

import eu.niva.seamlessclaim.sdk.Config;
import eu.niva.seamlessclaim.sdk.Configurable;

public abstract class HttpLoader implements Configurable
{
	private WebTarget http;

	@Override
	public void configure(Config config)
	{
		String serviceEndpoint = config.getProperty("serviceEndpoint")
			.orElseThrow(() -> new IllegalStateException("serviceEndpoint property not configured"));
		Client client = config.getClient()
			.orElseThrow(() -> new IllegalStateException("Http client not configured"));
		
		this.http = client.target(serviceEndpoint);
	}
	
	@Override
	public boolean isConfigured()
	{
		return http != null;
	}

	protected WebTarget getWebTarget()
	{
		return http;
	}

}
