package eu.niva.seamlessclaim.mock.auth;

/*-
 * #%L
 * mock-services-impl
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.ArrayList;
import java.util.Arrays;

import javax.ws.rs.container.ContainerRequestFilter;

import eu.niva.seamlessclaim.sdk.Config;
import eu.niva.seamlessclaim.sdk.auth.AuthPrincipal;
import eu.niva.seamlessclaim.sdk.auth.AuthProvider;
import io.dropwizard.auth.basic.BasicCredentialAuthFilter;

public class DumbAuthProvider implements AuthProvider
{

	@Override
	public void configure(Config config) throws Exception
	{
	}

	@Override
	public boolean isConfigured()
	{
		return true;
	}

	@Override
	public ContainerRequestFilter createAuthFilter()
	{
		return new BasicCredentialAuthFilter.Builder<AuthPrincipal>()
			.setAuthenticator(
				new BasicAuthenticator(new ArrayList<String>(Arrays.asList("ADMIN", "TOM", "UINO", "ZILO"))))
			.setRealm("NIVA")
			.buildAuthFilter();
	}

	@Override
	public boolean isBackofficeUser(AuthPrincipal user)
	{
		return "admin".equalsIgnoreCase(user.getName());
	}

}
