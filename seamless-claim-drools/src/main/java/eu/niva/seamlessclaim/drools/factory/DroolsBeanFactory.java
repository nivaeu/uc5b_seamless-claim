package eu.niva.seamlessclaim.drools.factory;

/*-
 * #%L
 * seamless-claim-drools
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.io.File;
import java.util.Date;

import org.drools.decisiontable.DecisionTableProviderImpl;
import org.kie.api.KieServices;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieRepository;
import org.kie.api.builder.ReleaseId;
import org.kie.api.io.Resource;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.StatelessKieSession;
import org.kie.internal.builder.DecisionTableConfiguration;
import org.kie.internal.builder.DecisionTableInputType;
import org.kie.internal.builder.KnowledgeBuilderFactory;
import org.kie.internal.io.ResourceFactory;

public class DroolsBeanFactory
{

	public StatelessKieSession getKieSession(File ruleFile)
	{
		DroolsCacheHolder cache = DroolsCacheHolder.getInstance();
		
		if (!cache.contains(ruleFile) || cache.isUpdatedRuleFile(ruleFile))
		{
			Resource resource = ResourceFactory.newFileResource(ruleFile);
			KieServices kieServices = KieServices.Factory.get();
			KieFileSystem kieFileSystem = kieServices.newKieFileSystem().write(resource);
			kieServices.newKieBuilder(kieFileSystem).buildAll();
			KieRepository kieRepository = kieServices.getRepository();
			ReleaseId krDefaultReleaseId = kieRepository.getDefaultReleaseId();
			KieContainer kieContainer = kieServices.newKieContainer(krDefaultReleaseId);
			DroolsContainer ruleContainer = new DroolsContainer(kieContainer, new Date(ruleFile.lastModified()));
			cache.put(ruleFile, ruleContainer);
		}
		
		return DroolsCacheHolder.getInstance()
			.get(ruleFile)
			.getKieContainer()
			.newStatelessKieSession();
	}

	public String getDrlFromExcel(String excelFile)
	{
		DecisionTableConfiguration configuration = KnowledgeBuilderFactory.newDecisionTableConfiguration();
		configuration.setInputType(DecisionTableInputType.XLS);
		Resource dt = ResourceFactory.newClassPathResource(excelFile, getClass());
		DecisionTableProviderImpl decisionTableProvider = new DecisionTableProviderImpl();
		String drl = decisionTableProvider.loadFromResource(dt, null);
		return drl;
	}

}
