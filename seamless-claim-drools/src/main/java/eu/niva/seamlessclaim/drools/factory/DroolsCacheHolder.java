package eu.niva.seamlessclaim.drools.factory;

/*-
 * #%L
 * seamless-claim-drools
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.io.File;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DroolsCacheHolder {

	private static final Logger log = LoggerFactory.getLogger(DroolsCacheHolder.class);

	private final ConcurrentHashMap<String, DroolsContainer> droolsContainerMap;

	private DroolsCacheHolder() {
		droolsContainerMap = new ConcurrentHashMap<String, DroolsContainer>();
	}

	private static DroolsCacheHolder reference = null;

	public static DroolsCacheHolder getInstance() {

		if (reference == null) {
			synchronized (DroolsCacheHolder.class) {
				if (reference == null) {
					reference = new DroolsCacheHolder();

				}
			}

		}
		return reference;
	}

	protected boolean isUpdatedRuleFile(File ruleFile) {
		if (droolsContainerMap.get(ruleFile.getAbsolutePath()).getLastModified().before(new Date(ruleFile.lastModified()))) {
			log.info("{} has been updated on {} ", ruleFile.getName(), new Date(ruleFile.lastModified()));
			return true;
		}
		return false;
	}

	public boolean contains(File ruleFile)
	{
		return droolsContainerMap.containsKey(ruleFile.getAbsolutePath());
	}

	public void put(File ruleFile, DroolsContainer ruleContainer)
	{
		droolsContainerMap.put(ruleFile.getAbsolutePath(), ruleContainer);
	}

	public DroolsContainer get(File ruleFile)
	{
		return droolsContainerMap.get(ruleFile.getAbsolutePath());
	}

}
