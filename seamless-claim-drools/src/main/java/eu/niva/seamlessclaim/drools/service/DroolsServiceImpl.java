package eu.niva.seamlessclaim.drools.service;

/*-
 * #%L
 * seamless-claim-drools
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.io.File;
import java.io.IOException;

import org.kie.api.runtime.StatelessKieSession;

import eu.niva.seamlessclaim.drools.factory.DroolsBeanFactory;

public class DroolsServiceImpl implements DroolsService {

	private StatelessKieSession kieSession;
	private File rulePath;

	public DroolsServiceImpl(File rulePath) throws IOException {
		if (!rulePath.isDirectory()) {
			throw new IOException("rules Directory not found " + rulePath);
		}
		this.rulePath = rulePath;

	}

	@Override
	public void fireRules(FireRulesRequest fireRulesRequest) {

		DroolsBeanFactory dbf = new DroolsBeanFactory();
		File ruleFile = new File(rulePath.getPath(), fireRulesRequest.getFileRulesName());
		if (!ruleFile.isFile()) {
			throw new IllegalStateException("rule file not found " + ruleFile.getAbsolutePath());
		}
		
		kieSession = dbf.getKieSession(ruleFile);
// 		DROOLS LOG SETUP
//		KieRuntimeLogger logger;
//		KieServices kieServices = KieServices.Factory.get();
//		kieSession.addEventListener(new DebugAgendaEventListener());
//		kieSession.addEventListener(new DebugRuleRuntimeEventListener());
//		logger = kieServices.getLoggers().newFileLogger(kieSession, "./rulesLog");

		if (fireRulesRequest.getVariableMap() != null) {
			fireRulesRequest.getVariableMap().forEach((k, v) -> kieSession.setGlobal(k, v));
		}
		
//		try {
			kieSession.execute(fireRulesRequest.getFactList());
//		} finally {
//			logger.close();
//		}

	}

}
