package eu.niva.seamlessclaim.drools.test.model;

/*-
 * #%L
 * seamless-claim-drools
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

public class Customer {

    private CustomerType type;

    private int years;

    private int discount;

    public Customer(CustomerType type, int numOfYears) {
        super();
        this.type = type;
        this.years = numOfYears;
    }

    public CustomerType getType() {
        return type;
    }

    public void setType(CustomerType type) {
        this.type = type;
    }

    public int getYears() {
        return years;
    }

    public void setYears(int years) {
        this.years = years;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public enum CustomerType {
        INDIVIDUAL, BUSINESS, TEST;
    }
}
