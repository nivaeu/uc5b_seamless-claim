package eu.niva.seamlessclaim.drools.test;

/*-
 * #%L
 * seamless-claim-drools
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertNotEquals;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.RepetitionInfo;
import org.junit.jupiter.api.Test;
import org.kie.api.runtime.KieContainer;

import eu.niva.seamlessclaim.drools.factory.DroolsCacheHolder;
import eu.niva.seamlessclaim.drools.service.DroolsService;
import eu.niva.seamlessclaim.drools.service.DroolsServiceImpl;
import eu.niva.seamlessclaim.drools.service.FireRulesRequest;
import eu.niva.seamlessclaim.drools.test.model.Customer;
import eu.niva.seamlessclaim.drools.test.model.Customer.CustomerType;
import eu.niva.seamlessclaim.drools.test.model.EligibilityResult;

public class DroolsIntegrationTest {

	public static KieContainer kieContainer;

	@RepeatedTest(3)
	public void whenFileIsUpdated_ThenUpdateDroolsCache(RepetitionInfo repetitionInfo) throws Exception {
		Customer customer1 = new Customer(CustomerType.INDIVIDUAL, 6);
		Customer customer2 = new Customer(CustomerType.INDIVIDUAL, 2);
		Customer customer3 = new Customer(CustomerType.BUSINESS, 51);
		ClassLoader classLoader = getClass().getClassLoader();
		File ruleDir = new File(classLoader.getResource("Drools").getFile());
		DroolsService ds;
		File ruleFile = new File(classLoader.getResource("Drools/Discount.xls").getFile());
		List<EligibilityResult> discountEligibilityList = new ArrayList<EligibilityResult>();
		if (repetitionInfo.getCurrentRepetition() == 3) {
			ruleFile.setLastModified(new Date().getTime());
		}
		ds = new DroolsServiceImpl(ruleDir);
		List<Object> facts = new ArrayList<Object>();
		facts.add(customer1);
		facts.add(customer2);
		facts.add(customer3);
		Map<String, Object> varsMap = new HashMap<String, Object>();
		varsMap.put("eligibilityList", discountEligibilityList);
		FireRulesRequest request = new FireRulesRequest(ruleFile.getName(), facts, varsMap);
		ds.fireRules(request);
		if (repetitionInfo.getCurrentRepetition() == 3) {
			assertNotEquals(DroolsCacheHolder.getInstance().get(ruleFile).getKieContainer(), kieContainer);
		} else if (kieContainer != null) {
			assertEquals(DroolsCacheHolder.getInstance().get(ruleFile).getKieContainer(), kieContainer);
		}
		kieContainer = DroolsCacheHolder.getInstance().get(ruleFile).getKieContainer();
	}

	@Test
	public void giveCustomerTypeAndAge_ThenSetCorrectDiscountInFacts() throws Exception {
		Customer customer1 = new Customer(CustomerType.INDIVIDUAL, 6);
		Customer customer2 = new Customer(CustomerType.INDIVIDUAL, 2);
		Customer customer3 = new Customer(CustomerType.BUSINESS, 51);
		ClassLoader classLoader = getClass().getClassLoader();
		File ruleDir = new File(classLoader.getResource("Drools").getFile());
		DroolsService ds;
		String fileName = "Discount.xls";
		List<EligibilityResult> discountEligibilityList = new ArrayList<EligibilityResult>();
		ds = new DroolsServiceImpl(ruleDir);
		List<Object> facts = new ArrayList<Object>();
		facts.add(customer1);
		facts.add(customer2);
		facts.add(customer3);
		Map<String, Object> varsMap = new HashMap<String, Object>();
		varsMap.put("eligibilityList", discountEligibilityList);
		FireRulesRequest request = new FireRulesRequest(fileName, facts, varsMap);
		ds.fireRules(request);
		assertEquals(customer3.getDiscount(), 20);
		assertEquals(customer1.getDiscount(), 15);
		assertEquals(customer2.getDiscount(), 5);
	}

	@Test
	public void giveCustomerTypeAndAge_ThenSetCorrectDiscountMessageInVars() throws Exception {
		Customer customer1 = new Customer(CustomerType.INDIVIDUAL, 6);
		Customer customer2 = new Customer(CustomerType.INDIVIDUAL, 2);
		Customer customer3 = new Customer(CustomerType.BUSINESS, 51);
		ClassLoader classLoader = getClass().getClassLoader();
		File ruleDir = new File(classLoader.getResource("Drools").getFile());
		DroolsService ds;
		String fileName = "Discount.xls";
		List<EligibilityResult> discountEligibilityList = new ArrayList<EligibilityResult>();
		ds = new DroolsServiceImpl(ruleDir);
		List<Object> facts = new ArrayList<Object>();
		facts.add(customer1);
		facts.add(customer2);
		facts.add(customer3);
		Map<String, Object> varsMap = new HashMap<String, Object>();
		varsMap.put("eligibilityList", discountEligibilityList);
		FireRulesRequest request = new FireRulesRequest(fileName, facts, varsMap);
		ds.fireRules(request);
		assertEquals(discountEligibilityList.get(0).getMessage(), "15% di sconto");
		assertEquals(discountEligibilityList.get(1).getMessage(), "5% di sconto");
		assertEquals(discountEligibilityList.get(2).getMessage(), "20% di sconto");
	}

}
