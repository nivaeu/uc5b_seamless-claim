package eu.niva.seamlessclaim.api.services.queue;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class ContractAnomaliesMessageTest
{
	@Test
	public void isDeserializable() throws JsonMappingException, JsonProcessingException
	{
		String str = "{\"anomalies\": [{\"message\": \"You have corresponding payment rights\", \"eligible\": false, \"checkedAt\": \"2021-02-03T09:48:44.334+01:00\", \"detailsURL\": \"http://www.abacogroup.eu\", \"sourceSystem\": \"entitlements registry\", \"notificationMessage\": \"\"}], \"contractId\": \"20-2019\", \"monitoringState\": \"ENDED\"}";

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		
		ContractAnomaliesMessage obj = mapper.readValue(str, ContractAnomaliesMessage.class);
	}
	
}

