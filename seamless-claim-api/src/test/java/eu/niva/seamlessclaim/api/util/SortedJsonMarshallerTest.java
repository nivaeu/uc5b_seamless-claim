package eu.niva.seamlessclaim.api.util;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SortedJsonMarshallerTest
{
	private static final SortedJsonMarshaller marshaller = new SortedJsonMarshaller(new ObjectMapper());
	
	@Test
	public void mustSerializeNulls() throws JsonProcessingException
	{
		assertEquals("null", marshaller.toString(null));
	}
	
	@Test
	public void mustDeserializeNulls() throws JsonProcessingException
	{
		assertNull(marshaller.fromString("null", Integer.class));
	}

	
	@Test
	public void mustSerializeInKeyOrder() throws JsonProcessingException
	{
		String expected = "{\"a\":\"a\",\"b\":\"b\",\"c\":\"c\","
			+ "\"child\":{\"a\":\"a1\",\"b\":\"b1\",\"c\":\"c1\",\"child\":null}}";
		
		Dummy dummy = new Dummy("c", "b", "a");
		dummy.child = new Dummy("c1", "b1", "a1");
		assertEquals(expected, marshaller.toString(dummy));
		
		ObjectMapper defaultMapper = new ObjectMapper();
		assertNotEquals(expected, defaultMapper.writeValueAsString(dummy));
	}
	
	@SuppressWarnings("unused")
	private static class Dummy
	{
		public String c;
		public String b;
		public Dummy child;
		public String a;

		public Dummy(String c, String b, String a)
		{
			super();
			this.c = c;
			this.b = b;
			this.a = a;
		}
	}
	
	
}
