package eu.niva.seamlessclaim.api.resources;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import static eu.niva.seamlessclaim.api.util.Eligibility.toMessages;
import static java.util.stream.Collectors.toMap;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import javax.annotation.security.PermitAll;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;

import eu.niva.seamlessclaim.api.api.ContractTypeEligibilityResult;
import eu.niva.seamlessclaim.api.api.ContractTypeOut;
import eu.niva.seamlessclaim.api.api.EligibilityMessage;
import eu.niva.seamlessclaim.api.db.ContractsDao;
import eu.niva.seamlessclaim.api.db.ntt.ContractType;
import eu.niva.seamlessclaim.api.resources.async.AsyncResponseResumer;
import eu.niva.seamlessclaim.api.services.MissingInterventionServiceException;
import eu.niva.seamlessclaim.api.services.PluginRegistry;
import eu.niva.seamlessclaim.sdk.BaseParameters;
import eu.niva.seamlessclaim.sdk.auth.AuthPrincipal;
import eu.niva.seamlessclaim.sdk.intervention.CampaignReferenceDate;
import eu.niva.seamlessclaim.sdk.intervention.ComputeIndicatorsParameters;
import eu.niva.seamlessclaim.sdk.intervention.EligibilityStatus;
import eu.niva.seamlessclaim.sdk.intervention.IndicatorsResult;
import eu.niva.seamlessclaim.sdk.intervention.InterventionService;
import io.dropwizard.auth.Auth;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@Path("contract-types")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Timed
@PermitAll
public class ContractTypesResource
{
	private static final Logger log = LoggerFactory.getLogger(ContractTypesResource.class);
	
	private ContractsDao contractsDao;
	private PluginRegistry pluginRegistry;
	private ExecutorService executorService;
	private AsyncResponseResumer responseResumer;

	public ContractTypesResource(ContractsDao contractsDao, PluginRegistry pluginRegistry, ExecutorService executor)
	{
		this.contractsDao = contractsDao;
		this.pluginRegistry = pluginRegistry;
		this.executorService = executor;
		this.responseResumer = new AsyncResponseResumer(executor);
	}

	@GET
	@Operation(description = "Returns the list of all contract types")
	public List<ContractTypeOut> getContractTypes(@HeaderParam("Accept-Language") @DefaultValue("en") @Parameter(schema = @Schema(implementation = String.class)) Locale locale)
	{
		return contractsDao.getContractTypes().stream()
			.map(ContractTypeOut::new)
			.collect(Collectors.toList());
	}

	@GET
	@Path("/parties/{partyId}/is-eligible")
	@Operation(description = "Test eligibility for the supplied contract types")
	@ApiResponse(responseCode = "200", description = "The eligibility results", 
		content = @Content(array = @ArraySchema(schema = @Schema(implementation = ContractTypeEligibilityResult.class))))
	public void isEligible(@Parameter(hidden = true) @Auth AuthPrincipal principal,
		@PathParam("partyId") String partyId,
		@QueryParam("type") @NotNull @Size(min = 1) List<String> contractTypes,
		@HeaderParam("Accept-Language") @DefaultValue("en") @Parameter(schema = @Schema(implementation = String.class)) Locale locale,
		@Suspended AsyncResponse response)
	{
		LinkedHashMap<String, ContractType> contractTypesMap = loadContractTypes(contractTypes);
		
		OffsetDateTime now = OffsetDateTime.now();	
		BaseParameters baseParameters = new BaseParameters(principal.getName(), locale);
		
		ContractType ct;
		CampaignReferenceDate ref = null;
		List<Future<IndicatorsResult<?>>> futures = new ArrayList<>();
		for (String id : contractTypes)
		{
			try
			{
				ct = contractTypesMap.get(id);
				InterventionService<?, ?, ?> service = pluginRegistry.getInterventionService(id);
				OffsetDateTime tmp = ct.getReferenceDate().withYear(now.getYear());
				
				ref = service.getCampaignReferenceDate(tmp);
				ComputeIndicatorsParameters params = new ComputeIndicatorsParameters(baseParameters, partyId, ref);
				
				futures.add(executorService.submit(() -> service.computeEligibilityIndicators(params)));
			}
			catch (Exception e)
			{
				log.error(e.getMessage());
				futures.add(null);
			}
		}
		
		int year = ref != null ? ref.getYear() : now.getYear();
		responseResumer.resume(response, () -> awaitFutures(futures, contractTypesMap, baseParameters, partyId, year));
	}

	private LinkedHashMap<String, ContractType> loadContractTypes(List<String> contractTypes)
	{
		Map<String, ContractType> tmp = contractsDao.getContractTypes(contractTypes)
			.stream()
			.collect(toMap(c -> c.getId(), c -> c));

		contractTypes.forEach(ct -> {
			if (!tmp.containsKey(ct))
				throw new NotFoundException("Contract type not found: " + ct);
		});

		// We want the map elements to have same order as contractTypes 
		LinkedHashMap<String, ContractType> contractTypesMap = new LinkedHashMap<>();
		contractTypes.forEach(id -> contractTypesMap.put(id, tmp.get(id)));
		return contractTypesMap;
	}

	private List<ContractTypeEligibilityResult> awaitFutures(List<Future<IndicatorsResult<?>>> futures, 
		LinkedHashMap<String, ContractType> contractTypesMap, 
		BaseParameters params, String partyId, int year) throws InterruptedException, ExecutionException, MissingInterventionServiceException
	{
		int i = 0;
		Future<IndicatorsResult<?>> fut;
		ContractTypeEligibilityResult eligResult;
		InterventionService<?, ?, ?> service;
		CampaignReferenceDate ref;
		List<ContractTypeEligibilityResult> ret = new ArrayList<>();
		for (Entry<String, ContractType> e : contractTypesMap.entrySet())
		{
			fut = futures.get(i++);
			
			service = pluginRegistry.getInterventionService(e.getKey());
			ref = service.getCampaignReferenceDate(e.getValue().getReferenceDate().withYear(year));
			
			eligResult = createContractTypeEligibilityResult(e.getKey(), 
				new ComputeIndicatorsParameters(params, partyId, ref), 
				fut == null ? null : fut.get());			
			ret.add(eligResult);
		}
		return ret;
	}

	private ContractTypeEligibilityResult createContractTypeEligibilityResult(String contractTypeId, ComputeIndicatorsParameters params, IndicatorsResult<?> ind)
	{
		EligibilityStatus status;
		ArrayList<EligibilityMessage> messages;
		if (ind != null)
		{
			messages = toMessages(params.getLocale(), ind.getInterventionEligibility(), ind.getPaymentEligibility());
			status = ind.getEligibilityStatus();
		}
		else
		{
			messages = new ArrayList<>(0);
			status = EligibilityStatus.ERROR;
		}

		return ContractTypeEligibilityResult.builder()
			.contractTypeId(contractTypeId)
			.eligibilityStatus(status)
			.messages(messages)
			.build();
	}

}
