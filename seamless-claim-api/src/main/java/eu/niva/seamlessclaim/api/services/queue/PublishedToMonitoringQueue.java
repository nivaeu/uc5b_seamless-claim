package eu.niva.seamlessclaim.api.services.queue;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.concurrent.ExecutorService;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.niva.queue.Queue;
import eu.niva.queue.Repository;
import eu.niva.seamlessclaim.api.db.ContractsDao;
import eu.niva.seamlessclaim.api.db.QueueDao;
import eu.niva.seamlessclaim.api.services.IndicatorsHolder;
import eu.niva.seamlessclaim.api.services.IndicatorsService;
import eu.niva.seamlessclaim.api.services.MissingInterventionServiceException;
import eu.niva.seamlessclaim.api.services.PluginRegistry;
import eu.niva.seamlessclaim.sdk.ContractStatus;
import eu.niva.seamlessclaim.sdk.intervention.EligibilityData;
import eu.niva.seamlessclaim.sdk.intervention.InterventionService;
import eu.niva.seamlessclaim.sdk.intervention.MonitoringData;
import eu.niva.seamlessclaim.sdk.intervention.MonitoringResult;
import lombok.SneakyThrows;

public class PublishedToMonitoringQueue extends Queue<PublishedToMonitoring>
{
	private static final String TABLE = "published_to_monitoring_queue";
	
	private IndicatorsService indicatorsService;
	private ContractsDao contractsDao;
	private final PluginRegistry pluginRegistry;

	public PublishedToMonitoringQueue(QueueDao dao, ExecutorService executor, IndicatorsService indicatorsservice, ContractsDao contractsDao, 
		PluginRegistry pluginRegistry, ObjectMapper objectMapper)
	{
		super(TABLE, createRepo(dao, objectMapper), executor, 60_000);
		this.indicatorsService = indicatorsservice;
		this.contractsDao = contractsDao;
		this.pluginRegistry = pluginRegistry;
		setConsumer(this::consumer);
	}

	@SneakyThrows
	private void consumer(PublishedToMonitoring message)
	{
		consumer(message, getService(message));
	}

	@SneakyThrows
	private <E extends EligibilityData, M extends MonitoringData, P> 
	void consumer(PublishedToMonitoring message, InterventionService<E, M, P> service)
	{
		IndicatorsHolder<E, M, P> indicators = indicatorsService.loadIndicators(message.getContractId(), service)
			.orElseThrow(() -> new IllegalStateException(String.format("Contract %s does not have any indicators", message.getContractId())));
		
		MonitoringResult<? extends MonitoringData> monitorigResults;
		monitorigResults = service.computeMonitoringResult(message, indicators.getEligibilityData(), message.getMonitoringRegistrationId());
		
		// Force the current monitoring system id to be sure implementors did not forget about it...
		MonitoringData data = monitorigResults.getData();
		data.setMonitoringSystemId(message.getMonitoringRegistrationId());
		IndicatorsHolder<?, ? extends MonitoringData, ?> indicatorsToSave = indicators.withMonitoringData(data);
		
		indicatorsService.useTransaction(dao -> {
			indicatorsService.storeIndicators(message.getContractId(), indicatorsToSave, service);	// Store indicators first
			if (contractsDao.updateContract(ContractStatus.UNDER_MONITORING, message.getContractId()))
				contractsDao.updateContractAud(message.getContractId());
		});
	}

	private InterventionService<? extends EligibilityData, ? extends MonitoringData,?> getService(PublishedToMonitoring message)
	{
		try
		{
			return pluginRegistry.getInterventionService(message.getContractTypeId());
		}
		catch (MissingInterventionServiceException e)
		{
			throw new IllegalStateException(e.getMessage(), e);
		}
	}

	private static Repository<PublishedToMonitoring> createRepo(QueueDao dao, ObjectMapper objectMapper)
	{
		return new DatabaseRepository<>(dao, TABLE, objectMapper, PublishedToMonitoring.class);
	}
}
