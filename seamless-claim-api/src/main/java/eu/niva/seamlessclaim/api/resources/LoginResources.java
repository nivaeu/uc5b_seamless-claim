package eu.niva.seamlessclaim.api.resources;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.codahale.metrics.annotation.Timed;

import eu.niva.seamlessclaim.api.api.User;
import eu.niva.seamlessclaim.sdk.auth.AuthPrincipal;
import eu.niva.seamlessclaim.sdk.auth.AuthProvider;
import io.dropwizard.auth.Auth;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@Path("/test-login")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Timed
public class LoginResources
{
	private AuthProvider authProvider;

	public LoginResources(AuthProvider authProvider)
	{
		this.authProvider = authProvider;
	}
	
	@GET
	@Operation(description = "Test if the user is logged in and returns it's data")
	@ApiResponses(value = {
		@ApiResponse(responseCode = "401", description = "Not logged in"),
		@ApiResponse(responseCode = "200", description = "The user's data", content = {
			@Content(schema = @Schema(implementation = User.class))
		})
	})
	public User testLogin(@Parameter(hidden = true) @Auth AuthPrincipal principal)
	{
		User ret = new User();
		ret.setUsername(principal.getName());
		ret.setDescription(principal.getName());
		
		switch (principal.getName())
		{
			case "ADMIN":
			case "FARMER":
				ret.setLang("en");
				break;
			default:
				ret.setLang("it");
				break;
		}
		
		ret.setBackendUser(authProvider.isBackofficeUser(principal));
		
		return ret;
	}
}
