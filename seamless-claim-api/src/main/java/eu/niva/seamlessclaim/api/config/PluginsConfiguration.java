package eu.niva.seamlessclaim.api.config;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.Map;

import javax.validation.Valid;

import io.dropwizard.client.JerseyClientConfiguration;
import io.dropwizard.db.DataSourceFactory;

public class PluginsConfiguration
{
	private @Valid AsyncExecutorConfig asyncExecutor;
	private @Valid JerseyClientConfiguration jerseyClient;
	private @Valid Map<String, DataSourceFactory> dataSources;
	private Map<String, String> properties;
	
	public AsyncExecutorConfig getAsyncExecutor()
	{
		return asyncExecutor;
	}
	public void setAsyncExecutor(AsyncExecutorConfig asyncExecutor)
	{
		this.asyncExecutor = asyncExecutor;
	}
	public JerseyClientConfiguration getJerseyClient()
	{
		return jerseyClient;
	}
	public void setJerseyClient(JerseyClientConfiguration jerseyClient)
	{
		this.jerseyClient = jerseyClient;
	}
	public Map<String, DataSourceFactory> getDataSources()
	{
		return dataSources;
	}
	public void setDataSources(Map<String, DataSourceFactory> dataSources)
	{
		this.dataSources = dataSources;
	}
	public Map<String, String> getProperties()
	{
		return properties;
	}
	public void setProperties(Map<String, String> properties)
	{
		this.properties = properties;
	}	
}
