package eu.niva.seamlessclaim.api.api;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.time.OffsetDateTime;

import eu.niva.seamlessclaim.api.db.ntt.ContractType;
import lombok.Data;

@Data
public class ContractTypeOut {
	
	private String id;
	private String shortDescr;
	private OffsetDateTime subscribeFrom;
	private OffsetDateTime subscribeTo;
	private Integer durationYears;
	
	
	public ContractTypeOut(ContractType ct) {
		this.id = ct.getId();
		this.shortDescr = ct.getShortDescr();
		this.subscribeFrom = ct.getSubscribeFrom();
		this.subscribeTo = ct.getSubscribeTo();
		this.durationYears = ct.getDurationYears();
	}

}

