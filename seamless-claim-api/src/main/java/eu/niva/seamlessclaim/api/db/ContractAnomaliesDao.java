package eu.niva.seamlessclaim.api.db;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.time.OffsetDateTime;
import java.util.List;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transactional;

import eu.niva.seamlessclaim.api.db.ntt.ContractAnomaly;

public interface ContractAnomaliesDao extends Transactional<ContractAnomaliesDao>
{
	
	@SqlQuery("select * from contract_anomalies"
		+ " where contract_id=:contract_id"
		+ " and :dt between dt_insert and dt_delete")
	@RegisterBeanMapper(ContractAnomaly.class)
	List<ContractAnomaly> getAnomalies(@Bind("contract_id") String contractId, @Bind("dt") OffsetDateTime date);

	@SqlUpdate("update contract_anomalies set dt_delete=:dt"
		+ " where contract_id=:contract_id"
		+ " and now() between dt_insert and dt_delete")
	void endDateAll(@Bind("contract_id") String contractId, @Bind("dt") OffsetDateTime dt);

	@SqlUpdate("update contract_anomalies set dt_delete=:dt where id=:id")
	void endDate(@Bind("id") Long id, @Bind("dt") OffsetDateTime dt);
	
	@SqlUpdate("insert into contract_anomalies (contract_id, dt_insert, dt_delete, message)"
		+ " values (:contractId, :dtInsert, :dtDelete, :message)")
	void insert(@BindBean ContractAnomaly anomaly);
	
}
