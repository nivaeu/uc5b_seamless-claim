package eu.niva.seamlessclaim.api.services.queue;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.time.LocalDateTime;
import java.util.Optional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.niva.queue.LockedElement;
import eu.niva.queue.Repository;
import eu.niva.seamlessclaim.api.db.QueueDao;
import eu.niva.seamlessclaim.api.db.ntt.QueueRow;

public class DatabaseRepository<T> implements Repository<T>
{
	private final QueueDao queueDao;
	private final String tableName;
	private final Class<T> elementClass;
	private final ObjectMapper objectMapper;
	
	public DatabaseRepository(QueueDao queueDao, String tableName, ObjectMapper objectMapper, Class<T> elementClass)
	{
		this.queueDao = queueDao;
		this.tableName = tableName;
		this.elementClass = elementClass;
		this.objectMapper = objectMapper;
	}

	@Override
	public void add(T element)
	{
		queueDao.insertElement(tableName, toJson(element));
	}

	@Override
	public Optional<LockedElement<T>> lockOne()
	{
		QueueRow row = queueDao.lockElement(tableName);
		if (row == null)
			return Optional.empty();
		return Optional.of(new LockedElement<>(row.getId(), toElement(row.getDoc())));
	}

	@Override
	public void unlockOlderThan(LocalDateTime date)
	{
		queueDao.unlockOlderThan(tableName, date);
	}

	@Override
	public void delete(LockedElement<T> lockedElement)
	{
		queueDao.deleteElement(tableName, lockedElement.getLockId());
	}

	@Override
	public void error(LockedElement<T> lockedElement)
	{
		queueDao.markError(tableName, lockedElement.getLockId());
	}

	private String toJson(T element)
	{
		try
		{
			return objectMapper.writeValueAsString(element);
		}
		catch (JsonProcessingException e)
		{
			throw new UnsupportedOperationException(e);
		}
	}

	private T toElement(String json)
	{
		try
		{
			return objectMapper.readValue(json, elementClass);
		}
		catch (JsonProcessingException e)
		{
			throw new UnsupportedOperationException(e);
		}
	}

}
