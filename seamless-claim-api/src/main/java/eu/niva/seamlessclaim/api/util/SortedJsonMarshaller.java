package eu.niva.seamlessclaim.api.util;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.TreeMap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ValueNode;
import com.fasterxml.jackson.databind.util.RawValue;

public class SortedJsonMarshaller
{

	private ObjectMapper objectMapper;
	
	public SortedJsonMarshaller(ObjectMapper objectMapper)
	{
		this.objectMapper = objectMapper.copy().setNodeFactory(new SortingNodeFactory());
	}
	
	public String toString(Object obj) throws JsonProcessingException
	{
		if (obj == null)
			return "null";
		
		String str = objectMapper.writeValueAsString(obj);	// This has unsorted keys
		JsonNode node = objectMapper.reader().readTree(str);
		return objectMapper.writeValueAsString(node);			// This has sorted keys
	}

	public <T> T fromString(String json, Class<T> clazz) throws JsonMappingException, JsonProcessingException
	{
		if (json == null)
			return null;
		return objectMapper.readValue(json, clazz);
	}

	private static class SortingNodeFactory extends JsonNodeFactory
	{
		private static final long serialVersionUID = 1917254581015547560L;

		@Override
		public ObjectNode objectNode()
		{
			return new ObjectNode(this, new TreeMap<String, JsonNode>());
		}

		@Override
		public ValueNode pojoNode(Object pojo)
		{
			return super.pojoNode(pojo);
		}

		@Override
		public ValueNode rawValueNode(RawValue value)
		{
			return super.rawValueNode(value);
		}
	}
}
