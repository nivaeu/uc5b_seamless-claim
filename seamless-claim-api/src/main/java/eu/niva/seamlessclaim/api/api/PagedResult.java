package eu.niva.seamlessclaim.api.api;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.function.Function;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PagedResult<T>
{
	private Long totalcount;
	private Integer offset;
	private Integer count;
	private List<T> records;
	
	public <R> PagedResult<R> map(Function<T, R> mapper)
	{
		return new PagedResult<>(totalcount, offset, count, 
			records.stream().map(mapper).collect(toList()));
	}
}
