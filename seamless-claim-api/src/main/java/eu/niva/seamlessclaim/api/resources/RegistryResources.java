package eu.niva.seamlessclaim.api.resources;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import static java.util.stream.Collectors.toList;

import java.util.Locale;
import java.util.concurrent.ExecutorService;

import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;

import com.codahale.metrics.annotation.Timed;

import eu.niva.seamlessclaim.api.api.Party;
import eu.niva.seamlessclaim.api.resources.async.AsyncResponseResumer;
import eu.niva.seamlessclaim.sdk.BaseParameters;
import eu.niva.seamlessclaim.sdk.auth.AuthPrincipal;
import eu.niva.seamlessclaim.sdk.datapull.loader.FarmSearchParameters;
import eu.niva.seamlessclaim.sdk.datapull.loader.PartyLoader;
import io.dropwizard.auth.Auth;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;

@PermitAll
@Path("/registry/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Timed
public class RegistryResources
{

	private PartyLoader partyLoader;
	private AsyncResponseResumer responseResumer;

	public RegistryResources(PartyLoader partyLoader, ExecutorService executor)
	{
		this.partyLoader = partyLoader;
		this.responseResumer = new AsyncResponseResumer(executor);
	}

	@GET
	@Path("/parties")
	@Operation(description = "Returns all parties matching the search criteria")
	public void getParties(
		@Parameter(hidden = true) @Auth AuthPrincipal principal, 
		@QueryParam("search") String search, 
		@HeaderParam("Accept-Language") @DefaultValue("en") @Parameter(schema = @Schema(implementation = String.class)) Locale locale,
		@Suspended AsyncResponse response)
	{
		FarmSearchParameters params = new FarmSearchParameters(new BaseParameters(principal.getName(), locale), search);
		responseResumer.resume(response, () -> { 
			return partyLoader.searchFarms(params)
				.getData()
				.stream()
				.map(f -> Party.builder()
					.code(f.getCode())
					.description(f.getDescription())
					.id(f.getId())
					.build())
				.collect(toList());
		});
	}

}
