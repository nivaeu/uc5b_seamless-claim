package eu.niva.seamlessclaim.api.db;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.time.LocalDateTime;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.Define;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import eu.niva.seamlessclaim.api.db.ntt.QueueRow;

public interface QueueDao
{
	
	@SqlUpdate("insert into <table> (dt_insert, doc) values (now(), :doc::jsonb)")
	void insertElement(@Define("table") String table, @Bind("doc") String json);

	@SqlUpdate("update <table> set dt_lock=now() where id=(SELECT id from <table> WHERE dt_lock IS NULL and is_error=false ORDER BY id FOR UPDATE SKIP LOCKED LIMIT 1) returning *")	
	@RegisterBeanMapper(QueueRow.class)
	@GetGeneratedKeys
	QueueRow lockElement(@Define("table") String tableName);

	@SqlUpdate("update <table> set dt_lock = null where dt_lock < :date")
	void unlockOlderThan(@Define("table") String tableName, @Bind("date") LocalDateTime date);

	@SqlUpdate("delete from <table> where id = :id")
	void deleteElement(@Define("table") String tableName, @Bind("id") Long lockId);

	@SqlUpdate("update <table> set is_error=true, dt_lock=null  where id = :id")
	void markError(@Define("table") String tableName, @Bind("id") Long lockId);
	
}
