package eu.niva.seamlessclaim.api.command;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.stream.Stream;

import org.jdbi.v3.core.Jdbi;

import com.fasterxml.jackson.databind.SerializationFeature;

import eu.niva.queue.Repository;
import eu.niva.seamlessclaim.api.ClaimConfiguration;
import eu.niva.seamlessclaim.api.db.ContractsDao;
import eu.niva.seamlessclaim.api.db.QueueDao;
import eu.niva.seamlessclaim.api.db.ntt.Contract;
import eu.niva.seamlessclaim.api.services.queue.ContractToMonitor;
import eu.niva.seamlessclaim.api.services.queue.ContractsToMonitorQueue;
import eu.niva.seamlessclaim.sdk.ContractStatus;
import io.dropwizard.cli.ConfiguredCommand;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import lombok.extern.slf4j.Slf4j;
import net.sourceforge.argparse4j.inf.Namespace;

@Slf4j
public class BulkMonitorContractsCommand extends ConfiguredCommand<ClaimConfiguration>
{
	public BulkMonitorContractsCommand()
	{
		super("bulk-monitor-contracts", "Bulk publish contracts into the monitoring queue");
	}

	@Override
	protected void run(Bootstrap<ClaimConfiguration> bootstrap, Namespace namespace, ClaimConfiguration configuration)
		throws Exception
	{
		final Environment environment = new Environment(bootstrap.getApplication().getName(),
			bootstrap.getObjectMapper(),
			bootstrap.getValidatorFactory(),
			bootstrap.getMetricRegistry(),
			bootstrap.getClassLoader(),
			bootstrap.getHealthCheckRegistry(),
			configuration);
		configuration.getMetricsFactory().configure(environment.lifecycle(), bootstrap.getMetricRegistry());
		
		run(environment, configuration);
	}	
	
	private void run(Environment environment, ClaimConfiguration config) throws Exception
	{
		final JdbiFactory factory = new JdbiFactory();
	    final Jdbi jdbi = factory.build(environment, config.getDataSourceFactory(), "claimDB");

	    environment.getObjectMapper().configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

	    ///////////////////// DAO /////////////////////
	    
	    ContractsDao contractsDao = jdbi.onDemand(ContractsDao.class);
	    QueueDao queueDao = jdbi.onDemand(QueueDao.class);
	    
	    ///////////////////// SERVICES ////////////////
	    
	    Repository<ContractToMonitor> repo = ContractsToMonitorQueue.createRepo(queueDao, environment.getObjectMapper());

	    try (Stream<Contract> stream = contractsDao.streamContracts(ContractStatus.UNDER_MONITORING))
	    {
	    	stream.forEach(contract -> {
				try
				{
		    	    repo.add(new ContractToMonitor(contract));	    		
				}
				catch (Exception e)
				{
					log.error(e.getMessage(), e);
				}
	    	});
	    }
	}

}
