package eu.niva.seamlessclaim.api.db;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.Optional;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transactional;

import eu.niva.seamlessclaim.api.db.ntt.Indicators;

@RegisterBeanMapper(Indicators.class)
public interface IndicatorsDao extends Transactional<IndicatorsDao>
{

	@SqlQuery("select * from indicators where contract_id=:contract_id and now() between dt_insert and dt_delete")
	Optional<Indicators> getIndicators(@Bind("contract_id") String contractId);

	@SqlUpdate("insert into indicators (dt_insert, dt_delete, dt_last_check, contract_id, hash, eligibility_data, monitoring_data, payment_data)"
		+ " values (now(), '9999-12-31Z'::timestamptz, now(), :contractId, :hash, :eligibilityData::jsonb, :monitoringData::jsonb, :paymentData::jsonb)")
	void insertIndicators(@BindBean Indicators row);

	@SqlUpdate("update indicators set dt_last_check = now() where id = :id")	
	void setCheckedNow(@Bind("id") Long id);

	@SqlUpdate("update indicators set dt_delete = now() where contract_id = :contract_id and now() between dt_insert and dt_delete")
	void endDateCurrentIndicators(@Bind("contract_id") String contractId);
	
}
