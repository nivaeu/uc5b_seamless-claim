package eu.niva.seamlessclaim.api.services.queue;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import static java.util.stream.Collectors.toList;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutorService;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.niva.queue.Queue;
import eu.niva.queue.Repository;
import eu.niva.seamlessclaim.api.db.ContractsDao;
import eu.niva.seamlessclaim.api.db.QueueDao;
import eu.niva.seamlessclaim.api.db.ntt.Contract;
import eu.niva.seamlessclaim.api.services.IndicatorsHolder;
import eu.niva.seamlessclaim.api.services.IndicatorsService;
import eu.niva.seamlessclaim.api.services.PluginRegistry;
import eu.niva.seamlessclaim.sdk.BaseParameters;
import eu.niva.seamlessclaim.sdk.ContractStatus;
import eu.niva.seamlessclaim.sdk.intervention.CampaignReferenceDate;
import eu.niva.seamlessclaim.sdk.intervention.ComputeIndicatorsParameters;
import eu.niva.seamlessclaim.sdk.intervention.EligibilityData;
import eu.niva.seamlessclaim.sdk.intervention.EligibilityResult;
import eu.niva.seamlessclaim.sdk.intervention.IndicatorsResult;
import eu.niva.seamlessclaim.sdk.intervention.InterventionService;
import eu.niva.seamlessclaim.sdk.intervention.MonitoringData;
import eu.niva.seamlessclaim.sdk.intervention.MonitoringResult;
import eu.niva.seamlessclaim.sdk.intervention.MonitoringState;
import lombok.SneakyThrows;

public class ContractsToMonitorQueue extends Queue<ContractToMonitor>
{
	private static final String TABLE = "contracts_to_monitor_queue";
	
	private ContractsAnomaliesQueue anomaliesQueue;
	private ContractsDao contractsDao;
	private PluginRegistry pluginRegistry;
	private IndicatorsService indicatorsService;

	public ContractsToMonitorQueue(ContractsAnomaliesQueue anomaliesQueue, QueueDao dao, ContractsDao contractsDao, ExecutorService executor, PluginRegistry pluginRegistry, IndicatorsService indicatorsService, ObjectMapper objectMapper)
	{
		super(TABLE, createRepo(dao, objectMapper), executor, 120_000);
		this.anomaliesQueue = anomaliesQueue;
		this.contractsDao = contractsDao;
		this.pluginRegistry = pluginRegistry;
		this.indicatorsService = indicatorsService;
		setConsumer(this::consumer);
	}
	
	@SneakyThrows
	private void consumer(ContractToMonitor message)
	{
		Contract contract = message.getContract();
		String contractTypeId = message.getContract().getContractTypeId();
		InterventionService<? extends EligibilityData, ? extends MonitoringData, ?> service = pluginRegistry.getInterventionService(contractTypeId);
		
		BaseParameters bp = new BaseParameters("SYSTEM", Locale.ENGLISH);
		CampaignReferenceDate ref = service.getCampaignReferenceDate(OffsetDateTime.now());
		ComputeIndicatorsParameters params = new ComputeIndicatorsParameters(bp, contract.getPartyId(), ref);
		
		processContract(contract, params, service);
		
	}

	private <E extends EligibilityData, M extends MonitoringData, P> 
	void processContract(Contract contract, ComputeIndicatorsParameters params, InterventionService<E, M, P> service) throws JsonProcessingException
	{
		IndicatorsResult<E> indicatorResult = service.computeEligibilityIndicators(params);		
		MonitoringResult<M> monitoring = service.computeMonitoringResult(params, indicatorResult.getData(), contract.getContractTypeId());
		P paymentData = null;
		
		List<EligibilityResult> messages = getIneligibileResult(indicatorResult);
		if (monitoring.getState() == MonitoringState.ENDED)
		{
			paymentData = service.calculatePayment(params, indicatorResult.getData(), monitoring.getData())
				.getData();
		}
		
		IndicatorsHolder<E, M, P> holder = new IndicatorsHolder<>(OffsetDateTime.now(), indicatorResult.getData(), monitoring.getData(), paymentData);
		indicatorsService.useTransaction(dao -> {
			ContractStatus status = messages.size() == 0 ? ContractStatus.PAYABLE : ContractStatus.NOT_PAYABLE;
			indicatorsService.storeIndicators(contract.getId(), holder, service);	// Save inidcators first
			if (contractsDao.updateContract(status, contract.getId()))
				contractsDao.updateContractAud(contract.getId());
			anomaliesQueue.add(new ContractAnomaliesMessage(contract.getId(), monitoring.getState(), messages));
		});
	}

	private <E extends EligibilityData, M extends MonitoringData, P> 
	List<EligibilityResult> getIneligibileResult(IndicatorsResult<E> indicatorResult)
	{
		List<EligibilityResult> messages = indicatorResult.getInterventionEligibility()
			.stream()
			.filter(er -> !er.isEligible())
			.collect(toList());

		indicatorResult.getPaymentEligibility()
			.stream()
			.filter(er -> !er.isEligible())
			.forEach(messages::add);
		
		return messages;
	}

	public static Repository<ContractToMonitor> createRepo(QueueDao dao, ObjectMapper objectMapper)
	{
		return new DatabaseRepository<>(dao, TABLE, objectMapper, ContractToMonitor.class);
	}

}
