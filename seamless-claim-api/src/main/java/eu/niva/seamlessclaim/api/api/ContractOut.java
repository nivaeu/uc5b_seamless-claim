package eu.niva.seamlessclaim.api.api;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.time.OffsetDateTime;

import eu.niva.seamlessclaim.api.db.ntt.Contract;
import eu.niva.seamlessclaim.api.db.ntt.ContractWithPartyDescription;
import eu.niva.seamlessclaim.sdk.ContractStatus;
import lombok.Data;

@Data
public class ContractOut
{
	private String id;
	private String contractTypeId;
	private String partyId;
	private String partyDescription;
	private OffsetDateTime subscribtionDt;
	private OffsetDateTime expiryDate;
	private String subscribedBy;
	private ContractStatus statusCode;
	
	public ContractOut(Contract contract, String partyDescription)
	{
		this.id = contract.getId();
		this.contractTypeId = contract.getContractTypeId();
		this.partyId = contract.getPartyId();
		this.subscribedBy = contract.getSubscribedBy();
		this.subscribtionDt = contract.getSubscribtionDt();
		this.expiryDate = contract.getExpiryDate();
		this.statusCode = contract.getStatusCode();
		
		this.partyDescription = partyDescription;
	}

	public ContractOut(ContractWithPartyDescription contract)
	{
		this.id = contract.getId();
		this.contractTypeId = contract.getContractTypeId();
		this.partyId = contract.getPartyId();
		this.partyDescription = contract.getPartyDescription();
		this.subscribedBy = contract.getSubscribedBy();
		this.subscribtionDt = contract.getSubscribtionDt();
		this.expiryDate = contract.getExpiryDate();
		this.statusCode = contract.getStatusCode();
	}

}
