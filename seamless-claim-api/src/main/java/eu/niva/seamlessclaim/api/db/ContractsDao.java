package eu.niva.seamlessclaim.api.db;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import javax.ws.rs.BadRequestException;

import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.statement.SqlStatements;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.customizer.BindList;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transactional;

import eu.niva.seamlessclaim.api.api.OrderDirection;
import eu.niva.seamlessclaim.api.api.PagedResult;
import eu.niva.seamlessclaim.api.db.ntt.Contract;
import eu.niva.seamlessclaim.api.db.ntt.ContractType;
import eu.niva.seamlessclaim.api.db.ntt.ContractWithPartyDescription;
import eu.niva.seamlessclaim.api.db.ntt.Party;
import eu.niva.seamlessclaim.sdk.ContractStatus;

public interface ContractsDao extends Transactional<ContractsDao>
{
	
	default String getNewContractId(int campaign)
	{
		getHandle().createUpdate("insert into contracts_sequences (campaign, seq) values (:campaign, 0)"
			+ " on conflict do nothing")
			.bind("campaign", campaign)
			.execute();
		
		return getHandle().inTransaction(h -> {
			long seq = h.createQuery("select seq from contracts_sequences"
				+ " where campaign=:campaign for update")
				.bind("campaign", campaign)
				.mapTo(Long.class)
				.one();
			
			seq++;
			h.createUpdate("update contracts_sequences set seq = :seq where campaign=:campaign")
				.bind("seq", seq)
				.bind("campaign", campaign)
				.execute();
			
			return seq + "-" + campaign;
		});
	}
	
	default PagedResult<ContractWithPartyDescription> getContracts(String contractId, String contractTypeId, String farm, ContractStatus status, Integer offset,  Integer count, String orderBy, OrderDirection orderDir)
	{
		String sql = "SELECT c.*, p.description as party_description"
			+ " FROM contracts c INNER JOIN parties p ON p.id = c.party_id";
		
		sql += " WHERE 1=1";
		if (contractId != null)
			sql += " AND c.id=:contractId";
		if (contractTypeId != null)
			sql += " AND c.contract_type_id=:contractTypeId";
		if (farm != null)
			sql += " AND (c.party_id=:farm OR p.description LIKE upper(:farm) || '%')";
		if (status != null)
			sql += " AND c.status_code=:status";
		
		if (orderBy == null)
			orderBy = "subscribtionDt";

		String field;
		switch (orderBy)
		{
		case "id":
			field = "c.id";
			break;
		case "contractTypeId":
			field = "c.contract_type_id";
			break;
		case "statusCode":
			field = "c.status_code";
			break;
		case "subscribtionDt":
			field = "c.subscribtion_dt";
			break;
		default:
			throw new BadRequestException("Unknown order condition: " + orderBy);
		}
		
		Handle h = getHandle()
			.configure(SqlStatements.class, stmts -> stmts.setUnusedBindingAllowed(true));
		
		long totalcount = h.createQuery("select count(*) from (" + sql + ") AS x")
			.bind("contractId", contractId)
			.bind("contractTypeId", contractTypeId)
			.bind("farm", farm)
			.bind("status", status)
			
			.mapTo(Long.class)
			.one();

		sql += " ORDER BY " + field + " " + orderDir;
		sql += " OFFSET :offset LIMIT :limit";
		
		List<ContractWithPartyDescription> rows = h.createQuery(sql)
			.bind("contractId", contractId)
			.bind("contractTypeId", contractTypeId)
			.bind("farm", farm)
			.bind("status", status)
			.bind("offset", offset)
			.bind("limit", count)
			.mapToBean(ContractWithPartyDescription.class)
			.list();
		
		return new PagedResult<>(totalcount, offset, rows.size(), rows);
	}

	
	/**
	 * Returns all contract types
	 * 
	 * @return all contract types
	 */
	@SqlQuery("SELECT * FROM contract_types")
	@RegisterBeanMapper(ContractType.class)
	public List<ContractType> getContractTypes();

	/**
	 * Returns a list of {@link ContractType} based on the supplied IDs.
	 * 
	 * @param ids
	 *            the contract type IDs that needs loaded
	 * @return the contract types
	 */
	@SqlQuery("SELECT * FROM contract_types where id in (<ids>)")
	@RegisterBeanMapper(ContractType.class)
	public List<ContractType> getContractTypes(@BindList("ids") List<String> ids);
	
	@SqlQuery("SELECT * FROM contract_types where id = :id")
	@RegisterBeanMapper(ContractType.class)
	public Optional<ContractType> getContractType(@Bind("id") String id);
	
	@SqlQuery("SELECT * FROM contracts WHERE id = :id")
	@RegisterBeanMapper(Contract.class)
	Optional<Contract> getContract(@Bind("id") String id);

	@SqlQuery("SELECT * FROM contracts where status_code=:status")
	@RegisterBeanMapper(Contract.class)
	Stream<Contract> streamContracts(@Bind("status") ContractStatus status);

	@SqlQuery("SELECT p.description"
		+ " FROM contracts c INNER JOIN parties p ON p.id = c.party_id"
		+ " WHERE c.id = :id")
	Optional<String> getPartyDescription(@Bind("id") String contractId);

	/**
	 * Returns a list of contracts for a party id
	 * 
	 * @param partyId
	 *            the party id
	 * @return all the contracts associated to the partyId
	 */
	@SqlQuery("SELECT c.*, p.description as party_description"
		+ " FROM contracts c INNER JOIN parties p ON p.id = c.party_id"
		+ " WHERE c.party_id = :partyId"
		+ " ORDER BY c.subscribtion_dt")
	@RegisterBeanMapper(ContractWithPartyDescription.class)
	List<ContractWithPartyDescription> getPartyContracts(@Bind("partyId") String partyId);

	/**
	 * Insert a new {@link Contract} into table contracts, using today as the
	 * subscribtion date.
	 * 
	 * @param newContract
	 *            the contract to save
	 */
	@SqlUpdate("INSERT into contracts (id, contract_type_id, party_id, subscribtion_dt, expiry_date, subscribed_by, status_code)"
		+ " VALUES (:id, :contractTypeId, :partyId, :subscribtionDt, :expiryDate, :subscribedBy, :statusCode)")
	void createContract(@BindBean Contract newContract);

	@SqlUpdate("insert into contracts_aud (id, change_dt, contract_type_id, party_id, subscribtion_dt, expiry_date, subscribed_by, status_code)" +
		"  select id, now(), contract_type_id, party_id, subscribtion_dt, expiry_date, subscribed_by, status_code" +
		"    from contracts" +
		"   where id = :id")
	void updateContractAud(@Bind("id") String contractId);

	@SqlUpdate("update contracts "
		+ " SET status_code = :newStatus"
		+ " WHERE id = :id and status_code != :newStatus")
	boolean updateContract(@Bind("newStatus") ContractStatus newStatus, @Bind("id") String contractId);

	@SqlUpdate("insert into parties (id, description) values (:id, :description)"
		+ "on conflict (id) do update set description = excluded.description")
	void updateParty(@BindBean Party party);

}
