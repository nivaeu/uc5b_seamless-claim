package eu.niva.seamlessclaim.api.services;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.time.OffsetDateTime;
import java.util.Optional;

import javax.annotation.Nonnull;

import eu.niva.seamlessclaim.sdk.intervention.EligibilityData;
import eu.niva.seamlessclaim.sdk.intervention.MonitoringData;

public class IndicatorsHolder<E, M, P>
{
	private final OffsetDateTime date;
	private final E eligibilityData;
	private final M monitoringData;
	private final P paymentData;
	
	public IndicatorsHolder(OffsetDateTime date, @Nonnull E eligibilityData, M monitoringData, P paymentData)
	{
		super();
		this.date = date;
		this.eligibilityData = eligibilityData;
		this.monitoringData = monitoringData;
		this.paymentData = paymentData;
	}
	
	public @Nonnull E getEligibilityData()
	{
		return eligibilityData;
	}
	public Optional<M> getMonitoringData()
	{
		return Optional.ofNullable(monitoringData);
	}
	
	public OffsetDateTime getDate()
	{
		return date;
	}

	public Optional<P> getPaymentData()
	{
		return Optional.ofNullable(paymentData);
	}

	@SuppressWarnings("unchecked")
	public <X extends EligibilityData> IndicatorsHolder<E, M, P> withEligibilityData(X data)
	{
		return new IndicatorsHolder<>(date, (E) data, monitoringData, paymentData);
	}

	@SuppressWarnings("unchecked")
	public <X extends MonitoringData> IndicatorsHolder<E, M, P> withMonitoringData(X data)
	{
		return new IndicatorsHolder<>(date, eligibilityData, (M) data, paymentData);
	}
}
