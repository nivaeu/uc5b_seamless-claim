package eu.niva.seamlessclaim.api.config;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;

import javax.ws.rs.client.Client;

import org.jdbi.v3.core.Jdbi;

import eu.niva.seamlessclaim.sdk.Config;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.client.JerseyClientConfiguration;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.setup.Environment;

public class ConfigImpl implements Config
{
	private Environment environment;
	private PluginsConfiguration pluginsConfig;
	private Optional<Client> client;
	private ConcurrentHashMap<String, Jdbi> jdbiCache;
	private Optional<ExecutorService> executorService;
	
	private JdbiFactory factory;

	public ConfigImpl(Environment environment, PluginsConfiguration pluginsConfig)
	{
		if (pluginsConfig == null)
			pluginsConfig = new PluginsConfiguration();
			
		this.environment = environment;
		this.pluginsConfig = pluginsConfig;
		this.jdbiCache = new ConcurrentHashMap<>();
		
		this.factory = new JdbiFactory();
	
		Client client = null;
		JerseyClientConfiguration tmp = pluginsConfig.getJerseyClient();
		if (tmp != null)
			client = new JerseyClientBuilder(environment).using(pluginsConfig.getJerseyClient()).build("pluginsHttpClient");
		this.client = Optional.ofNullable(client);
		
		executorService = Optional.ofNullable(createExecutionService(environment, pluginsConfig));		
	}

	private ExecutorService createExecutionService(Environment environment, PluginsConfiguration pluginsConfig2)
	{
		if (pluginsConfig.getAsyncExecutor() == null)
			return null;

	    return environment.lifecycle()
	    	.executorService("plugins-async-executor")
	    	.workQueue(new ArrayBlockingQueue<>(pluginsConfig.getAsyncExecutor().getQueueSize()))
	    	.maxThreads(pluginsConfig.getAsyncExecutor().getNumThreads())
	    	.build();
	}

	@Override
	public Optional<Client> getClient()
	{
		return client;
	}

	@Override
	public Optional<Jdbi> getDataSourceFactory(String name)
	{
		Map<String, DataSourceFactory> conf = pluginsConfig.getDataSources();
		if (conf == null || conf.isEmpty())
			return Optional.empty();

		return Optional.ofNullable(jdbiCache.computeIfAbsent(name, k -> {
			DataSourceFactory dsf = conf.get(name);
			if (dsf == null)
				return null;
			return factory.build(environment, dsf, "plugin-db-" + name);
		}));
	}

	@Override
	public Optional<String> getProperty(String name)
	{
		Map<String, String> conf = pluginsConfig.getProperties();
		if (conf == null || conf.isEmpty())
			return Optional.empty();
		return Optional.ofNullable(conf.get(name));
	}

	@Override
	public Optional<ExecutorService> getExecutorService()
	{
		return executorService;
	}

}
