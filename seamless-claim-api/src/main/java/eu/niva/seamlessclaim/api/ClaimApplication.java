package eu.niva.seamlessclaim.api;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.ws.rs.container.ContainerRequestFilter;

import org.jdbi.v3.core.Jdbi;

import com.fasterxml.jackson.databind.SerializationFeature;

import eu.niva.seamlessclaim.api.command.BulkMonitorContractsCommand;
import eu.niva.seamlessclaim.api.config.AsyncExecutorConfig;
import eu.niva.seamlessclaim.api.config.ConfigImpl;
import eu.niva.seamlessclaim.api.db.ContractAnomaliesDao;
import eu.niva.seamlessclaim.api.db.ContractsDao;
import eu.niva.seamlessclaim.api.db.IndicatorsDao;
import eu.niva.seamlessclaim.api.db.QueueDao;
import eu.niva.seamlessclaim.api.resources.ContractTypesResource;
import eu.niva.seamlessclaim.api.resources.ContractsResource;
import eu.niva.seamlessclaim.api.resources.LoginResources;
import eu.niva.seamlessclaim.api.resources.RegistryResources;
import eu.niva.seamlessclaim.api.services.IndicatorsService;
import eu.niva.seamlessclaim.api.services.PluginRegistry;
import eu.niva.seamlessclaim.api.services.queue.ContractsAnomaliesQueue;
import eu.niva.seamlessclaim.api.services.queue.ContractsCreatedQueue;
import eu.niva.seamlessclaim.api.services.queue.ContractsToMonitorQueue;
import eu.niva.seamlessclaim.api.services.queue.PublishedToMonitoringQueue;
import eu.niva.seamlessclaim.sdk.auth.AuthPrincipal;
import io.dropwizard.Application;
import io.dropwizard.auth.AuthDynamicFeature;
import io.dropwizard.auth.AuthValueFactoryProvider;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.jdbi3.bundles.JdbiExceptionsBundle;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.swagger.v3.jaxrs2.integration.resources.OpenApiResource;
import io.swagger.v3.oas.integration.SwaggerConfiguration;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;

public class ClaimApplication extends Application<ClaimConfiguration>{
	public static void main(String[] args) throws Exception
	{
		new ClaimApplication().run(args);
	}

	@Override
	public String getName()
	{
		return "Seamless Claim API";
	}
	
	@Override
	public void initialize(final Bootstrap<ClaimConfiguration> bootstrap)
	{
		SubstitutingSourceProvider provider = new SubstitutingSourceProvider(bootstrap.getConfigurationSourceProvider(), 
			new EnvironmentVariableSubstitutor(false));
		bootstrap.setConfigurationSourceProvider(provider);

		bootstrap.addCommand(new BulkMonitorContractsCommand());

		bootstrap.addBundle(new JdbiExceptionsBundle());
		bootstrap.addBundle(new MigrationsBundle<ClaimConfiguration>()
		{
			@Override
			public DataSourceFactory getDataSourceFactory(ClaimConfiguration configuration)
			{
				return configuration.getDataSourceFactory();
			}
		});
	}

	@Override
	public void run(ClaimConfiguration config, Environment environment) throws Exception
	{
		final JdbiFactory factory = new JdbiFactory();
	    final Jdbi jdbi = factory.build(environment, config.getDataSourceFactory(), "claimDB");

	    ConfigImpl pluginsConfig = new ConfigImpl(environment, config.getPluginsConfiguration());
	    
	    environment.getObjectMapper().configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
	    
	    ///////////// THREAD POOL EXECUTOR ////////////
	    
	    ExecutorService executor = createExecutorService(environment, config.getAsyncExecutorConfig());
	    ExecutorService monitoringQueuesExecutor = createExecutorService(environment, config.getMonitoringQueueExecutorConfig());

	    ///////////////////// DAO /////////////////////
	    
	    ContractsDao contractsDao = jdbi.onDemand(ContractsDao.class);
	    ContractAnomaliesDao contractAnomaliesDao = jdbi.onDemand(ContractAnomaliesDao.class);
	    IndicatorsDao indicatorsDao = jdbi.onDemand(IndicatorsDao.class);
	    QueueDao queueDao = jdbi.onDemand(QueueDao.class);
	    
	    ///////////////////// SERVICES ////////////////
	    
	    PluginRegistry pluginsRegistry = new PluginRegistry(pluginsConfig);
	    IndicatorsService indicatorsService = new IndicatorsService(indicatorsDao, environment.getObjectMapper());

	    ////////////////////// QUEUES /////////////////

	    PublishedToMonitoringQueue publishedToMonitoringQueue = new PublishedToMonitoringQueue(queueDao, monitoringQueuesExecutor, 
	    	indicatorsService, contractsDao, pluginsRegistry, environment.getObjectMapper());
	    ContractsCreatedQueue publishToMonitoringQueue = new ContractsCreatedQueue(publishedToMonitoringQueue, queueDao, 
	    	monitoringQueuesExecutor, pluginsRegistry, environment.getObjectMapper());
	    
	    ContractsAnomaliesQueue contractsAnomaliesQueue = new ContractsAnomaliesQueue(queueDao, contractAnomaliesDao, contractsDao, pluginsRegistry, 
	    	monitoringQueuesExecutor, environment.getObjectMapper());
	    
	    new ContractsToMonitorQueue(contractsAnomaliesQueue, queueDao, contractsDao,
	    	monitoringQueuesExecutor, pluginsRegistry, indicatorsService, environment.getObjectMapper());
	    
	    ////////////////// RESOURCES //////////////////
	    initAuth(environment, pluginsRegistry);
	    
	    environment.jersey().register(new LoginResources(pluginsRegistry.getAuthProvider()));
	    environment.jersey().register(new RegistryResources(pluginsRegistry.getPartyLoader(), executor));
	    environment.jersey().register(new ContractTypesResource(contractsDao, pluginsRegistry, executor));
	    environment.jersey().register(new ContractsResource(contractsDao, contractAnomaliesDao, indicatorsService, pluginsRegistry, publishToMonitoringQueue, executor));
	    
	    initOpenApi(environment);
	}

	private void initAuth(Environment environment, PluginRegistry pluginsRegistry)
	{
		ContainerRequestFilter filter = pluginsRegistry.getAuthProvider().createAuthFilter();
		environment.jersey().register(new AuthDynamicFeature(filter));
		environment.jersey().register(new AuthValueFactoryProvider.Binder<>(AuthPrincipal.class));
	}

	private ExecutorService createExecutorService(Environment environment, AsyncExecutorConfig asyncExecutorConfig)
	{
	    return environment.lifecycle()
	    	.executorService("async-calls")
	    	.workQueue(new ArrayBlockingQueue<>(asyncExecutorConfig.getQueueSize()))
	    	.minThreads(1)
	    	.maxThreads(asyncExecutorConfig.getNumThreads())
	    	.build();
	}

	private void initOpenApi(Environment environment)
	{
		OpenAPI oas = new OpenAPI();

		oas.info(new Info()
			.title("NIVA Seamless claim APIs"));
		
		SwaggerConfiguration oasConfig = new SwaggerConfiguration()
			.openAPI(oas)
			.prettyPrint(true)
			.resourcePackages(Stream.of("eu.niva.seamlessclaim.api.resources")
				.collect(Collectors.toSet()));

		environment.jersey().register(new OpenApiResource()
			.openApiConfiguration(oasConfig));
	}

}
