package eu.niva.seamlessclaim.api.resources;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import static eu.niva.seamlessclaim.api.util.Eligibility.toMessages;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

import java.io.IOException;
import java.io.OutputStream;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutorService;

import javax.annotation.security.PermitAll;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.StreamingOutput;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import eu.niva.queue.Queue;
import eu.niva.seamlessclaim.api.api.ContractMonitoringStatus;
import eu.niva.seamlessclaim.api.api.ContractOut;
import eu.niva.seamlessclaim.api.api.CreateContract;
import eu.niva.seamlessclaim.api.api.EligibilityMessage;
import eu.niva.seamlessclaim.api.api.OrderDirection;
import eu.niva.seamlessclaim.api.api.PagedResult;
import eu.niva.seamlessclaim.api.db.ContractAnomaliesDao;
import eu.niva.seamlessclaim.api.db.ContractsDao;
import eu.niva.seamlessclaim.api.db.ntt.Contract;
import eu.niva.seamlessclaim.api.db.ntt.ContractAnomaly;
import eu.niva.seamlessclaim.api.db.ntt.ContractType;
import eu.niva.seamlessclaim.api.db.ntt.ContractWithPartyDescription;
import eu.niva.seamlessclaim.api.db.ntt.Party;
import eu.niva.seamlessclaim.api.resources.async.AsyncResponseResumer;
import eu.niva.seamlessclaim.api.services.IndicatorsHolder;
import eu.niva.seamlessclaim.api.services.IndicatorsService;
import eu.niva.seamlessclaim.api.services.MissingInterventionServiceException;
import eu.niva.seamlessclaim.api.services.PluginRegistry;
import eu.niva.seamlessclaim.api.services.queue.ContractCreated;
import eu.niva.seamlessclaim.sdk.BaseParameters;
import eu.niva.seamlessclaim.sdk.ContractStatus;
import eu.niva.seamlessclaim.sdk.auth.AuthPrincipal;
import eu.niva.seamlessclaim.sdk.intervention.CampaignReferenceDate;
import eu.niva.seamlessclaim.sdk.intervention.ComputeIndicatorsParameters;
import eu.niva.seamlessclaim.sdk.intervention.ContractData;
import eu.niva.seamlessclaim.sdk.intervention.EligibilityData;
import eu.niva.seamlessclaim.sdk.intervention.EligibilityResult;
import eu.niva.seamlessclaim.sdk.intervention.EligibilityStatus;
import eu.niva.seamlessclaim.sdk.intervention.IndicatorsResult;
import eu.niva.seamlessclaim.sdk.intervention.InterventionService;
import eu.niva.seamlessclaim.sdk.intervention.MonitoringData;
import eu.niva.seamlessclaim.sdk.intervention.MonitoringState;
import io.dropwizard.auth.Auth;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@Path("/contracts")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Timed
@PermitAll
public class ContractsResource
{

	private ContractsDao contractsDao;
	private ContractAnomaliesDao anomaliesDao;
	private IndicatorsService indicatorsService;
	private PluginRegistry pluginsRegistry;
	private Queue<ContractCreated> publishToMonitoringQueue;
	private AsyncResponseResumer responseResumer;

	public ContractsResource(ContractsDao contractsDao, ContractAnomaliesDao anomaliesDao, 
		IndicatorsService indicatorsService, PluginRegistry pluginsRegistry, 
		Queue<ContractCreated> publishToMonitoringQueue, ExecutorService executor)
	{
		this.contractsDao = contractsDao;
		this.anomaliesDao = anomaliesDao;
		this.indicatorsService = indicatorsService;
		this.pluginsRegistry = pluginsRegistry;
		this.publishToMonitoringQueue = publishToMonitoringQueue;
		this.responseResumer = new AsyncResponseResumer(executor);
	}

	@GET
	@Path("/farmers/{party_id}")
	@Operation(description = "Gets all contracts for a party")
	public List<ContractOut> getPartyContracts(
		@Parameter(hidden = true) @Auth AuthPrincipal principal, 
		@PathParam("party_id") String partyId,
		@HeaderParam("Accept-Language") @DefaultValue("en") @Parameter(schema = @Schema(implementation = String.class)) Locale locale)
	{
		List<ContractWithPartyDescription> contracts = contractsDao.getPartyContracts(partyId);
		Map<String, InterventionService<? extends EligibilityData, ?, ?>> servicesByContractType = getInterventionServices(contracts);

		List<ContractOut> filteredContracts = contracts.stream()
			.filter(c -> servicesByContractType.containsKey(c.getContractTypeId()))
			.filter(c -> servicesByContractType.get(c.getContractTypeId()).canReadFarmData(principal.getName(), partyId))
			.map(ContractOut::new)
			.collect(toList());

		// TODO: Should we return a 403 or just an empty array?
//		if (filteredContracts.size() == 0 && contracts.size() != 0)
//			throw new ForbiddenException();
		
		return filteredContracts;
	}

	@GET
	@Operation(description = "Gets all contracts")
	public PagedResult<ContractOut> getContracts(
		@Parameter(hidden = true) @Auth AuthPrincipal principal,
		@QueryParam("contractId") String contractId, 
		@QueryParam("contractTypeId") String contractTypeId, 
		@QueryParam("farm") String farm, 
		@QueryParam("status") ContractStatus status, 
		@NotNull @QueryParam("offset") @DefaultValue("0") @Min(0) Integer offset,
		@NotNull @QueryParam("count") @DefaultValue("25") @Min(1) @Max(300) Integer count,
		@NotEmpty @QueryParam("orderBy") @DefaultValue("subscribtionDt") String orderBy,
		@NotNull @QueryParam("orderDir") @DefaultValue("ASC") OrderDirection orderDir,
		@HeaderParam("Accept-Language") @DefaultValue("en") @Parameter(schema = @Schema(implementation = String.class)) Locale locale)
	{
		boolean ok = pluginsRegistry.getAuthProvider().isBackofficeUser(principal);
		if (!ok)
			throw new ForbiddenException("You are not allowed to fulfill this action");
		
		PagedResult<ContractWithPartyDescription> contracts = contractsDao.getContracts(contractId, contractTypeId, farm, status, 
			offset, count, orderBy, orderDir);

		return contracts.map(ContractOut::new);
	}
	
	private Map<String, InterventionService<?, ?, ?>> getInterventionServices(List<ContractWithPartyDescription> contracts)
	{
		return contracts.stream()
			.map(ContractWithPartyDescription::getContractTypeId)
			.distinct()
			.collect(toMap(t -> t, t -> {
				try
				{
					return pluginsRegistry.getInterventionService(t);
				}
				catch (MissingInterventionServiceException e)
				{
					throw new RuntimeException(e);
				}
			}));
	}

	@POST
	@Operation(description = "Creates a contract")
	@ApiResponse(responseCode = "200", description = "The created contract", 
		content = @Content(schema = @Schema(implementation = ContractOut.class)))
	public void createContract(
		@Parameter(hidden = true) @Auth AuthPrincipal principal, 
		@Valid @NotNull CreateContract create, 
		@HeaderParam("Accept-Language") @DefaultValue("en") @Parameter(schema = @Schema(implementation = String.class)) Locale locale, 
		@Suspended AsyncResponse response) throws MissingInterventionServiceException 
	{
		ContractType contractType = contractsDao.getContractType(create.getContractTypeId())
			.orElseThrow(() -> new NotFoundException("Contract type not found"));

		OffsetDateTime now = OffsetDateTime.now();
		if (now.isBefore(contractType.getValidFrom()) || now.isAfter(contractType.getValidTo()))
			throw new NotFoundException("Contract type not found");

		InterventionService<?, ?, ?> service = pluginsRegistry.getInterventionService(create.getContractTypeId());
		OffsetDateTime tmp = contractType.getReferenceDate().withYear(now.getYear());
		CampaignReferenceDate ref = service.getCampaignReferenceDate(tmp);

		if (!service.canWriteFarmData(principal.getName(), create.getPartyId()))
			throw new ForbiddenException("Operation not permitted");
		
		OffsetDateTime from = contractType.getSubscribeFrom();
		OffsetDateTime to = contractType.getSubscribeTo();
		
		if (from != null)		
			from = from.withYear(ref.getYear());
		if (to != null)
			to = to.withYear(ref.getYear());
		
		if ((from != null && now.isBefore(from)) || (to != null && now.isAfter(to)))
			throw new ForbiddenException("Out of subscription period");
		
		ComputeIndicatorsParameters params = new ComputeIndicatorsParameters(new BaseParameters(principal.getName(), locale), 
			create.getPartyId(), ref);

		responseResumer.resume(response, () -> createContract(params, contractType, create, ref));
	}

	private ContractOut createContract(ComputeIndicatorsParameters params, ContractType contractType, CreateContract create, CampaignReferenceDate ref) throws MissingInterventionServiceException, JsonProcessingException
	{
		InterventionService<? extends EligibilityData, ?, ?> service = pluginsRegistry.getInterventionService(create.getContractTypeId());		
		IndicatorsResult<? extends EligibilityData> indicatorResult = service.computeEligibilityIndicators(params);

		boolean ok = indicatorResult.getInterventionEligibility()
			.stream()
			.allMatch(EligibilityResult::isEligible);
		
		if (!ok)
			throw new ClientErrorException(Response.status(Status.CONFLICT).entity("Not eligible").build());

		OffsetDateTime now = OffsetDateTime.now().withNano(0);
		OffsetDateTime expiryDate = now.plusYears(contractType.getDurationYears()).truncatedTo(ChronoUnit.DAYS); 
		
		
		// Get next number out of the insert transaction, to lock as little as possible...
		String contractId = contractsDao.getNewContractId(ref.getYear());
		Contract contract = Contract.builder()
			.contractTypeId(create.getContractTypeId())
			.expiryDate(expiryDate)
			.id(contractId)
			.partyId(create.getPartyId())
			.statusCode(ContractStatus.INTEREST_EXPRESSED)
			.subscribedBy(params.getUserId())
			.subscribtionDt(now)
			.build();
		
		return contractsDao.inTransaction(dao -> {
			dao.createContract(contract);
			dao.updateContractAud(contractId);
			dao.updateParty(new Party(contract.getPartyId(), indicatorResult.getData().getFarmDescription()));

			IndicatorsHolder<?, ?, ?> holder = new IndicatorsHolder<>(OffsetDateTime.now(), indicatorResult.getData(), null, null);
			indicatorsService.storeIndicators(contractId, holder, service);
			
			ComputeIndicatorsParameters paramsWithContractId = new ComputeIndicatorsParameters(params);
			paramsWithContractId.setContractId(contractId);
			publishToMonitoringQueue.add(new ContractCreated(paramsWithContractId, create.getContractTypeId()));
			
			return new ContractOut(contract, indicatorResult.getData().getFarmDescription());
		});
	}

	@GET
	@Path("/{contractId}/monitoring-status")
	@Operation(description = "Reads the monitring status for a contract, including eligibility")
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ContractMonitoringStatus getMonitoringStatus(
		@Parameter(hidden = true) @Auth AuthPrincipal principal,
		@PathParam("contractId") String contractId,
		@HeaderParam("Accept-Language") @DefaultValue("en") @Parameter(schema = @Schema(implementation = String.class)) Locale locale) 
			throws MissingInterventionServiceException, JsonMappingException, JsonProcessingException
	{
		Contract contract = getContract(contractId);
		InterventionService<?, ? extends MonitoringData, ?> service = pluginsRegistry.getInterventionService(contract.getContractTypeId());
		checkCanReadContract(contract, principal.getName(), service);
		
		IndicatorsHolder<? extends EligibilityData, ? extends MonitoringData, ?> ind = indicatorsService.loadIndicators(contractId, service)
			.orElseThrow(() -> new NotFoundException(String.format("Indicators not found for contract %s", contractId)));
		
		ComputeIndicatorsParameters params = createComputeIndicatorsParameters(principal, locale, contract);
		IndicatorsResult<?> res = ((InterventionService) service).computeEligibilityIndicators(params, ind.getEligibilityData());
		
		Optional<? extends MonitoringData> monitoringData = ind.getMonitoringData();
		MonitoringState state = monitoringData
			.map(MonitoringData::getState)
			.orElse(MonitoringState.NOT_STARTED);
		
		ArrayList<EligibilityMessage> messages = toMessages(locale, res.getInterventionEligibility(), res.getPaymentEligibility());
		EligibilityStatus status = res.getEligibilityStatus();
		
		List<ContractAnomaly> anomalies = anomaliesDao.getAnomalies(contractId, OffsetDateTime.now());
		
		return ContractMonitoringStatus.builder()
			.anomalies(anomalies.stream().map(ContractAnomaly::getMessage).collect(toList()))
			.contractId(contractId)
			.eligibilityStatus(status)
			.messages(messages)
			.monitoringState(state)
			.qualityIndex(monitoringData.map(MonitoringData::getQualityIndex).orElse(null))
			.referenceDate(ind.getDate())
			.build();
	}

	private ComputeIndicatorsParameters createComputeIndicatorsParameters(AuthPrincipal principal, Locale locale, Contract contract) throws MissingInterventionServiceException
	{
		InterventionService<? extends EligibilityData, ? extends MonitoringData, ?> service = pluginsRegistry.getInterventionService(contract.getContractTypeId());		
		ContractType contractType = contractsDao.getContractType(contract.getContractTypeId())
			.orElseThrow(() -> new IllegalStateException("contract type not found: " + contract.getContractTypeId()));
		
		OffsetDateTime tmp = contractType.getReferenceDate().withYear(OffsetDateTime.now().getYear());
		CampaignReferenceDate ref = service.getCampaignReferenceDate(tmp);
		ComputeIndicatorsParameters params = new ComputeIndicatorsParameters(new BaseParameters(principal.getName(), locale), 
			contract.getPartyId(), ref);
		return params;
	}

	@GET
	@Path("/{contractId}/contract.pdf")
	@Produces("application/pdf")
	@ApiResponse(responseCode = "200", description = "The PDF bytes")
	@Operation(description = "Gets the contract PDF")
	public Response getContractPdf(
		@Parameter(hidden = true) @Auth AuthPrincipal principal, 
		@PathParam("contractId") String contractId,
		@HeaderParam("Accept-Language") @DefaultValue("en") @Parameter(schema = @Schema(implementation = String.class)) Locale locale
		) throws Exception
	{
		Contract contract = getContract(contractId);
		InterventionService<?, ? extends MonitoringData, ?> service = pluginsRegistry.getInterventionService(contract.getContractTypeId());
		checkCanReadContract(contract, principal.getName(), service);
		
		BaseParameters params = new BaseParameters(principal.getName(), locale);
		StreamingOutput out = createContractPdfStreamingOutput(params, contract, service);
		return Response.ok(out).build();
	}
	
	private <E extends EligibilityData, M extends MonitoringData, P> 
	StreamingOutput createContractPdfStreamingOutput(BaseParameters params, Contract contract, InterventionService<E, M, P> service) throws Exception
	{
		IndicatorsHolder<E, M, P> indicators = indicatorsService.loadIndicators(contract.getId(), service)
			.orElseThrow(() -> new NotFoundException("Indicators not found for contract " + contract.getId()));
		
		E eligibilityData = indicators.getEligibilityData();
		ContractData cd = toContractData(contract);
		
		return new StreamingOutput()
		{
			@Override
			public void write(OutputStream output) throws IOException, WebApplicationException
			{
				try
				{
					service.generateContractPDF(params, cd, eligibilityData, output);
				}
				catch (Exception e)
				{
					throw new IOException(e);
				}
			}
		};		
	}

	@GET
	@Path("/{contractId}/payment.pdf")
	@Produces("application/pdf")
	@ApiResponse(responseCode = "200", description = "The PDF bytes")
	@Operation(description = "Gets the paymnet PDF")
	public Response getPaymentPdf(
		@Parameter(hidden = true) @Auth AuthPrincipal principal, 
		@PathParam("contractId") String contractId,
		@HeaderParam("Accept-Language") @DefaultValue("en") @Parameter(schema = @Schema(implementation = String.class)) Locale locale
		) throws Exception
	{
		Contract contract = getContract(contractId);
		InterventionService<?, ? extends MonitoringData, ?> service = pluginsRegistry.getInterventionService(contract.getContractTypeId());
		checkCanReadContract(contract, principal.getName(), service);
		
		BaseParameters params = new BaseParameters(principal.getName(), locale);
		StreamingOutput out = createPaymentPdfStreamingOutput(params, contract, service);
		return Response.ok(out).build();
	}
	
	private <E extends EligibilityData, M extends MonitoringData, P> 
	StreamingOutput createPaymentPdfStreamingOutput(BaseParameters params, Contract contract, InterventionService<E, M, P> service) throws Exception
	{
		IndicatorsHolder<E, M, P> indicators = indicatorsService.loadIndicators(contract.getId(), service)
			.orElseThrow(() -> new NotFoundException("Indicators not found for contract " + contract.getId()));
		
		E eligibilityData = indicators.getEligibilityData();
		M monitoringData = indicators.getMonitoringData()
			.orElseThrow(() -> new ForbiddenException("Contract not yet monitored"));
		P paymnentData = indicators.getPaymentData()
			.orElseThrow(() -> new ForbiddenException("Paymnet not yet computed"));
		ContractData cd = toContractData(contract);
		
		return new StreamingOutput()
		{
			@Override
			public void write(OutputStream output) throws IOException, WebApplicationException
			{
				try
				{
					service.generatePaymentPDF(params, cd, eligibilityData, monitoringData, paymnentData, output);
				}
				catch (Exception e)
				{
					throw new IOException(e);
				}
			}
		};		
	}

	private ContractData toContractData(Contract contract)
	{
		String partyDescription = contractsDao.getPartyDescription(contract.getId()).orElse("");
		return ContractData.builder()
			.contractTypeId(contract.getContractTypeId())
			.expiryDate(contract.getExpiryDate())
			.id(contract.getId())
			.partyId(contract.getPartyId())
			.partyDescription(partyDescription)
			.statusCode(contract.getStatusCode())
			.subscribedBy(contract.getSubscribedBy())
			.subscribtionDt(contract.getSubscribtionDt())
			.build();
	}

	@DELETE
	@Path("/{contractId}")
	@Operation(description = "Withdrawn a contract")
	public void withdrawContract(@Parameter(hidden = true) @Auth AuthPrincipal principal, @PathParam("contractId") String contractId) throws MissingInterventionServiceException
	{
		Contract contract = getContract(contractId);
		InterventionService<?, ? extends MonitoringData, ?> service = pluginsRegistry.getInterventionService(contract.getContractTypeId());
		checkCanWriteContract(contract, principal.getName(), service);

		contractsDao.useTransaction(tn -> {
			if (tn.updateContract(ContractStatus.WITHDRAWN, contractId))
				tn.updateContractAud(contractId);
		});
	}

	@POST
	@Path("/{contractId}/ask-for-payment")
	@Operation(description = "Ask for contract payment")
	public void payContract(@Parameter(hidden = true) @Auth AuthPrincipal principal, @PathParam("contractId") String contractId) throws MissingInterventionServiceException
	{
		Contract contract = getContract(contractId);
		if (contract.getStatusCode() != ContractStatus.PAYABLE)
			throw new ForbiddenException("Contract is not payable");
		
		InterventionService<?, ? extends MonitoringData, ?> service = pluginsRegistry.getInterventionService(contract.getContractTypeId());
		checkCanWriteContract(contract, principal.getName(), service);

		contractsDao.useTransaction(tn -> {
			if (tn.updateContract(ContractStatus.PAID, contractId))
				tn.updateContractAud(contractId);
		});
	}

	private Contract getContract(String contractId)
	{
		return contractsDao.getContract(contractId)
			.orElseThrow(() -> new NotFoundException("Contract not found"));
	}

	private void checkCanReadContract(Contract contract, String userId, InterventionService<?,? extends MonitoringData,?> service)
	{
		if (!service.canReadFarmData(userId, contract.getPartyId()))
			throw new ForbiddenException("Operation not permitted");
	}
	
	private void checkCanWriteContract(Contract contract, String userId, InterventionService<?, ? extends MonitoringData, ?> service)
	{
		if (!service.canWriteFarmData(userId, contract.getPartyId()))
			throw new ForbiddenException("Operation not permitted");
	}
	
}
