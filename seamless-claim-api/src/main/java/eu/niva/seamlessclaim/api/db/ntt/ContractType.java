package eu.niva.seamlessclaim.api.db.ntt;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.time.OffsetDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ContractType {
	
	private String id;
	private String shortDescr;
	private OffsetDateTime validFrom;
	private OffsetDateTime validTo;
	private OffsetDateTime subscribeFrom;
	private OffsetDateTime subscribeTo;
	private OffsetDateTime referenceDate;
	private Integer durationYears;
	
}
