package eu.niva.seamlessclaim.api.resources.async;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.concurrent.Executor;

import javax.ws.rs.container.AsyncResponse;

public class AsyncResponseResumer
{
	private Executor executor;

	public AsyncResponseResumer(Executor executor)
	{
		this.executor = executor;
	}
	
	public void resume(AsyncResponse response, SupplierThatCanThrow<?> supplier)
	{
		executor.execute(() -> {
			try
			{
				response.resume(supplier.get());
			}
			catch (Throwable t)
			{
				response.resume(t);
			}
		});
	}
	
	@FunctionalInterface
	public static interface SupplierThatCanThrow<T> {
		T get() throws Throwable;
	}
	
}
