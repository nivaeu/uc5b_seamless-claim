package eu.niva.seamlessclaim.api.services.queue;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.niva.queue.Queue;
import eu.niva.queue.Repository;
import eu.niva.seamlessclaim.api.db.ContractAnomaliesDao;
import eu.niva.seamlessclaim.api.db.ContractsDao;
import eu.niva.seamlessclaim.api.db.QueueDao;
import eu.niva.seamlessclaim.api.db.ntt.Contract;
import eu.niva.seamlessclaim.api.db.ntt.ContractAnomaly;
import eu.niva.seamlessclaim.api.services.MissingInterventionServiceException;
import eu.niva.seamlessclaim.api.services.PluginRegistry;
import eu.niva.seamlessclaim.sdk.intervention.EligibilityData;
import eu.niva.seamlessclaim.sdk.intervention.EligibilityResult;
import eu.niva.seamlessclaim.sdk.intervention.InterventionService;
import eu.niva.seamlessclaim.sdk.intervention.MonitoringData;
import eu.niva.seamlessclaim.sdk.intervention.MonitoringState;
import lombok.SneakyThrows;

public class ContractsAnomaliesQueue extends Queue<ContractAnomaliesMessage>
{
	private static final String TABLE = "contracts_anomalies_queue";
	private static final OffsetDateTime FOREVER = OffsetDateTime.of(9999, 12,31, 0, 0, 0, 0, ZoneOffset.UTC);
	
	private ContractAnomaliesDao contractAnomaliesDao;
	private ContractsDao contractsDao;
	private PluginRegistry pluginRegistry;

	public ContractsAnomaliesQueue(QueueDao dao, ContractAnomaliesDao contractAnomaliesDao, ContractsDao contractsDao,
		PluginRegistry pluginRegistry, ExecutorService executor, ObjectMapper objectMapper)
	{
		super(TABLE, createRepo(dao, objectMapper), executor, 10_000);
		this.contractAnomaliesDao = contractAnomaliesDao;
		this.contractsDao = contractsDao;
		this.pluginRegistry = pluginRegistry;
		setConsumer(this::consumer);
	}
	
	@SneakyThrows
	private void consumer(ContractAnomaliesMessage message)
	{
		OffsetDateTime now = OffsetDateTime.now();
		List<EligibilityResult> anomalies = message.getAnomalies();
		List<ContractAnomaly> current = contractAnomaliesDao.getAnomalies(message.getContractId(), now);

		anomalies.removeIf(a -> !a.getNotificationMessage().isPresent());
		
		if (anomalies.size() == 0 && current.size() == 0)
			return;	// Nothing to do
		
		if (anomalies.size() == 0)
		{
			contractAnomaliesDao.endDateAll(message.getContractId(), now);
			return;
		}

		Set<String> currentMessages = getMessagesSet(current);		
		anomalies.removeIf(a -> currentMessages.contains(a.getNotificationMessage().get()));
		if (anomalies.size() == 0)
			return;	// all anomalies are already in the db
		
		Set<String> anomMessages = getAnomaliesMessagesSet(anomalies);
		contractAnomaliesDao.useTransaction(dao -> {
			current.stream()
				.filter(c -> anomMessages.contains(c.getMessage()))
				.forEach(c -> dao.endDate(c.getId(), now));
			
			anomalies.stream()
				.map(er -> toContractAnomaly(er, message.getContractId(), now))
				.forEach(dao::insert);
			
			if (message.getMonitoringState() == MonitoringState.IN_PROGRESS)
				sendNotifications(message, anomalies);
		});
	}
	
	private void sendNotifications(ContractAnomaliesMessage message, List<EligibilityResult> anomalies) throws MissingInterventionServiceException
	{
		Contract contract = contractsDao.getContract(message.getContractId())
			.orElseThrow(() -> new IllegalStateException("Contract not found: " + message.getContractId()));

		List<String> messages = anomalies.stream()
			.map(a -> a.getNotificationMessage().get())
			.collect(toList());
		
		InterventionService<? extends EligibilityData, ? extends MonitoringData, ?> service = pluginRegistry.getInterventionService(contract.getContractTypeId());
		service.sendNotification(contract.getPartyId(), messages);
	}

	private ContractAnomaly toContractAnomaly(EligibilityResult er, String contractId, OffsetDateTime now)
	{
		return ContractAnomaly.builder()
			.contractId(contractId)
			.dtDelete(FOREVER)
			.dtInsert(now)
			.message(er.getNotificationMessage().get())
			.build();
	}

	private Set<String> getAnomaliesMessagesSet(List<EligibilityResult> anomalies)
	{
		Set<String> anomMessages = anomalies
			.stream()
			.map(e -> e.getNotificationMessage().get())
			.collect(toCollection(() -> new HashSet<>()));
		return anomMessages;
	}

	private Set<String> getMessagesSet(List<ContractAnomaly> current)
	{
		Set<String> currentMessages = current
			.stream()
			.map(a -> a.getMessage())
			.collect(toCollection(() -> new HashSet<>()));
		return currentMessages;
	}

	private static Repository<ContractAnomaliesMessage> createRepo(QueueDao dao, ObjectMapper objectMapper)
	{
		return new DatabaseRepository<>(dao, TABLE, objectMapper, ContractAnomaliesMessage.class);
	}

}
