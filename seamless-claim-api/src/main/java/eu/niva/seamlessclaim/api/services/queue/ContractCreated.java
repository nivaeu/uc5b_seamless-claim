package eu.niva.seamlessclaim.api.services.queue;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import eu.niva.seamlessclaim.sdk.intervention.ComputeIndicatorsParameters;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ContractCreated extends ComputeIndicatorsParameters
{
	private String contractTypeId;

	public ContractCreated()
	{
	}

	public ContractCreated(ComputeIndicatorsParameters cip, String contractTypeId)
	{
		super(cip);
		this.contractTypeId = contractTypeId;
	}
		
}
