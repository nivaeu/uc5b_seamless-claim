package eu.niva.seamlessclaim.api.services.queue;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.concurrent.ExecutorService;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.niva.queue.Queue;
import eu.niva.queue.Repository;
import eu.niva.seamlessclaim.api.db.QueueDao;
import eu.niva.seamlessclaim.api.services.PluginRegistry;
import eu.niva.seamlessclaim.sdk.intervention.InterventionService;
import eu.niva.seamlessclaim.sdk.intervention.MonitoringData;
import lombok.SneakyThrows;

public class ContractsCreatedQueue extends Queue<ContractCreated>
{
	private static final String TABLE = "contracts_created_queue";
	
	private Queue<PublishedToMonitoring> publishedQueue;
	private final PluginRegistry pluginRegistry;

	public ContractsCreatedQueue(Queue<PublishedToMonitoring> publishedQueue, QueueDao dao, ExecutorService executor, PluginRegistry pluginRegistry, ObjectMapper objectMapper)
	{
		super(TABLE, createRepo(dao, objectMapper), executor, 60_000);
		this.publishedQueue = publishedQueue;
		this.pluginRegistry = pluginRegistry;
		setConsumer(this::consumer);
	}

	@SneakyThrows
	private void consumer(ContractCreated message)
	{
		InterventionService<?, ? extends MonitoringData, ?> service = pluginRegistry.getInterventionService(message.getContractTypeId());

		String monitoringSystemId = service.registerToMonitoringSystem(message);		
		
		// Enqueue the published message
		PublishedToMonitoring newMessage = new PublishedToMonitoring(message, message.getContractTypeId(), monitoringSystemId);
		publishedQueue.add(newMessage);
	}

	private static Repository<ContractCreated> createRepo(QueueDao dao, ObjectMapper objectMapper)
	{
		return new DatabaseRepository<>(dao, TABLE, objectMapper, ContractCreated.class);
	}

}
