package eu.niva.seamlessclaim.api.services;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.Iterator;
import java.util.Optional;
import java.util.ServiceLoader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.niva.seamlessclaim.sdk.Config;
import eu.niva.seamlessclaim.sdk.Configurable;
import eu.niva.seamlessclaim.sdk.auth.AuthProvider;
import eu.niva.seamlessclaim.sdk.datapull.loader.PartyLoader;
import eu.niva.seamlessclaim.sdk.intervention.EligibilityData;
import eu.niva.seamlessclaim.sdk.intervention.InterventionService;
import eu.niva.seamlessclaim.sdk.intervention.MonitoringData;
import io.dropwizard.logback.shaded.guava.base.Predicate;

public final class PluginRegistry
{
	private static final Logger log = LoggerFactory.getLogger(PluginRegistry.class);
	
	private Config config;

	private PartyLoader partyLoader;
	private AuthProvider authProvider;
	
	@SuppressWarnings("rawtypes")
	private ServiceLoader<InterventionService> interventionServicesLoader;

	public PluginRegistry(Config config)
	{
		this.config = config;
		interventionServicesLoader = ServiceLoader.load(InterventionService.class);
		logLoadedInterventionServices();
		partyLoader = load(PartyLoader.class);
		authProvider = load(AuthProvider.class);
	}

	public PartyLoader getPartyLoader()
	{
		return partyLoader;
	}
	
	public AuthProvider getAuthProvider()
	{
		return authProvider;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public InterventionService<? extends EligibilityData, ? extends MonitoringData, ?> getInterventionService(String contractTypeId) throws MissingInterventionServiceException
	{
		Iterator<InterventionService> iter = interventionServicesLoader.iterator();
		InterventionService ret = firstInIterator(iter, c -> c.getSupportedInterventionTypes().contains(contractTypeId))
			.orElseThrow(() -> new MissingInterventionServiceException(contractTypeId));
		
		if (!ret.isConfigured())
		{
			try
			{
				ret.configure(config);
			}
			catch (Exception e)
			{
				throw new RuntimeException(e);
			}
		}
		
		return ret;
	}
	
	private <T> Optional<T> firstInIterator(Iterator<T> iter, Predicate<T> pred)
	{
		T current;
		while (iter.hasNext())
		{
			current = iter.next();
			if (pred.apply(current))
				return Optional.of(current);
		}
		return Optional.empty();
	}

	private <T> T load(Class<T> clazz)
	{
		ServiceLoader<T> loader = ServiceLoader.load(clazz);
		Iterator<T> iter = loader.iterator();
		if (!iter.hasNext())
			throw new IllegalStateException("Cannot load service of type " + clazz.getName());
		
		T ret = iter.next();
		if (!iter.hasNext())
		{
			log.info("Loaded plugin " + ret.getClass().getName());
			if (Configurable.class.isAssignableFrom(clazz))
			{
				try
				{
					((Configurable) ret).configure(config);
				}
				catch (Exception e)
				{
					throw new RuntimeException(e);
				}
			}
			return ret;
		}
		
		StringBuilder all = new StringBuilder(ret.getClass().getName());
		while (iter.hasNext())
			all.append(", ").append(iter.next().getClass().getName());
		
		throw new IllegalStateException("Too many implementations of " + clazz.getName() + ": " + all);
	}

	private void logLoadedInterventionServices()
	{
		@SuppressWarnings("rawtypes")
		Iterator<InterventionService> iter = interventionServicesLoader.iterator();
		while (iter.hasNext())
			log.info("Loaded plugin " + iter.next().getClass().getName());
	}
	
}
