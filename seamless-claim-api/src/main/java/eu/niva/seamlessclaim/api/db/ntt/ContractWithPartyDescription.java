package eu.niva.seamlessclaim.api.db.ntt;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.time.OffsetDateTime;

import eu.niva.seamlessclaim.sdk.ContractStatus;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
public class ContractWithPartyDescription 
{
	@NonNull private String id;
	@NonNull private String contractTypeId;
	@NonNull private String partyId;
	@NonNull private String partyDescription;
	@NonNull private OffsetDateTime subscribtionDt;
	@NonNull private OffsetDateTime expiryDate;
	@NonNull private String subscribedBy;
	@NonNull private ContractStatus statusCode;
}
