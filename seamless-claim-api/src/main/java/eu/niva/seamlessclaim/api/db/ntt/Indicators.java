package eu.niva.seamlessclaim.api.db.ntt;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.time.OffsetDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Indicators
{
	  private Long id;
	  private OffsetDateTime dtInsert;
	  private OffsetDateTime dtDelete;
	  private OffsetDateTime dtLastCheck;
	  private String contractId;
	  private String hash;
	  private String eligibilityData;
	  private String monitoringData;
	  private String paymentData;	  
}
