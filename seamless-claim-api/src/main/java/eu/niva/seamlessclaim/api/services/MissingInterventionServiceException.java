package eu.niva.seamlessclaim.api.services;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

public class MissingInterventionServiceException extends Exception
{

	private static final long serialVersionUID = -6425403737582547397L;

	public MissingInterventionServiceException(String interventionCode)
	{
		super(String.format("Service not found for intervention %s", interventionCode));
	}

}
