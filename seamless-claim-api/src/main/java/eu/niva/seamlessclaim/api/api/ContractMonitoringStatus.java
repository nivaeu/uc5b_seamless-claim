package eu.niva.seamlessclaim.api.api;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.time.OffsetDateTime;
import java.util.List;

import javax.validation.constraints.NotNull;

import eu.niva.seamlessclaim.sdk.intervention.EligibilityStatus;
import eu.niva.seamlessclaim.sdk.intervention.MonitoringState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ContractMonitoringStatus
{
	private @NotNull String contractId;
	private @NotNull EligibilityStatus eligibilityStatus;
	private @NotNull List<EligibilityMessage> messages;
	private @NotNull MonitoringState monitoringState;
	private @NotNull OffsetDateTime referenceDate;
	private Double qualityIndex;
	private @NotNull List<String> anomalies;
}
