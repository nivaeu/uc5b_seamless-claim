package eu.niva.seamlessclaim.api.services;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;

import org.apache.commons.codec.binary.Hex;
import org.jdbi.v3.sqlobject.transaction.TransactionalConsumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.niva.seamlessclaim.api.db.IndicatorsDao;
import eu.niva.seamlessclaim.api.db.ntt.Indicators;
import eu.niva.seamlessclaim.api.util.SortedJsonMarshaller;
import eu.niva.seamlessclaim.sdk.intervention.EligibilityData;
import eu.niva.seamlessclaim.sdk.intervention.InterventionService;
import eu.niva.seamlessclaim.sdk.intervention.MonitoringData;

public class IndicatorsService
{
	private static Charset UTF8 = Charset.forName("UTF-8");
	private IndicatorsDao indicatorsDao;
	private SortedJsonMarshaller sortedJsonMarshaller;

	public IndicatorsService(IndicatorsDao indicatorsDao, ObjectMapper objectMapper)
	{
		this.indicatorsDao = indicatorsDao;
		this.sortedJsonMarshaller = new SortedJsonMarshaller(objectMapper);
	}
	
	public <X extends Exception> void useTransaction(TransactionalConsumer<IndicatorsDao, X> callback) throws X
	{
		indicatorsDao.useTransaction(callback);
	}

	public void storeIndicators(String contractId, IndicatorsHolder<?, ?, ?> holder, InterventionService<?, ?, ?> service) throws JsonProcessingException
	{
		String e = sortedJsonMarshaller.toString(holder.getEligibilityData());
		String m = sortedJsonMarshaller.toString(holder.getMonitoringData());
		String p = sortedJsonMarshaller.toString(holder.getPaymentData());
		
		MessageDigest digester = createDigester();
		digester.update(e.getBytes(UTF8));
		digester.update(m.getBytes(UTF8));
		digester.update(p.getBytes(UTF8));
		String hash = Hex.encodeHexString(digester.digest());
		
		Optional<Indicators> row = indicatorsDao.getIndicators(contractId);
		if (row.isPresent() && row.get().getHash().equals(hash))
		{
			indicatorsDao.setCheckedNow(row.get().getId());
			return;
		}
		
		indicatorsDao.useTransaction(dao -> {
			
			dao.endDateCurrentIndicators(contractId);
			dao.insertIndicators(Indicators.builder()
				.contractId(contractId)
				.hash(hash)
				.eligibilityData(e)
				.monitoringData(m)
				.paymentData(p)
				.build());			
		});
	}
	
	public <E extends EligibilityData, M extends MonitoringData, P> Optional<IndicatorsHolder<E, M, P>> loadIndicators(String contractId, InterventionService<E, M, P> service) throws JsonMappingException, JsonProcessingException
	{
		return indicatorsDao.getIndicators(contractId)
			.map(row -> toIndicatorsHolder(row, service));
	}
	
	private <E extends EligibilityData, M extends MonitoringData, P> IndicatorsHolder<E, M, P> toIndicatorsHolder(Indicators row, InterventionService<E, M, P> service)
	{
		try
		{
			E e = sortedJsonMarshaller.fromString(row.getEligibilityData(), service.getEligibilityDataClass());
			M m = sortedJsonMarshaller.fromString(row.getMonitoringData(), service.getMonitoringDataClass());
			P p = sortedJsonMarshaller.fromString(row.getPaymentData(), service.getPaymentDataClass());
			return new IndicatorsHolder<>(row.getDtLastCheck(), e, m, p);
		}
		catch (JsonProcessingException e)
		{
			throw new RuntimeException(e);
		}
	}
	
	private MessageDigest createDigester()
	{
		try
		{
			return MessageDigest.getInstance("MD5");
		}
		catch (NoSuchAlgorithmException e)
		{
			throw new IllegalStateException("Cannot create an MD5 digester");
		}
	}
	
}
