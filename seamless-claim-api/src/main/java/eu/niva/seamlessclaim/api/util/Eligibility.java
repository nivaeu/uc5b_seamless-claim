package eu.niva.seamlessclaim.api.util;

/*-
 * #%L
 * Seamless claim API
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import eu.niva.seamlessclaim.api.api.EligibilityMessage;
import eu.niva.seamlessclaim.sdk.intervention.EligibilityResult;

public abstract class Eligibility
{
	public static ArrayList<EligibilityMessage> toMessages(Locale locale, List<EligibilityResult> interventionEligibility, List<EligibilityResult> paymentEligibility)
	{
		ArrayList<EligibilityMessage> messages = new ArrayList<>();
		interventionEligibility.stream()
			.map(e -> toMessage(locale, e))
			.forEach(messages::add);

		paymentEligibility.stream()
			.map(e -> toMessage(locale, e))
			.forEach(messages::add);
		
		return messages;
	}
	
	private static EligibilityMessage toMessage(Locale locale, EligibilityResult res)
	{
		String key = res.isEligible() ? "eligibilityMessageOK" : "eligibilityMessageNotOK";
		String msg = String.format(Translator.translate(key, locale),
			res.getMessage(), res.getSourceSystem(), res.getCheckedAt(), res.getCheckedAt(), res.getCheckedAt());
		
		return new EligibilityMessage(msg, res.isEligible(), res.getDetailsURL().orElse(null));
	}

}
