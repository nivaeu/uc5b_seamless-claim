--liquibase formatted sql  

--changeset niva:90

insert into contract_types (id, short_descr, valid_from, valid_to, subscribe_from, subscribe_to, reference_date, duration_years)
  values('YOUNG_FARMER', 'Young farmer scheme', now(), now() + INTERVAL '5' YEAR, null, null, '20200515', 5);

commit;
