--liquibase formatted sql  

--changeset niva:72

CREATE TABLE parties (
  id varchar(50) NOT NULL,
  description varchar(200) NOT NULL,
  CONSTRAINT parties_pk PRIMARY KEY (id)
);

CREATE INDEX parties_ix1 ON parties (description);

BEGIN TRANSACTION;
insert into parties (id, description)
select c.party_id, i.eligibility_data->'farmDescription'
  from contracts c inner join indicators i on i.contract_id =c.id
 where now() between i.dt_insert and i.dt_delete
   and i.eligibility_data->'farmDescription' is not null;
COMMIT;
