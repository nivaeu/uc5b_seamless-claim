--liquibase formatted sql  

--changeset niva:20

insert into contract_types (id, short_descr, valid_from, valid_to, subscribe_from, subscribe_to, reference_date, duration_years)
	values('BISS', 'Basic Income Support Scheme', now(), now() + INTERVAL '5' YEAR, '20200515T00:00:00+01:00', '20200515T00:00:00+01:00'::timestamp + interval '5' year, '20200515', 5);

commit;
