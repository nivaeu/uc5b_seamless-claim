--liquibase formatted sql  

--changeset niva:40

CREATE TABLE indicators (
  id bigserial NOT NULL,
  dt_insert timestamp WITH TIME ZONE NOT NULL,
  dt_delete timestamp WITH TIME ZONE NOT NULL,
  dt_last_check timestamp WITH TIME ZONE NOT NULL,
  contract_id varchar(50) NOT NULL,
  hash varchar(32) NOT NULL,
  eligibility_data jsonb NOT NULL,
  monitoring_data jsonb NOT NULL,
  payment_data jsonb NOT NULL,
  CONSTRAINT indicators_pk PRIMARY KEY (id),
  CONSTRAINT indicators_uk1 UNIQUE (contract_id, dt_delete),
  CONSTRAINT indicators_fk1 FOREIGN KEY (contract_id) REFERENCES contracts (id)
);

DROP TABLE contracts_land_uses;
