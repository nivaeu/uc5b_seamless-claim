--liquibase formatted sql  

--changeset niva:73

DROP INDEX parties_ix1;
CREATE INDEX parties_ix1 ON parties (upper(description));

BEGIN TRANSACTION;
  delete from parties;
  insert into parties (id, description)
  select c.party_id, trim('"' FROM (i.eligibility_data->'farmDescription')::text)
    from contracts c inner join indicators i on i.contract_id =c.id
   where now() between i.dt_insert and i.dt_delete
     and trim('"' FROM (i.eligibility_data->'farmDescription')::text) is not null;
COMMIT;
