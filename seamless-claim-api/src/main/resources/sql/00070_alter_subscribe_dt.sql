--liquibase formatted sql  

--changeset niva:70

ALTER TABLE contract_types ALTER COLUMN subscribe_from drop not null;
ALTER TABLE contract_types ALTER COLUMN subscribe_to drop not null;

begin transaction;
  update contract_types set subscribe_from=null, subscribe_to=null;
commit;
