--liquibase formatted sql  

--changeset niva:60

CREATE TABLE contracts_sequences (
  campaign int NOT NULL,
  seq int NOT NULL,
  CONSTRAINT contracts_sequences_pk PRIMARY KEY (campaign)
);
