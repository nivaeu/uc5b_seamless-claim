--liquibase formatted sql  

--changeset niva:80

ALTER TABLE contracts_created_queue ADD is_error boolean DEFAULT FALSE NOT NULL;
ALTER TABLE published_to_monitoring_queue ADD is_error boolean DEFAULT FALSE NOT NULL;

CREATE TABLE contracts_to_monitor_queue (
  id bigserial NOT NULL,
  dt_insert timestamp WITH TIME ZONE NOT NULL,
  dt_lock timestamp WITH TIME ZONE,
  is_error boolean DEFAULT FALSE NOT NULL,
  doc jsonb,
  CONSTRAINT contracts_to_monitor_queue_pk PRIMARY KEY (id)
);

CREATE TABLE contracts_anomalies_queue (
  id bigserial NOT NULL,
  dt_insert timestamp WITH TIME ZONE NOT NULL,
  dt_lock timestamp WITH TIME ZONE,
  is_error boolean DEFAULT FALSE NOT NULL,
  doc jsonb,
  CONSTRAINT contracts_anomalies_pk PRIMARY KEY (id)
);
