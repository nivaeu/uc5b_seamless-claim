--liquibase formatted sql  

--changeset niva:50

CREATE TABLE contracts_created_queue (
  id bigserial NOT NULL,
  dt_insert timestamp WITH TIME ZONE NOT NULL,
  dt_lock timestamp WITH TIME ZONE,
  doc jsonb,
  CONSTRAINT contracts_created_queue_pk PRIMARY KEY (id)
);

CREATE TABLE published_to_monitoring_queue (
  id bigserial NOT NULL,
  dt_insert timestamp WITH TIME ZONE NOT NULL,
  dt_lock timestamp WITH TIME ZONE,
  doc jsonb,
  CONSTRAINT published_to_monitoring_queue_pk PRIMARY KEY (id)
);
