--liquibase formatted sql  

--changeset niva:10

create table contract_types (
	id varchar(50) not null,
	short_descr varchar(100) not null,
	valid_from timestamp with time zone not null,
	valid_to timestamp with time zone not null,
	subscribe_from timestamp with time zone not null,
	subscribe_to timestamp with time zone not null,
	reference_date timestamp with time zone not null,
	duration_years integer not null,
	constraint contract_types_pk primary key (id)
);

COMMENT ON COLUMN contract_types.subscribe_from IS 'Start of subscription period, the year will not be considered but replaced with the current system clock year';
COMMENT ON COLUMN contract_types.subscribe_to IS 'End of subscription period, the year will not be considered but replaced with the current system clock year';
COMMENT ON COLUMN contract_types.reference_date IS 'Reference date at which data will be asked to the external systems, the year will not be considered but replaced with the current system clock year';

create table contracts (
	id varchar(50) not null,
	contract_type_id varchar(50) not null,
	party_id varchar(50) not null,
	subscribtion_dt timestamp with time zone not null,
	subscribed_by varchar(100) not null,
	status_code	varchar(20) not null,
	constraint contracts_pk primary key (id),
	constraint contracts_fk1 foreign key (contract_type_id) references contract_types (id)
);

create index contracts_ix1 on contracts (contract_type_id);
create index contracts_ix2 on contracts (party_id);

create table contracts_land_uses (
	contract_id varchar(50) not null,
	parcel_land_use_id varchar(50) not null,
	descr varchar(1000) not null,
	area_sqm integer not null,
	monitoring_status varchar(20) not null,
	constraint contracts_land_uses_pk primary key (contract_id, parcel_land_use_id)
);