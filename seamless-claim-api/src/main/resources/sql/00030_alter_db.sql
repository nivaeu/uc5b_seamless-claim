--liquibase formatted sql  

--changeset niva:30

create table contracts_aud (
	id varchar(50) not null,
	change_dt timestamp with time zone not null,
	contract_type_id varchar(50) not null,
	party_id varchar(50) not null,
	subscribtion_dt timestamp with time zone not null,
	subscribed_by varchar(100) not null,
	status_code	varchar(20) not null,
	constraint contracts_aud_pk primary key (id, change_dt),
	constraint contracts_aud_fk1 foreign key (contract_type_id) references contract_types (id)
);

insert into contracts_aud 
  select id, subscribtion_dt, contract_type_id, party_id, subscribtion_dt, subscribed_by, status_code
    from contracts