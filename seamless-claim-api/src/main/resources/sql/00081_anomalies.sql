--liquibase formatted sql  

--changeset niva:81

CREATE TABLE contract_anomalies (
  id bigserial NOT NULL,
  contract_id varchar(50) NOT NULL,
  dt_insert timestamp WITH time ZONE NOT NULL,
  dt_delete timestamp WITH time ZONE NOT NULL,
  message varchar NOT NULL,
  CONSTRAINT contract_anomalies_pk PRIMARY KEY (id),
  CONSTRAINT contract_anomalies_fk1 FOREIGN KEY (contract_id) REFERENCES contracts (id)
);

CREATE INDEX contract_anomalies_ix1 ON contract_anomalies (contract_id);
