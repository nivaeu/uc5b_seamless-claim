--liquibase formatted sql  

--changeset niva:71

ALTER TABLE contracts ADD expiry_date timestamp WITH time ZONE;
UPDATE contracts SET expiry_date = date_trunc('day', subscribtion_dt + interval '5 year');
ALTER TABLE contracts alter expiry_date set not null;

ALTER TABLE contracts_aud ADD expiry_date timestamp WITH time ZONE;
UPDATE contracts_aud SET expiry_date = date_trunc('day', subscribtion_dt + interval '5 year');
ALTER TABLE contracts_aud alter expiry_date set not null;
