# seamless-claim-api


How to start the database as a container
---

The following steps will start a docker container:
1. `cd docker/db`
1. Run `docker-compose up -d`
1. You then need to performa a DB migration to create the needed data structures

How to start the seamless-claim-api application
---

1. Run `mvn clean package` to build your application
1. Perform a db migration running `java -jar target/seamless-claim-api-<version>.jar db migrate config.yml`
1. Start application with `java -jar target/seamless-claim-api-<version>.jar server config.yml`
1. To check that your application is running enter url `http://localhost:8080`
1. Pelase note that no plugins are delivered as part of this repository, t odevelop a plugin follow the instructions in the seamless-claim-sdk module

Health Check
---

To see your applications health enter the url `http://localhost:8081/healthcheck`
