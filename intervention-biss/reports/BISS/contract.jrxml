<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="contract" pageWidth="595" pageHeight="842" columnWidth="435" leftMargin="80" rightMargin="80" topMargin="80" bottomMargin="0">
  <property name="ireport.zoom" value="2.0"/>
  <property name="ireport.x" value="0"/>
  <property name="ireport.y" value="192"/>
  <parameter name="SUBREPORT_DIR" class="java.lang.String" isForPrompting="false">
    <defaultValueExpression><![CDATA["C:\\Users\\jimmy\\Downloads\\"]]></defaultValueExpression>
  </parameter>
  <field name="AGENCY_COUNTRY" class="java.lang.String"/>
  <field name="FARM_COUNTRY" class="java.lang.String"/>
  <field name="AGENCY_ADDRESS" class="java.lang.String"/>
  <field name="FARM_ADDRESS" class="java.lang.String"/>
  <field name="CODE_AGREEMENT" class="java.lang.String"/>
  <field name="AGENCY_CODE" class="java.lang.String"/>
  <field name="FARMER_ID" class="java.lang.String"/>
  <field name="EU_REG_NUMBER" class="java.lang.String"/>
  <field name="FARM_DESC" class="java.lang.String"/>
  <field name="NAME_ROLE_FARM" class="java.lang.String"/>
  <field name="DATE_FARM" class="java.util.Date"/>
  <field name="AGENCY_NAME" class="java.lang.String"/>
  <field name="FARMER_NAME" class="java.lang.String"/>
  <background>
    <band splitType="Stretch"/>
  </background>
  <title>
    <band height="117" splitType="Stretch">
      <staticText>
        <reportElement positionType="Float" mode="Transparent" x="23" y="0" width="386" height="20"/>
        <textElement textAlignment="Center" markup="none">
          <font size="14" isBold="true" isItalic="false"/>
        </textElement>
        <text><![CDATA[BASIC INCOME SUPPORT SCHEME (BISS) Agreement]]></text>
      </staticText>
      <textField isStretchWithOverflow="true" isBlankWhenNull="true">
        <reportElement positionType="Float" mode="Transparent" x="60" y="49" width="311" height="57"/>
        <textElement textAlignment="Center" markup="html">
          <font size="11" isBold="true"/>
        </textElement>
        <textFieldExpression class="java.lang.String"><![CDATA["Code of Agreement No: "+$F{CODE_AGREEMENT}+"<br>"+
"Paying Agency: "+ $F{AGENCY_CODE} + " - " + $F{AGENCY_NAME} +"<br>"+
"Farm: "+ $F{FARMER_ID} + " - " + $F{FARMER_NAME}]]></textFieldExpression>
      </textField>
      <staticText>
        <reportElement positionType="Float" mode="Transparent" x="23" y="20" width="386" height="20"/>
        <textElement textAlignment="Center" markup="none">
          <font size="14" isBold="true" isItalic="false"/>
        </textElement>
        <text><![CDATA[Part A – General Conditions]]></text>
      </staticText>
    </band>
  </title>
  <detail>
    <band height="496" splitType="Stretch">
      <textField isStretchWithOverflow="true" isBlankWhenNull="true">
        <reportElement positionType="Float" mode="Transparent" x="0" y="0" width="435" height="20"/>
        <textElement textAlignment="Justified" markup="html"/>
        <textFieldExpression class="java.lang.String"><![CDATA["This digital agreement is made by and between:<br>"]]></textFieldExpression>
      </textField>
      <textField isStretchWithOverflow="true" isBlankWhenNull="true">
        <reportElement positionType="Float" mode="Transparent" x="0" y="121" width="435" height="20"/>
        <textElement textAlignment="Justified" markup="html"/>
        <textFieldExpression class="java.lang.String"><![CDATA["The Farm operates on land for agricultural activities as described in Article 4 of Regulation (EU) [CAP Strategic Plan Regulation] and maintains land.<br><br>"+"The Agency implements the European and National Policies as defined by the relevant Authorities and is in charge to provide support to Farm at the given capacity;<br><br>"+"The Parties intend to set forth the terms and conditions for the services in this Agreement."]]></textFieldExpression>
      </textField>
      <textField isStretchWithOverflow="true" isBlankWhenNull="true">
        <reportElement positionType="Float" mode="Transparent" x="0" y="167" width="435" height="20"/>
        <textElement textAlignment="Justified" markup="html"/>
        <textFieldExpression class="java.lang.String"><![CDATA["in consideration of the mutual promises herein contained, Parties agree as follows:<br><br>"+"<b>Subject</b><br><br>"+"The Paying Agency accredited according the annex I of Commission Delegated Regulation (EU) No 907/2014 commits to annually pay the Farm an amount of money based on the following Terms and Conditions.<br><br>"+"<b>Amount of Payment</b><br><br>"+"The amount of payment will be calculated based on the total area found by the monitoring system against the land listed in the FRGD. Respective to the area found, a payable area will be determined applying the payment rules<sup>1</sup>.<br><br>"+"The payable area will be compared against the available entitlements and the area found value will be multiplied by the value of the entitlements.<br><br>"+"The list of land and available entitlements is reported in Part B – Digital Agreement Valorisation and authorisation to annual payment<br><br>"+"<b>Paying Agency Obligations</b><br><br>"+"a. The PA commits to constantly monitor the agricultural parcels registered in the Farm Registry<br><br>"+"b. The PA commits to pay the Amount as form 16 October of each year on the Bank Account registered in the Farm Register or earlier if obligations are met earlier.<br><br>"]]></textFieldExpression>
      </textField>
      <staticText>
        <reportElement positionType="Float" mode="Transparent" x="0" y="101" width="435" height="20"/>
        <textElement textAlignment="Center"/>
        <text><![CDATA[WHEREAS]]></text>
      </staticText>
      <staticText>
        <reportElement positionType="Float" mode="Transparent" x="0" y="147" width="435" height="20"/>
        <textElement textAlignment="Center"/>
        <text><![CDATA[NOW, THEREFORE, ]]></text>
      </staticText>
      <textField isStretchWithOverflow="true" isBlankWhenNull="true">
        <reportElement positionType="Float" mode="Transparent" x="0" y="187" width="435" height="20"/>
        <textElement textAlignment="Justified" markup="html"/>
        <textFieldExpression class="java.lang.String"><![CDATA["<b>Farm Obligations</b><br><br>"+"a. The farm commits to maintain its Farm Register updated (i.e. system for the identification of beneficiaries of the interventions and measures as established by the article 64(1)(d) of the new CAP Horizontal Regulation).<br><br>"+"b. The farm commits to maintain the agricultural parcels in the Farm Register any agricultural area of the holding, during the year claim is referred, is used for an agricultural activity or, where the area is also used for non-agricultural activities, is predominantly used for agricultural activities, and which is at the farmer's disposal as established by the Article 4(c)(i) of the Regulation of the European Parliament and of the Council establishing rules on support for strategic plans to be drawn up by Member States under the Common agricultural policy (CAP Strategic Plans) and financed by the European Agricultural Guarantee Fund (EAGF) and by the European Agricultural Fund for Rural Development (EAFRD) and repealing Regulation (EU) No 1305/2013 of the European Parliament and of the Council and Regulation (EU) No 1307/2013 of the European Parliament and of the Council (new CAP Strategic Plan Regulation).<br><br>"+"c. The farm must be “genuine” as established as established by the Article 4(d) of the new CAP Strategic Plan Regulation.<br><br>"+"d. The farm commits to Cross-compliance conformity biodiversity GAECs 9 and 10 as established by the articles 11 and 12 of CAP Strategic Plan Regulation and to the eligible conditions as established by the Articles 65.1 and 65.6 of the new CAP Strategic plan regulation.<br><br>"+"e. The farm commits to approve the payment according with the Art. 63.4(f) of the new CAP Horizontal Regulation (“automatic claims system”).<br><br>"]]></textFieldExpression>
      </textField>
      <textField isStretchWithOverflow="true" isBlankWhenNull="true">
        <reportElement positionType="Float" mode="Transparent" x="0" y="207" width="435" height="20"/>
        <textElement textAlignment="Justified" markup="html"/>
        <textFieldExpression class="java.lang.String"><![CDATA["<b>Methods of Monitoring</b><br><br>"+"The following Conditions will be checked through monitoring using the following methods<br><br>"+"a. Presence of Agricultural Activity (ploughing, tilling, seeding, harvesting, etc.) through the methodology made the articles 40 and 40a of EU Regulation no 809/2014 (i.e. remote sensing and Copernicus Sentinels) and article 64(1)(c) of the new CAP Horizontal Regulation.<br><br>"+"b.  Evidence sent by the Farm also through Geotagged photos or new technologies (i.e. digital and electronic dashboard).<br><br>"+"c. Eligible maximum area with land cover/land use through the update LPIS according with the article 64(1)(a) of the new CAP Horizontal Regulation.<br><br>"+"d. Presence of entitlements allocated through the system for the identification and registration of payment entitlements as established by the article 64(1)(f) of the new CAP Horizontal Regulation, if applicable).<br><br>"]]></textFieldExpression>
      </textField>
      <textField isStretchWithOverflow="true" isBlankWhenNull="true">
        <reportElement positionType="Float" mode="Transparent" x="0" y="227" width="435" height="20"/>
        <textElement textAlignment="Justified" markup="html"/>
        <textFieldExpression class="java.lang.String"><![CDATA["<b>Remedies</b><br><br>"+"The Farmer can connect to the IACS system as established by the article 64 of the new CAP Horizontal Regulation at any time and visualise the items that may require additional information or amendments. S(he) can update the relevant information in the Farm Register or other systems. Such amendments will be re-evaluated to verify if conditions are met.<br><br>"+"<b>Duration</b><br><br>"+"These General Conditions are valid until 31/12/2027<br><br>"]]></textFieldExpression>
      </textField>
      <textField isStretchWithOverflow="true" isBlankWhenNull="true">
        <reportElement positionType="Float" mode="Transparent" x="0" y="247" width="435" height="20"/>
        <textElement textAlignment="Justified" markup="html"/>
        <textFieldExpression class="java.lang.String"><![CDATA["<b>For Acceptance</b><br><br>"]]></textFieldExpression>
      </textField>
      <textField>
        <reportElement positionType="Float" x="0" y="267" width="215" height="20"/>
        <textElement>
          <font isBold="true"/>
        </textElement>
        <textFieldExpression class="java.lang.String"><![CDATA["Farm"]]></textFieldExpression>
      </textField>
      <textField>
        <reportElement positionType="Float" x="0" y="287" width="141" height="20"/>
        <textElement/>
        <textFieldExpression class="java.lang.String"><![CDATA[$F{FARMER_ID}]]></textFieldExpression>
      </textField>
      <textField>
        <reportElement positionType="Float" x="0" y="357" width="141" height="20"/>
        <textElement/>
        <textFieldExpression class="java.lang.String"><![CDATA[$F{NAME_ROLE_FARM}]]></textFieldExpression>
      </textField>
      <textField>
        <reportElement positionType="Float" x="0" y="337" width="215" height="20"/>
        <textElement>
          <font isBold="true"/>
        </textElement>
        <textFieldExpression class="java.lang.String"><![CDATA["Name and role (printed)"]]></textFieldExpression>
      </textField>
      <textField>
        <reportElement positionType="Float" x="0" y="408" width="141" height="20"/>
        <textElement/>
        <textFieldExpression class="java.lang.String"><![CDATA[new SimpleDateFormat("dd/MM/yyyy").format($F{DATE_FARM})]]></textFieldExpression>
      </textField>
      <textField>
        <reportElement positionType="Float" x="0" y="388" width="215" height="20"/>
        <textElement>
          <font isBold="true"/>
        </textElement>
        <textFieldExpression class="java.lang.String"><![CDATA["Date:"]]></textFieldExpression>
      </textField>
      <textField>
        <reportElement positionType="Float" x="220" y="267" width="215" height="20"/>
        <textElement>
          <font isBold="true"/>
        </textElement>
        <textFieldExpression class="java.lang.String"><![CDATA["Agency"]]></textFieldExpression>
      </textField>
      <textField>
        <reportElement positionType="Float" x="220" y="287" width="215" height="20"/>
        <textElement>
          <font isBold="true"/>
        </textElement>
        <textFieldExpression class="java.lang.String"><![CDATA[$F{AGENCY_CODE} + " - " + $F{AGENCY_NAME}]]></textFieldExpression>
      </textField>
      <textField>
        <reportElement positionType="Float" x="0" y="442" width="215" height="20"/>
        <textElement>
          <font isBold="true"/>
        </textElement>
        <textFieldExpression class="java.lang.String"><![CDATA["Digitally signed by:"]]></textFieldExpression>
      </textField>
      <textField>
        <reportElement positionType="Float" x="0" y="462" width="141" height="20"/>
        <textElement/>
        <textFieldExpression class="java.lang.String"><![CDATA[$F{NAME_ROLE_FARM}]]></textFieldExpression>
      </textField>
      <textField>
        <reportElement positionType="Float" x="0" y="307" width="141" height="20"/>
        <textElement/>
        <textFieldExpression class="java.lang.String"><![CDATA[$F{FARMER_NAME}]]></textFieldExpression>
      </textField>
      <textField isStretchWithOverflow="true" isBlankWhenNull="true">
        <reportElement positionType="Float" mode="Transparent" x="0" y="80" width="435" height="20"/>
        <textElement textAlignment="Justified" markup="html"/>
        <textFieldExpression class="java.lang.String"><![CDATA["The Farm and the Agency may be referred to herein individually as a “Party” and collectively as the “Parties”."]]></textFieldExpression>
      </textField>
      <textField isStretchWithOverflow="true" isBlankWhenNull="true">
        <reportElement positionType="Float" mode="Transparent" x="23" y="20" width="412" height="60"/>
        <textElement textAlignment="Justified" markup="html"/>
        <textFieldExpression class="java.lang.String"><![CDATA["<b>The Paying Agency (Party A)</b> - a public authority in Italy and having its registered office at "+$F{AGENCY_ADDRESS}+" - 00185 Roma (the <b>“Agency”</b>)<br><br>" + "     <b>The Farm (Party B)</b> - a private company with limited liability, incorporated in Italy and having its registered address at "+$F{FARM_ADDRESS}+" (the <b>“Farm”</b>)<br>"]]></textFieldExpression>
      </textField>
    </band>
  </detail>
  <pageFooter>
    <band height="60">
      <textField isStretchWithOverflow="true" isBlankWhenNull="true">
        <reportElement positionType="Float" mode="Transparent" x="0" y="0" width="435" height="20"/>
        <textElement textAlignment="Justified" markup="html">
          <font size="8" isUnderline="false"/>
        </textElement>
        <textFieldExpression class="java.lang.String"><![CDATA["________________________________________________________________________________________________<br>"+
"1* if the area found is less than a minimum threshold, for example it is 0.1 ha and the minimum threshold is 0.2 ha, the payable area becomes 0 (zero)."]]></textFieldExpression>
      </textField>
    </band>
  </pageFooter>
</jasperReport>
