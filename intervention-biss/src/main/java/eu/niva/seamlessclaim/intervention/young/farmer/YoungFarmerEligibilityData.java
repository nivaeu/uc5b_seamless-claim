package eu.niva.seamlessclaim.intervention.young.farmer;

/*-
 * #%L
 * intervention-biss
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import eu.niva.seamlessclaim.intervention.biss.BISSEligibilityData;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class YoungFarmerEligibilityData extends BISSEligibilityData
{

	private int farmerAge;
	
	// From rules
	private Boolean isYoungFarmer = false;

}
