package eu.niva.seamlessclaim.intervention.young.farmer;

/*-
 * #%L
 * intervention-biss
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import eu.niva.seamlessclaim.drools.service.DroolsService;
import eu.niva.seamlessclaim.drools.service.DroolsServiceImpl;
import eu.niva.seamlessclaim.drools.service.FireRulesRequest;
import eu.niva.seamlessclaim.intervention.biss.BISSMonitoringData;
import eu.niva.seamlessclaim.intervention.biss.BISSPaymentData;
import eu.niva.seamlessclaim.intervention.biss.EligibleLandUse;
import eu.niva.seamlessclaim.intervention.biss.Entitlement;
import eu.niva.seamlessclaim.intervention.biss.NumberHolder;
import eu.niva.seamlessclaim.intervention.biss.report.AgreementAnnexBean;
import eu.niva.seamlessclaim.intervention.biss.report.AgreementBean;
import eu.niva.seamlessclaim.intervention.biss.report.AgreementEntlBean;
import eu.niva.seamlessclaim.intervention.biss.report.AgreementEntlSumBean;
import eu.niva.seamlessclaim.intervention.biss.report.ContractBean;
import eu.niva.seamlessclaim.jasper.service.JasperService;
import eu.niva.seamlessclaim.jasper.service.JasperServiceImpl;
import eu.niva.seamlessclaim.sdk.BaseParameters;
import eu.niva.seamlessclaim.sdk.Config;
import eu.niva.seamlessclaim.sdk.datapull.loader.FarmLoadParameters;
import eu.niva.seamlessclaim.sdk.datapull.loader.LoadResults;
import eu.niva.seamlessclaim.sdk.datapull.loader.PartyLoader;
import eu.niva.seamlessclaim.sdk.datapull.model.Address;
import eu.niva.seamlessclaim.sdk.datapull.model.FarmWithDetails;
import eu.niva.seamlessclaim.sdk.intervention.ComputeIndicatorsParameters;
import eu.niva.seamlessclaim.sdk.intervention.ContractData;
import eu.niva.seamlessclaim.sdk.intervention.InterventionService;
import eu.niva.seamlessclaim.sdk.intervention.PaymentCalculationResult;

public abstract class YoungFarmerInterventionService implements InterventionService<YoungFarmerEligibilityData, BISSMonitoringData, BISSPaymentData>
{
	private static final Set<String> SUPPORTED_INTERVENTION = Collections.unmodifiableSet(new HashSet<>(Arrays.asList("YOUNG_FARMER")));
	
	private DroolsService droolsService;
	protected JasperService jasperService;

	private String paCity;
	private String paCode;
	private String paName;
	private String paCountry;
	private String paAddress;
	
	/**
	 * Configuration method to be overridden by implementors
	 * @see #configure(Config)
	 */
	protected abstract void doConfigure(Config config) throws Exception;

	/**
	 * Gets the party loader to use
	 * @return the PartyLoader
	 */
	protected abstract PartyLoader getPartyLoader();
	
	protected DroolsService getDroolsService()
	{
		return droolsService;
	}
	
	@Override
	public Class<YoungFarmerEligibilityData> getEligibilityDataClass()
	{
		return YoungFarmerEligibilityData.class;
	}

	@Override
	public Class<BISSMonitoringData> getMonitoringDataClass()
	{
		return BISSMonitoringData.class;
	}

	@Override
	public Class<BISSPaymentData> getPaymentDataClass()
	{
		return BISSPaymentData.class;
	}

	@Override
	public Set<String> getSupportedInterventionTypes()
	{
		return SUPPORTED_INTERVENTION;
	}

	@Override
	public void generateContractPDF(BaseParameters params, ContractData contract, YoungFarmerEligibilityData eligibilityData, OutputStream out) throws Exception
	{
		ContractBean contractBean = new ContractBean();
		Date reportDate = new Date();
		contractBean.setAGENCY_ADDRESS(paAddress);
		contractBean.setAGENCY_CODE(paCode);
		contractBean.setAGENCY_COUNTRY(paCountry);
		contractBean.setAGENCY_NAME(paName);
		contractBean.setCODE_AGREEMENT(contract.getId());
		contractBean.setDATE_AGENCY(reportDate);
		contractBean.setDATE_FARM(reportDate);
		contractBean.setEU_REG_NUMBER("XXX");
		contractBean.setFARM_ADDRESS(getFarmAddress(params, contract.getPartyId()));
		contractBean.setFARM_COUNTRY(null);
		contractBean.setFARM_DESC(contract.getPartyDescription());
		contractBean.setFARMER_ID(contract.getPartyId());
		contractBean.setFARMER_NAME(contract.getPartyDescription());
		contractBean.setNAME_ROLE_AGENCY("");
		contractBean.setNAME_ROLE_FARM(contract.getSubscribedBy());

		List<ContractBean> list = new ArrayList<ContractBean>();
		list.add(contractBean);
		jasperService.generateReport("YOUNG_FARMER/contract.jrxml", list, out);
	}
	
	@Override
	public void generatePaymentPDF(BaseParameters params, ContractData contract, YoungFarmerEligibilityData eligibilityData,
		BISSMonitoringData monitoringData, BISSPaymentData paymentData, OutputStream out) throws Exception
	{
		List<AgreementBean> agreementBeanList = new ArrayList<AgreementBean>();
		AgreementBean agreementBean = new AgreementBean();
		Date reportDate = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(reportDate);
		
		agreementBean.setCONTRACT_TYPE_ID(contract.getId());
		agreementBean.setDECL_ELIGIBLE_AREA(new BigDecimal(eligibilityData.getDeclaredEligibleAreaSqm() / 10_000)
			.setScale(2, RoundingMode.HALF_UP).doubleValue());
		agreementBean.setFARM_ID(contract.getPartyId());
		agreementBean.setFARM_NAME(contract.getPartyDescription());
		agreementBean.setFOUND_AREA(new BigDecimal(monitoringData.getAreaFoundSqm() / 10_000)
			.setScale(2, RoundingMode.HALF_UP).doubleValue());
		agreementBean.setIS_ACTIVE_FARMER(eligibilityData.getGenuineFarmer() ? "YES" : "NO");
		agreementBean.setIS_CC_COMPLIANT("C".equals(monitoringData.getCrossComplianceConformity()) ? "YES" : "NO");
		agreementBean.setIS_ELIGIBLE("NO");
		if (eligibilityData.getActiveFarmer() && eligibilityData.getEnoughLandUse()
			&& eligibilityData.getHavePaymentRights() && eligibilityData.getGenuineFarmer())
		{
			agreementBean.setIS_ELIGIBLE("YES");
		}
		agreementBean.setNET_AMOUNT(paymentData.getNetAmount());
		agreementBean.setPA_CITY(paCity);
		agreementBean.setPAYABLE_AREA(new BigDecimal(paymentData.getAreaPayableSqm() / 10_000)
			.setScale(2, RoundingMode.HALF_UP).doubleValue());
		agreementBean.setPAYMENT_DATE(reportDate);
		double payRights = eligibilityData.getEntitlements().stream().mapToDouble(e -> e.getValue()).sum();
		agreementBean.setPAYMENT_RIGHTS_VALUE(new BigDecimal(payRights)
			.setScale(2, RoundingMode.HALF_UP).doubleValue());
		agreementBean.setPAYMENT_YEAR(calendar.get(Calendar.YEAR));

		int i = 0;
		AgreementAnnexBean agreementAnnexBean;
		for (EligibleLandUse elu : eligibilityData.getLandUses())
		{
			agreementAnnexBean = new AgreementAnnexBean(++i, elu.getParcelDescription(),
				elu.getLandUseDescription(), elu.getAreaSqm() / 10_000);
			agreementBean.getAGREEMENT_ANNEX_LIST().add(agreementAnnexBean);
		}

		AgreementEntlBean agreementEntlBean;
		for (Entitlement ent : eligibilityData.getEntitlements())
		{
			agreementEntlBean = new AgreementEntlBean(ent.getCode(), ent.getValue(),
				ent.getAreaSqm() / 10_000);
			agreementBean.getAGREEMENT_ENTL_LIST().add(agreementEntlBean);
		}
		
		Map<String, List<Entitlement>> groups = eligibilityData.getEntitlements()
			.stream()
			.collect(groupingBy(e -> e.getValue() + "*" + e.getAreaSqm()));
		
		List<AgreementEntlSumBean> aggregated = groups.entrySet()
			.stream()
			.map(e -> new AgreementEntlSumBean(e.getValue()))
			.collect(toList());
		
		agreementBean.setAGREEMENT_ENTL_SUM_LIST(aggregated);
		
		LoadResults<String> iban = getPartyLoader()
			.getIban(new FarmLoadParameters(params, contract.getPartyId(), OffsetDateTime.now()));
		agreementBean.setIBAN(iban.getData());
		
		agreementBeanList.add(agreementBean);
		jasperService.generateReport("YOUNG_FARMER/pay/young_farmer_agreement.jrxml", agreementBeanList, out);
	}
	
	@Override
	public PaymentCalculationResult<BISSPaymentData> calculatePayment(ComputeIndicatorsParameters params,
		YoungFarmerEligibilityData indicatorsData, BISSMonitoringData monitoringData)
	{
        double areaPayable = nvl(indicatorsData.getPaymentRightsAreaSqm());
        // areaPayable = Math.min(areaPayable, nvl(indicatorsData.getDeclaredEligibleAreaSqm()));
        areaPayable = Math.min(areaPayable, nvl(monitoringData.getAreaFoundSqm()));
       
		// Get payment rate from the rule
        NumberHolder holder = new NumberHolder();
		Map<String, Object> vars = new HashMap<>();
		vars.put("holder", holder);
		getDroolsService().fireRules(new FireRulesRequest("YOUNG_FARMER/payment-rate.xls", new ArrayList<>(0), vars));
       
		double paymentRate = holder.getNumber().doubleValue();
		
		BISSPaymentData ret = new BISSPaymentData();
        ret.setAreaPayableSqm(areaPayable);
        ret.setGrossAmount(areaPayable / 10_000 * paymentRate);
        ret.setNetAmount(areaPayable / 10_000 * paymentRate);
       
        return new PaymentCalculationResult<>(ret);
	}

	@Override
	public final void configure(Config config) throws Exception
	{
		if (jasperService == null)
			createJasperService(config);

		if (droolsService == null)
			createDroolsService(config);

		paCity = config.getProperty("paCity").orElse("");
		paCode = config.getProperty("paCode").orElse("");
		paName = config.getProperty("paName").orElse("");
		paCountry = config.getProperty("paCountry").orElse("");
		paAddress = config.getProperty("paAddress").orElse("");

		doConfigure(config);		
	}
	
	protected String getFarmAddress(BaseParameters params, String farmId)
	{
		FarmLoadParameters flp = new FarmLoadParameters(params, farmId, OffsetDateTime.now());
		LoadResults<FarmWithDetails> farm = getPartyLoader().getFarm(flp);
		String ret = "";
		Address address = farm.getData().getAddress();
		if (address != null)
			ret = address.getAllLines();
		
		return ret;
	}

	private void createJasperService(Config config) throws Exception
	{
		String dir = config.getProperty("reportDefinitionsBaseDirectory")
			.orElseThrow(() -> new IllegalStateException("Property reportDefinitionsBaseDirectory not set in pluginsConfiguration"));
		
		jasperService =  new JasperServiceImpl(new File(dir));
	}

	private void createDroolsService(Config config) throws IOException
	{
		String dir = config.getProperty("rulesBaseDirectory")
			.orElseThrow(() -> new IllegalStateException("Property rulesBaseDirectory not set in pluginsConfiguration"));
		
		File file = new File(dir);
		if (!file.exists())
			throw new IOException(String.format("%s does not exist", dir));
		if (!file.isDirectory())
			throw new IOException(String.format("%s is not a directory", dir));
		if (!file.canRead())
			throw new IOException(String.format("%s is not readable", dir));
		
		droolsService = new DroolsServiceImpl(file);
	}

	protected double nvl(Double val)
	{
		return val == null ? 0 : val;
	}

}
