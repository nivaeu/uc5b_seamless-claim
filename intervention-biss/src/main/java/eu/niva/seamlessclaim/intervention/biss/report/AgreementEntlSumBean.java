package eu.niva.seamlessclaim.intervention.biss.report;

/*-
 * #%L
 * intervention-biss
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import static java.util.stream.Collectors.summingDouble;

import java.util.List;

import eu.niva.seamlessclaim.intervention.biss.Entitlement;
import lombok.Getter;

@Getter
public class AgreementEntlSumBean
{
	private Double AREA;
	private Double VALUE;
	private Integer QUANTITY;
	private Double TOTAL_AREA;
	private Double TOTAL_VALUE;
	
	public AgreementEntlSumBean(List<Entitlement> group)
	{
		this.AREA = group.get(0).getAreaSqm() / 10_000;
		this.VALUE = group.get(0).getValue();
		this.QUANTITY = group.size();
		this.TOTAL_VALUE = VALUE * QUANTITY;
		this.TOTAL_AREA = group.stream()
			.map(e -> e.getAreaSqm() / 10_000)
			.collect(summingDouble(Double::doubleValue));
	}

}
