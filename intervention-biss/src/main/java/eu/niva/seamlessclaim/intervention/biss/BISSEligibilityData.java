package eu.niva.seamlessclaim.intervention.biss;

/*-
 * #%L
 * intervention-biss
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import eu.niva.seamlessclaim.sdk.intervention.EligibilityData;
import lombok.Data;

@Data
public class BISSEligibilityData implements EligibilityData
{
	private String farmDescription;
	private Boolean activeFarmer;
	private Double declaredEligibleAreaSqm;
	private Double paymentRightsAreaSqm;
	private Double paymentRightsValue;
	
	private List<EligibleLandUse> landUses = new ArrayList<>(0);
	private List<Entitlement> entitlements = new ArrayList<>(0);
	
	// From rules
	private Boolean genuineFarmer = false;
	private Boolean landUseDeclared = false;
	private Boolean enoughLandUse = false;
	private Boolean havePaymentRights = false;

}
