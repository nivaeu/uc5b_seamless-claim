package eu.niva.seamlessclaim.intervention.biss;

/*-
 * #%L
 * intervention-biss
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import eu.niva.seamlessclaim.sdk.intervention.MonitoringData;
import eu.niva.seamlessclaim.sdk.intervention.MonitoringState;
import lombok.Data;

@Data
public class BISSMonitoringData implements MonitoringData
{
	private MonitoringState state;
	private String monitoringSystemId;
	private Double areaFoundSqm;
	
	// From NIDAS
	private Long nidasId;
	private Double qualityIndex;
	
	// From GAEC
	private Double crossComplianceReduction;
	private String crossComplianceConformity;

}
