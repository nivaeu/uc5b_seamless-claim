package eu.niva.seamlessclaim.intervention.biss.report;

/*-
 * #%L
 * intervention-biss
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AgreementEntlBean {

	private String DESCRIPTION;
	private Double VALUE;
	private Double AREA;

}
