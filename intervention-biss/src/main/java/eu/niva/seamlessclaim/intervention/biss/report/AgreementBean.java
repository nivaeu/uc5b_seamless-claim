package eu.niva.seamlessclaim.intervention.biss.report;

/*-
 * #%L
 * intervention-biss
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class AgreementBean {
	private Integer PAYMENT_YEAR;
	private String FARM_ID;
	private String FARM_NAME;
	private String IBAN;
	private String CONTRACT_TYPE_ID;
	private Double NET_AMOUNT;
	private Date PAYMENT_DATE;
	private Double DECL_ELIGIBLE_AREA;
	private Double FOUND_AREA;
	private Double PAYABLE_AREA;
	private String IS_ACTIVE_FARMER;
	private String IS_CC_COMPLIANT;
	private String IS_ELIGIBLE;
	private Double PAYMENT_RIGHTS_VALUE;
	private String PA_CITY;
	private String AGENCY_CODE;
	private String AGENCY_NAME;
	private String NAME_ROLE_FARM;

	private List<AgreementEntlSumBean> AGREEMENT_ENTL_SUM_LIST = new ArrayList<>();
	private List<AgreementEntlBean> AGREEMENT_ENTL_LIST = new ArrayList<>();
	private List<AgreementAnnexBean> AGREEMENT_ANNEX_LIST = new ArrayList<>();
}
