package eu.niva.seamlessclaim.intervention.biss.report;

/*-
 * #%L
 * intervention-biss
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.Date;

import lombok.Data;

@Data
public class ContractBean {
	private String AGENCY_COUNTRY;
	private String FARM_COUNTRY;
	private String AGENCY_ADDRESS;
	private String FARM_ADDRESS;
	private String FARMER_NAME;
	private String CODE_AGREEMENT;
	private String AGENCY_CODE;
	private String AGENCY_NAME;
	private String FARMER_ID;
	private String EU_REG_NUMBER;
	private String FARM_DESC;
	private String NAME_ROLE_FARM;
	private Date DATE_FARM;
	private String NAME_ROLE_AGENCY;
	private Date DATE_AGENCY;
}
