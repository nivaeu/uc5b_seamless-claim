package eu.niva.seamlessclaim.intervention.biss;

/*-
 * #%L
 * intervention-biss
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

public class BISSPaymentData
{
	private Double areaPayableSqm;
	private Double grossAmount;
	private Double netAmount;
	private Double crossComplianceReduction;
	
	public Double getAreaPayableSqm()
	{
		return areaPayableSqm;
	}
	public void setAreaPayableSqm(Double areaPayableSqm)
	{
		this.areaPayableSqm = areaPayableSqm;
	}
	public Double getGrossAmount()
	{
		return grossAmount;
	}
	public void setGrossAmount(Double grossAmount)
	{
		this.grossAmount = grossAmount;
	}
	public Double getNetAmount()
	{
		return netAmount;
	}
	public void setNetAmount(Double netAmount)
	{
		this.netAmount = netAmount;
	}
	public Double getCrossComplianceReduction() {
		return crossComplianceReduction;
	}
	public void setCrossComplianceReduction(Double crossComplianceReduction) {
		this.crossComplianceReduction = crossComplianceReduction;
	}
	
	
}
