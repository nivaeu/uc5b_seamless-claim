package eu.niva.seamlessclaim.intervention.biss;

/*-
 * #%L
 * intervention-biss
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.io.File;
import java.time.OffsetDateTime;
import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;

import eu.niva.seamlessclaim.intervention.biss.BISSEligibilityData;
import eu.niva.seamlessclaim.intervention.biss.BISSInterventionService;
import eu.niva.seamlessclaim.intervention.biss.BISSMonitoringData;
import eu.niva.seamlessclaim.jasper.service.JasperServiceImpl;
import eu.niva.seamlessclaim.sdk.BaseParameters;
import eu.niva.seamlessclaim.sdk.Config;
import eu.niva.seamlessclaim.sdk.datapull.loader.PartyLoader;
import eu.niva.seamlessclaim.sdk.intervention.CampaignReferenceDate;
import eu.niva.seamlessclaim.sdk.intervention.ComputeIndicatorsParameters;
import eu.niva.seamlessclaim.sdk.intervention.IndicatorsResult;
import eu.niva.seamlessclaim.sdk.intervention.MonitoringResult;

public class BISSInterventionServiceReport extends BISSInterventionService
{

	private File reportDir;
	
	public BISSInterventionServiceReport() throws Exception
	{
		reportDir = new File("reports");
		jasperService =  new JasperServiceImpl(reportDir);
	}

	@Override
	protected String getFarmAddress(BaseParameters params, String farmId)
	{
		return "This is the farm address";
	}
	
	@Override
	public @NonNull CampaignReferenceDate getCampaignReferenceDate(OffsetDateTime date)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public @NonNull IndicatorsResult<BISSEligibilityData> computeEligibilityIndicators(
		ComputeIndicatorsParameters params)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public @NonNull IndicatorsResult<BISSEligibilityData> computeEligibilityIndicators(
		ComputeIndicatorsParameters params, BISSEligibilityData data)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public @NonNull MonitoringResult<BISSMonitoringData> computeMonitoringResult(ComputeIndicatorsParameters params,
		BISSEligibilityData eligibilityData, String monitoringSystemId)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public @NonNull String registerToMonitoringSystem(ComputeIndicatorsParameters params)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void sendNotification(String farmId, List<String> message)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public boolean canReadFarmData(String userId, String farmId)
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean canWriteFarmData(String userId, String farmId)
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isConfigured()
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected PartyLoader getPartyLoader()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void doConfigure(Config config) throws Exception
	{
		// TODO Auto-generated method stub
		
	}

}
