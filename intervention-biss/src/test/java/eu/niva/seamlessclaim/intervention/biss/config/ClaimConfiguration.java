package eu.niva.seamlessclaim.intervention.biss.config;

/*-
 * #%L
 * intervention-biss
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

public class ClaimConfiguration extends Configuration
{

	@Valid @NotNull private DataSourceFactory database;
	@Valid private PluginsConfiguration pluginsConfiguration;
	@Valid @NotNull private AsyncExecutorConfig asyncExecutorConfig;
	@Valid @NotNull private AsyncExecutorConfig monitoringQueueExecutorConfig;

	@JsonProperty("database")
	public void setDataSourceFactory(DataSourceFactory factory)
	{
		this.database = factory;
	}

	@JsonProperty("database")
	public DataSourceFactory getDataSourceFactory()
	{
		return database;
	}

	public PluginsConfiguration getPluginsConfiguration()
	{
		return pluginsConfiguration;
	}

	public void setPluginsConfiguration(PluginsConfiguration pluginsConfiguration)
	{
		this.pluginsConfiguration = pluginsConfiguration;
	}

	public AsyncExecutorConfig getAsyncExecutorConfig()
	{
		return asyncExecutorConfig;
	}

	@JsonProperty("asyncExecutor")
	public void setAsyncExecutorConfig(AsyncExecutorConfig config)
	{
		this.asyncExecutorConfig = config;
	}
	
	public AsyncExecutorConfig getMonitoringQueueExecutorConfig()
	{
		return monitoringQueueExecutorConfig;
	}

	@JsonProperty("monitoringQueueExecutor")
	public void setMonitoringQueueExecutorConfig(AsyncExecutorConfig config)
	{
		this.monitoringQueueExecutorConfig = config;
	}	
	
}
