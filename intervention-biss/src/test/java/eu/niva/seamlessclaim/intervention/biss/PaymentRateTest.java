package eu.niva.seamlessclaim.intervention.biss;

/*-
 * #%L
 * intervention-biss
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

import eu.niva.seamlessclaim.drools.service.DroolsService;
import eu.niva.seamlessclaim.drools.service.DroolsServiceImpl;
import eu.niva.seamlessclaim.drools.service.FireRulesRequest;
import eu.niva.seamlessclaim.intervention.biss.NumberHolder;

public class PaymentRateTest
{
	File ruleFile = new File("rules/BISS");

	@Test
	public void whenRuleIsFired_ThenReturnsPaymentRate() throws Exception
	{
		NumberHolder holder = new NumberHolder();
		Map<String, Object> vars = new HashMap<String, Object>();
		vars.put("holder", holder);

		DroolsService droolsService = new DroolsServiceImpl(ruleFile);
		FireRulesRequest fireRulesRequest = new FireRulesRequest("payment-rate.xls", new ArrayList<>(), vars);
		droolsService.fireRules(fireRulesRequest);
		assertNotNull(holder.getNumber());
	}

}
