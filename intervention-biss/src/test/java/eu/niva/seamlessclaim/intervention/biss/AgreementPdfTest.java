package eu.niva.seamlessclaim.intervention.biss;

/*-
 * #%L
 * intervention-biss
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayOutputStream;
import java.util.Locale;

import org.junit.jupiter.api.Test;

import eu.niva.seamlessclaim.intervention.biss.BISSEligibilityData;
import eu.niva.seamlessclaim.intervention.biss.BISSMonitoringData;
import eu.niva.seamlessclaim.intervention.biss.BISSPaymentData;
import eu.niva.seamlessclaim.intervention.biss.EligibleLandUse;
import eu.niva.seamlessclaim.intervention.biss.Entitlement;
import eu.niva.seamlessclaim.sdk.BaseParameters;
import eu.niva.seamlessclaim.sdk.intervention.ContractData;

public class AgreementPdfTest {
	private static final BaseParameters PARAMS = new BaseParameters("TEST", Locale.ENGLISH);

	@Test
	public void WhenGenerateBissAgreementPDF_thenOutputStreamIsFilled() throws Exception {
		System.out.println("WhenGenerateBissAgreementPDF_thenOutputStreamIsFilled - START");
		BISSPaymentData paymentData = new BISSPaymentData();
		BISSMonitoringData monitoringData = new BISSMonitoringData();
		BISSEligibilityData eligibilityData = new BISSEligibilityData();
		ContractData contractData = new ContractData();
		contractData.setPartyId("PartyId-123");
		contractData.setPartyDescription("PartyDescription-123");
		contractData.setSubscribedBy("SubscribedBy-123");
		contractData.setId("Id-123");
		
		eligibilityData.setDeclaredEligibleAreaSqm(345.60);
		eligibilityData.setGenuineFarmer(true);
		eligibilityData.setActiveFarmer(true);
		eligibilityData.setEnoughLandUse(true);
		eligibilityData.setHavePaymentRights(true);
		
		monitoringData.setAreaFoundSqm(678.5);
		
		paymentData.setNetAmount(25000.5);
		paymentData.setAreaPayableSqm(125600.8);
		
		Entitlement entitlementData = new Entitlement();
		entitlementData.setCode("Code-123");
		entitlementData.setAreaSqm(2000.500);
		entitlementData.setValue(35.05);		
		eligibilityData.getEntitlements().add(entitlementData);
		entitlementData = new Entitlement();
		entitlementData.setAreaSqm(8000.500);
		entitlementData.setValue(55.00);		
		entitlementData.setCode("Code-456");		
		eligibilityData.getEntitlements().add(entitlementData);	
		
		EligibleLandUse eligibleLandUseData = new EligibleLandUse();
		eligibleLandUseData.setAreaSqm(16800.50);
		eligibleLandUseData.setLandUseDescription("LandUseDescription-123");
		eligibleLandUseData.setParcelDescription("LandParcelDescription-123");
		eligibilityData.getLandUses().add(eligibleLandUseData);
		eligibleLandUseData = new EligibleLandUse();
		eligibleLandUseData.setAreaSqm(40000.50);
		eligibleLandUseData.setLandUseDescription("LandUseDescription-456");
		eligibleLandUseData.setParcelDescription("LandParcelDescription-789");
		eligibilityData.getLandUses().add(eligibleLandUseData);
		
		// OutputStream pdfOut = new FileOutputStream("E:\\test\\Agreement.pdf");
		ByteArrayOutputStream pdfOut = new ByteArrayOutputStream();
		BISSInterventionServiceReport bissInterventionServiceReport = new BISSInterventionServiceReport();
		bissInterventionServiceReport.generatePaymentPDF(PARAMS, contractData, eligibilityData, monitoringData, paymentData, pdfOut);
		assertTrue(pdfOut.size() > 1000);
		System.out.println("WhenGenerateBissAgreementPDF_thenOutputStreamIsFilled - END");
	}	
	
}
