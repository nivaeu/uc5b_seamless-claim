package eu.niva.seamlessclaim.intervention.biss;

/*-
 * #%L
 * intervention-biss
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import eu.niva.seamlessclaim.drools.service.DroolsService;
import eu.niva.seamlessclaim.drools.service.DroolsServiceImpl;
import eu.niva.seamlessclaim.drools.service.FireRulesRequest;
import eu.niva.seamlessclaim.intervention.biss.BISSEligibilityData;
import eu.niva.seamlessclaim.sdk.intervention.EligibilityResult;

public class EligibilityRulesSetTest {
	File ruleFile;

	@BeforeEach
	public void init() {
		ruleFile = new File("rules/BISS");
	}

	@Test
	public void WhenRulesAreFired_ThenSetEligibilityFactValuesToTrue() throws Exception  {

		ArrayList<Object> bissEligibilityDataList = new ArrayList<Object>();

		BISSEligibilityData bissEligibilityData = new BISSEligibilityData();
		bissEligibilityData.setActiveFarmer(true);
		bissEligibilityData.setDeclaredEligibleAreaSqm(7000.0);
		bissEligibilityData.setPaymentRightsValue(101.0);
		bissEligibilityDataList.add(bissEligibilityData);

		List<EligibilityResult> interventionEligibilityList = new ArrayList<EligibilityResult>();
		List<EligibilityResult> interventionEligibilityPaymentList = new ArrayList<EligibilityResult>();
		Map<String, Object> varsMap = new HashMap<String, Object>();
		varsMap.put("eligibilityResultList", interventionEligibilityList);
		varsMap.put("eligibilityResultPaymentList", interventionEligibilityPaymentList);
		String locale = new String("EN");
		varsMap.put("locale", locale);

		DroolsService droolsService = new DroolsServiceImpl(ruleFile);
		FireRulesRequest fireRulesRequest = new FireRulesRequest("eligibility.xls", bissEligibilityDataList, varsMap);
		droolsService.fireRules(fireRulesRequest);
		//System.out.println(bissEligibilityData.toString());
		assertEquals(bissEligibilityData.getGenuineFarmer(), true);
		assertEquals(bissEligibilityData.getLandUseDeclared(), true);
		assertEquals(bissEligibilityData.getEnoughLandUse(), true);
		assertEquals(bissEligibilityData.getHavePaymentRights(), true);
	}

	@Test
	public void WhenRulesAreNotFired_ThenSetEligibilityFactValuesToFalse() throws Exception {

		ArrayList<Object> bissEligibilityDataList = new ArrayList<Object>();

		BISSEligibilityData bissEligibilityData = new BISSEligibilityData();
		bissEligibilityData.setActiveFarmer(false);
		bissEligibilityData.setDeclaredEligibleAreaSqm(0.0);
		bissEligibilityData.setPaymentRightsValue(100.0);
		bissEligibilityDataList.add(bissEligibilityData);

		List<EligibilityResult> interventionEligibilityList = new ArrayList<EligibilityResult>();
		List<EligibilityResult> interventionEligibilityPaymentList = new ArrayList<EligibilityResult>();
		Map<String, Object> varsMap = new HashMap<String, Object>();
		varsMap.put("eligibilityResultList", interventionEligibilityList);
		varsMap.put("eligibilityResultPaymentList", interventionEligibilityPaymentList);
		String locale = new String("IT");
		varsMap.put("locale", locale);

		DroolsService droolsService = new DroolsServiceImpl(ruleFile);
		FireRulesRequest fireRulesRequest = new FireRulesRequest("eligibility.xls", bissEligibilityDataList, varsMap);
		droolsService.fireRules(fireRulesRequest);
		//System.out.println(bissEligibilityData.toString());
		assertEquals(bissEligibilityData.getGenuineFarmer(), false);
		assertEquals(bissEligibilityData.getLandUseDeclared(), false);
		assertEquals(bissEligibilityData.getEnoughLandUse(), false);
		assertEquals(bissEligibilityData.getHavePaymentRights(), false);
	}
	
	@Test
	public void WhenRulesAreFired_ThenSetEligibilityResultToTrue() throws Exception  {

		ArrayList<Object> bissEligibilityDataList = new ArrayList<Object>();

		BISSEligibilityData bissEligibilityData = new BISSEligibilityData();
		bissEligibilityData.setActiveFarmer(true);
		bissEligibilityData.setDeclaredEligibleAreaSqm(7000.0);
		bissEligibilityData.setPaymentRightsValue(101.0);
		bissEligibilityDataList.add(bissEligibilityData);

		List<EligibilityResult> interventionEligibilityList = new ArrayList<EligibilityResult>();
		List<EligibilityResult> interventionEligibilityPaymentList = new ArrayList<EligibilityResult>();
		Map<String, Object> varsMap = new HashMap<String, Object>();
		varsMap.put("eligibilityResultList", interventionEligibilityList);
		varsMap.put("eligibilityResultPaymentList", interventionEligibilityPaymentList);
		String locale = new String("EN");
		varsMap.put("locale", locale);

		DroolsService droolsService = new DroolsServiceImpl(ruleFile);
		FireRulesRequest fireRulesRequest = new FireRulesRequest("eligibility.xls", bissEligibilityDataList, varsMap);
		droolsService.fireRules(fireRulesRequest);
		//System.out.println(bissEligibilityData.toString());		
		for (Object er : interventionEligibilityList) {
			assertEquals(((EligibilityResult)er).isEligible(), true);
		}
		for (Object er : interventionEligibilityPaymentList) {
			assertEquals(((EligibilityResult)er).isEligible(), true);
		}

	}
	
	@Test
	public void WhenRulesAreFired_ThenSetEligibilityResultLocaleMessage() throws Exception  {

		ArrayList<String> messageList = new ArrayList<String>();
		ArrayList<String> messagePaymentsList = new ArrayList<String>();
		
		messageList.add("You are an genuine farmer");
		
		messagePaymentsList.add("You declare your land use");
		messagePaymentsList.add("You have at least 0.2 ha of land");
		messagePaymentsList.add("You have corresponding payment rights");		
		
		
		ArrayList<Object> bissEligibilityDataList = new ArrayList<Object>();

		BISSEligibilityData bissEligibilityData = new BISSEligibilityData();
		bissEligibilityData.setActiveFarmer(true);
		bissEligibilityData.setDeclaredEligibleAreaSqm(7000.0);
		bissEligibilityData.setPaymentRightsValue(101.0);
		bissEligibilityDataList.add(bissEligibilityData);

		List<EligibilityResult> interventionEligibilityList = new ArrayList<EligibilityResult>();
		List<EligibilityResult> interventionEligibilityPaymentList = new ArrayList<EligibilityResult>();
		Map<String, Object> varsMap = new HashMap<String, Object>();
		varsMap.put("eligibilityResultList", interventionEligibilityList);
		varsMap.put("eligibilityResultPaymentList", interventionEligibilityPaymentList);
		String locale = new String("EN");
		varsMap.put("locale", locale);
		DroolsService droolsService = new DroolsServiceImpl(ruleFile);
		FireRulesRequest fireRulesRequest = new FireRulesRequest("eligibility.xls", bissEligibilityDataList, varsMap);
		droolsService.fireRules(fireRulesRequest);
		//System.out.println("ELIGIBILITY LIST:");
		for (int i = 0; i < interventionEligibilityList.size(); i++) {
			//System.out.println((interventionEligibilityList.get(i)).toString());
			assertEquals(((EligibilityResult)interventionEligibilityList.get(i)).getMessage(), messageList.get(i));
			
		}
		//System.out.println("ELIGIBILITY PAYMENT LIST:");
		for (int i = 0; i < interventionEligibilityPaymentList.size(); i++) {
			//System.out.println((interventionEligibilityPaymentList.get(i)).toString());
			assertEquals(((EligibilityResult)interventionEligibilityPaymentList.get(i)).getMessage(), messagePaymentsList.get(i));
			
		}
	}
	
	@Test
	public void WhenChecksEvaluatesToFalse_ThenFillMessagesAnyway() throws Exception  {

		ArrayList<String> messageList = new ArrayList<String>();
		ArrayList<String> messagePaymentsList = new ArrayList<String>();
		
		messageList.add("You are an genuine farmer");
		messagePaymentsList.add("You declare your land use");
		messagePaymentsList.add("You have at least 0.2 ha of land");
		messagePaymentsList.add("You have corresponding payment rights");		
		
		
		ArrayList<Object> bissEligibilityDataList = new ArrayList<Object>();

		BISSEligibilityData bissEligibilityData = new BISSEligibilityData();
		bissEligibilityData.setActiveFarmer(false);
		bissEligibilityData.setDeclaredEligibleAreaSqm(0.0);
		bissEligibilityData.setPaymentRightsValue(0.0);
		bissEligibilityDataList.add(bissEligibilityData);

		List<EligibilityResult> interventionEligibilityList = new ArrayList<EligibilityResult>();
		List<EligibilityResult> interventionEligibilityPaymentList = new ArrayList<EligibilityResult>();
		Map<String, Object> varsMap = new HashMap<String, Object>();
		varsMap.put("eligibilityResultList", interventionEligibilityList);
		varsMap.put("eligibilityResultPaymentList", interventionEligibilityPaymentList);
		String locale = new String("EN");
		varsMap.put("locale", locale);
		DroolsService droolsService = new DroolsServiceImpl(ruleFile);
		FireRulesRequest fireRulesRequest = new FireRulesRequest("eligibility.xls", bissEligibilityDataList, varsMap);
		droolsService.fireRules(fireRulesRequest);
		
		assertEquals(1, interventionEligibilityList.size());
		assertEquals(3, interventionEligibilityPaymentList.size());
		
		int i = 0;
		for (EligibilityResult res : interventionEligibilityList) {
			assertEquals(res.getMessage(), messageList.get(i++));
			assertFalse(res.isEligible());
			
		}
		
		i = 0;
		for (EligibilityResult res : interventionEligibilityPaymentList) {
			assertEquals(res.getMessage(), messagePaymentsList.get(i++));
			assertFalse(res.isEligible());
			
		}
	}
	
}
