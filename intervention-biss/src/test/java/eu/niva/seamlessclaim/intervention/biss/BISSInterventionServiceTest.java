package eu.niva.seamlessclaim.intervention.biss;

/*-
 * #%L
 * intervention-biss
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutorService;

import javax.validation.Validator;
import javax.ws.rs.client.Client;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.jdbi.v3.core.Jdbi;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.niva.seamlessclaim.intervention.biss.config.ClaimConfiguration;
import eu.niva.seamlessclaim.sdk.Config;
import eu.niva.seamlessclaim.sdk.datapull.loader.PartyLoader;
import eu.niva.seamlessclaim.sdk.intervention.CampaignReferenceDate;
import eu.niva.seamlessclaim.sdk.intervention.ComputeIndicatorsParameters;
import eu.niva.seamlessclaim.sdk.intervention.IndicatorsResult;
import eu.niva.seamlessclaim.sdk.intervention.MonitoringResult;
import eu.niva.seamlessclaim.sdk.intervention.PaymentCalculationResult;
import io.dropwizard.configuration.YamlConfigurationFactory;
import io.dropwizard.jackson.Jackson;
import io.dropwizard.jersey.validation.Validators;

public class BISSInterventionServiceTest {

	private BISSInterventionService bissInterventionService;
	private final ObjectMapper objectMapper = Jackson.newObjectMapper();
	private final Validator validator = Validators.newValidator();
	private final YamlConfigurationFactory<ClaimConfiguration> factory =
			new YamlConfigurationFactory<>(ClaimConfiguration.class, validator, objectMapper, "dw");

	@Test
	public void calculatePayment_emptyParams_NotNullValues(){

		createBISSInterventionService();
		PaymentCalculationResult<BISSPaymentData> calculationResult = 
				bissInterventionService.calculatePayment(new ComputeIndicatorsParameters(), new BISSEligibilityData(), new BISSMonitoringData());

		assertNotNull(calculationResult.getData().getAreaPayableSqm());	
		assertNotNull(calculationResult.getData().getCrossComplianceReduction());	
		assertNotNull(calculationResult.getData().getGrossAmount());	
		assertNotNull(calculationResult.getData().getNetAmount());	
	}
	
	@Test
	public void calculatePayment_negativeParams_netValueNotNegative(){

		ComputeIndicatorsParameters computeIndicatorsParameters = new ComputeIndicatorsParameters();
		
		BISSEligibilityData bissEligibilityData = new BISSEligibilityData();
		bissEligibilityData.setPaymentRightsAreaSqm(-3.2);
		
		BISSMonitoringData bissMonitoringData = new BISSMonitoringData();
		bissMonitoringData.setAreaFoundSqm(5.8);
		bissMonitoringData.setCrossComplianceReduction(0.90);
		
		
		createBISSInterventionService();
		PaymentCalculationResult<BISSPaymentData> calculationResult = 
				bissInterventionService.calculatePayment(computeIndicatorsParameters, bissEligibilityData, bissMonitoringData);

		assertTrue(calculationResult.getData().getNetAmount() >= 0);	
	}

	private void createBISSInterventionService() {
		bissInterventionService = new BISSInterventionService() {

			@Override
			public boolean isConfigured() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public void sendNotification(String farmId, List<String> message) {
				// TODO Auto-generated method stub

			}

			@Override
			public @NonNull String registerToMonitoringSystem(ComputeIndicatorsParameters params) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public @NonNull CampaignReferenceDate getCampaignReferenceDate(OffsetDateTime date) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public @NonNull MonitoringResult<BISSMonitoringData> computeMonitoringResult(ComputeIndicatorsParameters params,
					BISSEligibilityData eligibilityData, String monitoringSystemId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public @NonNull IndicatorsResult<BISSEligibilityData> computeEligibilityIndicators(
					ComputeIndicatorsParameters params, BISSEligibilityData data) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public @NonNull IndicatorsResult<BISSEligibilityData> computeEligibilityIndicators(
					ComputeIndicatorsParameters params) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean canWriteFarmData(String userId, String farmId) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean canReadFarmData(String userId, String farmId) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			protected PartyLoader getPartyLoader() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			protected void doConfigure(Config config) throws Exception {
				// TODO Auto-generated method stub

			}
		};
		
		configureBISSInterventionService();

	}

	private void configureBISSInterventionService() {
		try {
			final File yaml = new File(Thread.currentThread().getContextClassLoader().getResource("config.yml").getPath());
			ClaimConfiguration configuration;
			configuration = factory.build(yaml);
			
			bissInterventionService.configure(new Config() {

				@Override
				public Optional<Client> getClient() {
					// TODO Auto-generated method stub
					return Optional.empty();
				}

				@Override
				public Optional<Jdbi> getDataSourceFactory(String name) {
					// TODO Auto-generated method stub
					return Optional.empty();
				}

				@Override
				public Optional<String> getProperty(String name) {
					Map<String, String> conf = configuration.getPluginsConfiguration().getProperties();
					if (conf == null || conf.isEmpty())
						return Optional.empty();

					return Optional.ofNullable(conf.get(name));
				}

				@Override
				public Optional<ExecutorService> getExecutorService() {
					// TODO Auto-generated method stub
					return Optional.empty();
				}
				
			});
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

	}




}
