package eu.niva.seamlessclaim.intervention.biss;

/*-
 * #%L
 * intervention-biss
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayOutputStream;
import java.util.Locale;

import org.junit.jupiter.api.Test;

import eu.niva.seamlessclaim.sdk.BaseParameters;
import eu.niva.seamlessclaim.sdk.intervention.ContractData;

public class ContractPdfTest {
	private static final BaseParameters PARAMS = new BaseParameters("TEST", Locale.ENGLISH);
	

	@Test
	public void WhenGenerateBissContractPDF_thenOutputStreamIsFilled() throws Exception {
		ContractData contractObj = new ContractData();
		contractObj.setPartyId("PartyId-123");
		contractObj.setPartyDescription("PartyDescription-123");
		contractObj.setSubscribedBy("SubscribedBy-123");
		contractObj.setId("Id-123");
		ByteArrayOutputStream pdfOut = new ByteArrayOutputStream();
		BISSInterventionServiceReport bissInterventionServiceReport = new BISSInterventionServiceReport();
		bissInterventionServiceReport.generateContractPDF(PARAMS, contractObj, null, pdfOut);
		assertTrue(pdfOut.size() > 1000);
	}

	
}
