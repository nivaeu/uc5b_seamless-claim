# NIVA - Seamless claim

This repo contains the backend project for the NIVA seamless claim application, the project is managed as a set of MAVEN modules; 
please see README.md files in the sub directories for module specific information.

# Introduction
This sub-project is part of the ["New IACS Vision in Action” --- NIVA](https://www.niva4cap.eu/) project that delivers a
a suite of digital solutions, e-tools and good practices for e-governance and initiates an innovation ecosystem to
support further development of IACS that will facilitate data and information flows.
This project has received funding from the European Union’s
Horizon 2020 research and innovation programme under grant agreement No 842009.
Please visit the [website](https://www.niva4cap.eu) for
further information. A complete list of the sub-projects
made available under the NIVA project can be found on [gitlab](https://gitlab.com/nivaeu/)

## Compile and package

You can compile and create a docker image for the project using the following command: `docker build -t niva/seamless-claim-builder:<version> -f docker/Dockerfile .`

The image does not include any plugins and does not include a config.yml file; it is meant to be used as a builder image for the PA specific plugins.

As an example:

    # Builder image
    FROM niva/seamless-claim-builder:1.0.0-SNAPSHOT as builder
    COPY ./ /build/
    WORKDIR /build
    RUN mvn clean package
    
    # Final image
    FROM openjdk:11.0.9-jre
    WORKDIR /app
    COPY --from=builder /build/sian.niva.seamlessclaim.main/target/dependency/*.jar /app/
    COPY --from=builder /build/sian.niva.seamlessclaim.main/target/*.jar /app/
    
    CMD [ "java", "-jar", "sian.niva.seamlessclaim.main-1.0.0-SNAPSHOT.jar", "server", "config.yml" ]

If you use a maven registry, you can avoid building this image and just publish the generated artifacts to your registry, i.e.:

    mvn clean deploy
