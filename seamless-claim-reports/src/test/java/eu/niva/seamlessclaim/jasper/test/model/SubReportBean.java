package eu.niva.seamlessclaim.jasper.test.model;

/*-
 * #%L
 * seamless-claim-reports
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

public class SubReportBean {
	   private String city;
	   private String street;

	   public String getCity() {
	      return city;
	   }

	   public void setCity(String city) {
	      this.city = city;
	   }

	   public String getStreet() {
	      return street;
	   }

	   public void setStreet(String street) {
	      this.street = street;
	   }
	}
