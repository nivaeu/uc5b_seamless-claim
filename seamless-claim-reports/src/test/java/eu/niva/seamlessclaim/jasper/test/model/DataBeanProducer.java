package eu.niva.seamlessclaim.jasper.test.model;

/*-
 * #%L
 * seamless-claim-reports
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DataBeanProducer {
	public ArrayList<DataBean> getDataBeanList() {
	
		ArrayList<DataBean> dataBeanList = new ArrayList<DataBean>();
		SubReportBean subBean1 = new SubReportBean();
		subBean1.setCity("Parigi");
		subBean1.setStreet("rue italie");
		SubReportBean subBean4 = new SubReportBean();
		subBean4.setCity("Firenze");
		subBean4.setStreet("Piazza Signoria");
		SubReportBean subBean5 = new SubReportBean();
		subBean5.setCity("Mantova");
		subBean5.setStreet("via roma");
		SubReportBean subBean6 = new SubReportBean();
		subBean6.setCity("Perugia");
		subBean6.setStreet("via po");		
		ArrayList<SubReportBean> subReportBeanList = new ArrayList<SubReportBean>();
		subReportBeanList.add(subBean4);
		subReportBeanList.add(subBean5);
		subReportBeanList.add(subBean6);
		ArrayList<Sub2ReportBean> sub2ReportBeanList = new ArrayList<Sub2ReportBean>();	
		Sub2ReportBean sub2Bean1 = new Sub2ReportBean();
		sub2Bean1.setCap("00033");
		sub2Bean1.setTelefono("034588967");
		Sub2ReportBean sub2Bean2 = new Sub2ReportBean();
		sub2Bean2.setCap("05511");
		sub2Bean2.setTelefono("04444661");
		sub2ReportBeanList.add(sub2Bean1);
		sub2ReportBeanList.add(sub2Bean2);
		dataBeanList.add(produce("Rossi", "Italia", subReportBeanList, sub2ReportBeanList));		
		dataBeanList.add(produce("Mbappe", "Francia", Arrays.asList(subBean1), sub2ReportBeanList));		
		
		return dataBeanList;
	}

	

	private DataBean produce(String name, String country, List<SubReportBean> subBeanList, List<Sub2ReportBean> sub2BeanList) {
		DataBean dataBean = new DataBean();

		dataBean.setName(name);
		dataBean.setCountry(country);
		dataBean.setSubReportBeanList(subBeanList);
		dataBean.setSubReportBeanList2(	sub2BeanList);

		return dataBean;
	}
	

	
	

}
