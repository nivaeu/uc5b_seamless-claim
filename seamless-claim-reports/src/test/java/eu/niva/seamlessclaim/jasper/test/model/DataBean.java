package eu.niva.seamlessclaim.jasper.test.model;

/*-
 * #%L
 * seamless-claim-reports
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.List;

public class DataBean {
	private String name;
	private String country;
	private List<SubReportBean> subReportBeanList;
	private List<Sub2ReportBean> subReportBeanList2;


	public List<Sub2ReportBean> getSubReportBeanList2() {
		return subReportBeanList2;
	}

	public void setSubReportBeanList2(List<Sub2ReportBean> subReportBeanList2) {
		this.subReportBeanList2 = subReportBeanList2;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public List<SubReportBean> getSubReportBeanList() {
		return subReportBeanList;
	}

	public void setSubReportBeanList(List<SubReportBean> subReportBeanList) {
		this.subReportBeanList = subReportBeanList;
	}
}
