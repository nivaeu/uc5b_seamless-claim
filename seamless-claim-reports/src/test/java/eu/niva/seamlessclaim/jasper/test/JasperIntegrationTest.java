package eu.niva.seamlessclaim.jasper.test;

/*-
 * #%L
 * seamless-claim-reports
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayOutputStream;

//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertNotEquals;

import java.io.File;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;

import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.RepetitionInfo;
import org.junit.jupiter.api.Test;

import eu.niva.seamlessclaim.jasper.factory.JasperCacheHolder;
import eu.niva.seamlessclaim.jasper.service.JasperService;
import eu.niva.seamlessclaim.jasper.service.JasperServiceImpl;
import eu.niva.seamlessclaim.jasper.test.model.DataBean;
import eu.niva.seamlessclaim.jasper.test.model.DataBeanProducer;
import net.sf.jasperreports.engine.JasperReport;

public class JasperIntegrationTest {

	public static JasperReport jasperReport;
	public static final String sMainTemplate = "main_template";

	@RepeatedTest(3)
	public void whenAtemplateIsUpdated_ThenUpdateJasperCache(RepetitionInfo repetitionInfo) throws Exception {
		System.out.println("whenAtemplateIsUpdated_ThenUpdateJasperCache - START " + repetitionInfo.getCurrentRepetition());
		ClassLoader classLoader = getClass().getClassLoader();
		File reportDir = new File(classLoader.getResource("Report").getFile());
		JasperService jasperService = new JasperServiceImpl(reportDir);
		DataBeanProducer dataBeanProducer = new DataBeanProducer();
		ArrayList<DataBean> dataList = dataBeanProducer.getDataBeanList();
		if (repetitionInfo.getCurrentRepetition() == 3) {
			File reportFile = new File(classLoader.getResource("Report/Biss/sub_template.jrxml").getFile());
			reportFile.setLastModified(new Date().getTime());
		}
		OutputStream pdfOut = new ByteArrayOutputStream();
		jasperService.generateReport("Biss/main_template.jrxml", dataList, pdfOut);
		if (repetitionInfo.getCurrentRepetition() == 3) {
			assertNotEquals(JasperCacheHolder.getInstance().getJasperResourceMap().get(sMainTemplate).get(0).getJasperReport(), jasperReport);
		} else if (jasperReport != null) {
			assertEquals(JasperCacheHolder.getInstance().getJasperResourceMap().get(sMainTemplate).get(0).getJasperReport(), jasperReport);
		}
		jasperReport = JasperCacheHolder.getInstance().getJasperResourceMap().get(sMainTemplate).get(0).getJasperReport();
		System.out.println("whenAtemplateIsUpdated_ThenUpdateJasperCache - END " + repetitionInfo.getCurrentRepetition());
	}

	@Test
	public void whenGiveDataBeanThenCompileAndExportReport() throws Exception {
		System.out.println("whenGiveDataBeanThenCompileAndExportReport - START");		
		ClassLoader classLoader = getClass().getClassLoader();
		File reportDir = new File(classLoader.getResource("Report").getFile());
		JasperService jasperService = new JasperServiceImpl(reportDir);
		DataBeanProducer dataBeanProducer = new DataBeanProducer();
		ArrayList<DataBean> dataList = dataBeanProducer.getDataBeanList();
		ByteArrayOutputStream pdfOut = new ByteArrayOutputStream();
		jasperService.generateReport("Biss/main_template.jrxml", dataList, pdfOut);
		assertTrue(pdfOut.size() > 1000);
		System.out.println("whenGiveDataBeanThenCompileAndExportReport - END");
	}

}
