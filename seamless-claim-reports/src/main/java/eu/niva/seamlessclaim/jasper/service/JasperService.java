package eu.niva.seamlessclaim.jasper.service;

/*-
 * #%L
 * seamless-claim-reports
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.io.OutputStream;
import java.util.List;

public interface JasperService {

//	void generateReport(String mainReportName, List<?> reportBeanList, OutputStream outPdf) throws Exception;

	void generateReport(String reportFileName, List<?> reportBeanList, OutputStream pdfOut) throws Exception;

}
