package eu.niva.seamlessclaim.jasper.factory;

/*-
 * #%L
 * seamless-claim-reports
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;

public class JasperReportFactory {

	private static final Logger log = LoggerFactory.getLogger(JasperReportFactory.class);

	private static File getLastModified(File[] files) {
		long lastModifiedTime = Long.MIN_VALUE;
		File chosenFile = null;
		if (files != null) {
			for (File file : files) {
				if (file.lastModified() > lastModifiedTime) {
					chosenFile = file;
					lastModifiedTime = file.lastModified();
				}
			}
		}
		return chosenFile;
	}

	public List<JasperResource> getJasperReport(File reportPath, String mainRepo) throws Exception {
		File[] files = reportPath.listFiles((dir, name) -> name.toLowerCase().endsWith(".jrxml"));
		File lastModifiedFile = getLastModified(files);
		JasperCacheHolder cache = JasperCacheHolder.getInstance();
		if (!cache.getJasperResourceMap().containsKey(mainRepo) || cache.isUpdatedReportFile(mainRepo, lastModifiedFile)) {
			List<JasperResource> jasperResourceList = new ArrayList<JasperResource>();
			for (File file : files) {
				 log.info("compiling template report {} .... " , file.getName());
				JasperReport jr = JasperCompileManager.compileReport(file.getAbsolutePath());
				jasperResourceList.add(new JasperResource(jr, new Date(file.lastModified())));
			}
			cache.getJasperResourceMap().put(mainRepo, jasperResourceList);
		}
		return cache.getJasperResourceMap().get(mainRepo);

	}

}
