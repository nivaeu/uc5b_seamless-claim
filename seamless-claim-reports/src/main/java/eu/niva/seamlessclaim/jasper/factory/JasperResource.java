package eu.niva.seamlessclaim.jasper.factory;

/*-
 * #%L
 * seamless-claim-reports
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import net.sf.jasperreports.engine.JasperReport;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JasperResource{
	private @NonNull JasperReport jasperReport;
	private @NonNull Date lastModified;
}
