package eu.niva.seamlessclaim.jasper.service;

/*-
 * #%L
 * seamless-claim-reports
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.niva.seamlessclaim.jasper.factory.JasperReportFactory;
import eu.niva.seamlessclaim.jasper.factory.JasperResource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class JasperServiceImpl implements JasperService {

	private File reportRootPath;
	private static final Logger log = LoggerFactory.getLogger(JasperServiceImpl.class);

	public JasperServiceImpl(File reportPath) throws Exception {
		if (!reportPath.isDirectory()) {
			throw new Exception("report Directory not found " + reportPath);
		}
		this.reportRootPath = reportPath;
	}

	@Override
	public void generateReport(String reportFileName, List<?> reportBeanList, OutputStream pdfOut) throws Exception {
		JRBeanCollectionDataSource beanColDataSource = new JRBeanCollectionDataSource(reportBeanList, false);
		File reportFile = new File(this.reportRootPath + File.separator + reportFileName);
		String mainReportName = reportFile.getName().replaceFirst("[.][^.]+$", "");

		JasperReportFactory jrf = new JasperReportFactory();
		List<JasperResource> jrl = jrf.getJasperReport(reportFile.getParentFile(), mainReportName);

		Map<String, Object> parameters = new HashMap<String, Object>();
		JasperReport mainJasperReport = null;

		for (JasperResource jr : jrl) {
			if (!jr.getJasperReport().getName().equalsIgnoreCase(mainReportName)) {
				parameters.put(jr.getJasperReport().getName(), jr.getJasperReport());
			} else {
				mainJasperReport = jr.getJasperReport();
			}
		}

		ByteArrayOutputStream fillOut = new ByteArrayOutputStream();
		InputStream pdfIn = null;
		log.info("filling report {} ...", mainReportName);
		JasperFillManager.fillReportToStream(mainJasperReport, fillOut, parameters, beanColDataSource);
		pdfIn = new ByteArrayInputStream(fillOut.toByteArray());

		log.info("exporting pdf...");
		JasperExportManager.exportReportToPdfStream(pdfIn, pdfOut);
		log.info("pdf report {} exported", mainReportName);
		pdfIn.close();
		fillOut.close();
	}



}
