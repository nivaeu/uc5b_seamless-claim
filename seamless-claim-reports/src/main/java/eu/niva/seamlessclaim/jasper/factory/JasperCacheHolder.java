package eu.niva.seamlessclaim.jasper.factory;

/*-
 * #%L
 * seamless-claim-reports
 * %%
 * Copyright (C) 2019 - 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JasperCacheHolder {

	private static final Logger log = LoggerFactory.getLogger(JasperCacheHolder.class);

	private Map<String, List<JasperResource>> jasperResourceMap;

	public Map<String, List<JasperResource>> getJasperResourceMap() {
		return jasperResourceMap;
	}

	public void setJasperResourceMap(Map<String, List<JasperResource>> jasperResourceMap) {
		this.jasperResourceMap = jasperResourceMap;
	}

	private JasperCacheHolder() {
		jasperResourceMap = new HashMap<String, List<JasperResource>>();
	}

	private static JasperCacheHolder reference = null;

	public static JasperCacheHolder getInstance() {

		if (reference == null) {
			synchronized (JasperCacheHolder.class) {
				if (reference == null) {
					reference = new JasperCacheHolder();

				}
			}

		}
		return reference;
	}

	protected boolean isUpdatedReportFile(String jasperMain, File lastModifiedFile) {
		for (JasperResource jr : jasperResourceMap.get(jasperMain)) {
			if ((jr.getJasperReport().getName() + ".jrxml").equalsIgnoreCase(lastModifiedFile.getName())) {
				if (jr.getLastModified().before(new Date(lastModifiedFile.lastModified()))) {
					log.info("template report {} has been updated on {} ", jr.getJasperReport().getName(), new Date(lastModifiedFile.lastModified()));
					log.info("all jrxml files of {}  will now be recompiled", jasperMain);
					return true;
				}
			}
		}
		return false;

	}

}
